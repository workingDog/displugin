package com.kodekutters.dis.system;

import org.openide.explorer.ExplorerManager;

/**
 * EntityExplorerManager, the shared global ExplorerManager
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
public class EntityExplorerManager {

    public static final ExplorerManager SHARED_ENTITY_MANAGER = new ExplorerManager();
}