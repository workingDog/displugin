/**
 * DisReplaySupport a class to support DIS socket for writing pdu and send the
 * pdu to the listeners.
 *
 * Code base is from various examples of reference:
 * http://open-dis.sourceforge.net/Open-DIS.html
 *
 * @author Ringo Wathelet
 * @version 1.0, 2011
 */
package com.kodekutters.dis.core;

import com.kodekutters.dis.util.DisAreaMonitor;
import edu.nps.moves.dis.PduContainer;
import edu.nps.moves.disutil.PduFactory;
import edu.nps.moves.net.BehaviorConsumerIF;
import java.io.*;
import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class DisReplaySupport extends DisSupport implements DisDirectorySupport, Runnable {

    public static String END_OF_DATA = "END_OF_DATA";
    protected String dirName = System.getProperty("user.home");
    private ZipFile zipFile = null;

    public DisReplaySupport() {
        super();
    }

    public DisReplaySupport(int disPort, String disAddress) {
        super(disPort, disAddress);
    }

    public DisReplaySupport(Object aListerner) {
        this();
        this.addListener(aListerner);
    }

    public void setDirectoryName(String dirname) {
        this.dirName = dirname;
    }

    public String getDirectoryName() {
        return this.dirName;
    }

    @Override
    public void run() {
        System.out.println("Writing pdu to the connection...");
        try {
            this.getConnection().makeSocketConnection();
        } catch (Exception ex) {
            Logger.getLogger(DisSupport.class.getName()).log(Level.WARNING, "connection failed", ex);
            this.fireAction(ACTION_COMMAND_FAIL);
            this.stop();
            return;
        }
        this.setRunning(true);
        this.replayExercise();
        this.stop();
        this.fireAction(END_OF_DATA);
    }

    private void replayExercise() {
        try {
            this.getPacketCounter().resetToZero();

            String logFiles[];
            File selectedFile = new File(this.getDirectoryName());

            if (selectedFile.isDirectory()) {
                // have a directory, get the list of files in it
                logFiles = selectedFile.list();
            } else {
                // have a single file
                logFiles = new String[]{selectedFile.getAbsolutePath()};
            }

            // for each zip file
            for (int idx = 0; idx < logFiles.length; idx++) {

                String fileName;
                if (selectedFile.isDirectory()) {
                    if (this.getDirectoryName().endsWith(System.getProperty("file.separator"))) {
                        fileName = this.getDirectoryName() + logFiles[idx];
                    } else {
                        fileName = this.getDirectoryName() + System.getProperty("file.separator") + logFiles[idx];
                    }
                } else {
                    fileName = logFiles[idx];
                }
                if (!fileName.toLowerCase().endsWith(".zip")) {
                    System.out.println("skipping file : " + fileName + " because it does not ends with .zip");
                    continue;
                }

                try {
                    zipFile = new ZipFile(fileName);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(DisReplaySupport.class.getName()).log(Level.SEVERE, null, ex);
                    continue;
                }

                // for each file entry in the zip file
                Enumeration elist = zipFile.entries();
                while (elist.hasMoreElements()) {
                    ZipEntry zipEntry = (ZipEntry) elist.nextElement();

                    JAXBContext context = JAXBContext.newInstance(
                            edu.nps.moves.dis.MinefieldStatePdu.class,
                            edu.nps.moves.dis.AcknowledgeReliablePdu.class,
                            edu.nps.moves.dis.SyntheticEnvironmentFamilyPdu.class,
                            edu.nps.moves.dis.DesignatorPdu.class,
                            edu.nps.moves.dis.AcousticBeamData.class,
                            edu.nps.moves.dis.AcousticEmitterSystemData.class,
                            edu.nps.moves.dis.FundamentalParameterDataIff.class,
                            edu.nps.moves.dis.Relationship.class,
                            edu.nps.moves.dis.EntityManagementFamilyPdu.class,
                            edu.nps.moves.dis.FastEntityStatePdu.class,
                            edu.nps.moves.dis.DataReliablePdu.class,
                            edu.nps.moves.dis.BurstDescriptor.class,
                            edu.nps.moves.dis.EntityInformationFamilyPdu.class,
                            edu.nps.moves.dis.IffAtcNavAidsLayer2Pdu.class,
                            edu.nps.moves.dis.NamedLocation.class,
                            edu.nps.moves.dis.TransferControlRequestPdu.class,
                            edu.nps.moves.dis.EightByteChunk.class,
                            edu.nps.moves.dis.ElectronicEmissionsPdu.class,
                            edu.nps.moves.dis.CreateEntityReliablePdu.class,
                            edu.nps.moves.dis.LinearSegmentParameter.class,
                            edu.nps.moves.dis.EventReportPdu.class,
                            edu.nps.moves.dis.MinefieldResponseNackPdu.class,
                            edu.nps.moves.dis.FourByteChunk.class,
                            edu.nps.moves.dis.EntityStateUpdatePdu.class,
                            edu.nps.moves.dis.EnvironmentalProcessPdu.class,
                            edu.nps.moves.dis.GridAxisRecord.class,
                            edu.nps.moves.dis.IntercomCommunicationsParameters.class,
                            edu.nps.moves.dis.LinearObjectStatePdu.class,
                            edu.nps.moves.dis.EventReportReliablePdu.class,
                            edu.nps.moves.dis.ArealObjectStatePdu.class,
                            edu.nps.moves.dis.AcousticBeamFundamentalParameter.class,
                            edu.nps.moves.dis.Vector3Float.class,
                            edu.nps.moves.dis.StopFreezePdu.class,
                            edu.nps.moves.dis.ArticulationParameter.class,
                            edu.nps.moves.dis.FixedDatum.class,
                            edu.nps.moves.dis.CommentReliablePdu.class,
                            edu.nps.moves.dis.ServiceRequestPdu.class,
                            edu.nps.moves.dis.IsGroupOfPdu.class,
                            edu.nps.moves.dis.UaPdu.class,
                            edu.nps.moves.dis.PointObjectStatePdu.class,
                            edu.nps.moves.dis.FundamentalParameterData.class,
                            edu.nps.moves.dis.DataQueryReliablePdu.class,
                            edu.nps.moves.dis.SetRecordReliablePdu.class,
                            edu.nps.moves.dis.ElectronicEmissionBeamData.class,
                            edu.nps.moves.dis.DetonationPdu.class,
                            edu.nps.moves.dis.RecordSet.class,
                            edu.nps.moves.dis.ActionResponsePdu.class,
                            edu.nps.moves.dis.ShaftRPMs.class,
                            edu.nps.moves.dis.ActionRequestPdu.class,
                            edu.nps.moves.dis.IsPartOfPdu.class,
                            edu.nps.moves.dis.DeadReckoningParameter.class,
                            edu.nps.moves.dis.GridAxisRecordRepresentation2.class,
                            edu.nps.moves.dis.MinefieldQueryPdu.class,
                            edu.nps.moves.dis.SystemID.class,
                            edu.nps.moves.dis.MinefieldDataPdu.class,
                            edu.nps.moves.dis.SimulationAddress.class,
                            edu.nps.moves.dis.EntityID.class,
                            edu.nps.moves.dis.AntennaLocation.class,
                            edu.nps.moves.dis.DataQueryPdu.class,
                            edu.nps.moves.dis.EntityType.class,
                            edu.nps.moves.dis.IffAtcNavAidsLayer1Pdu.class,
                            edu.nps.moves.dis.GridAxisRecordRepresentation1.class,
                            edu.nps.moves.dis.BeamAntennaPattern.class,
                            edu.nps.moves.dis.ResupplyReceivedPdu.class,
                            edu.nps.moves.dis.Orientation.class,
                            edu.nps.moves.dis.Pdu.class,
                            edu.nps.moves.dis.ReceiverPdu.class,
                            edu.nps.moves.dis.AggregateType.class,
                            edu.nps.moves.dis.Environment.class,
                            edu.nps.moves.dis.WarfareFamilyPdu.class,
                            edu.nps.moves.dis.StopFreezeReliablePdu.class,
                            edu.nps.moves.dis.SetDataReliablePdu.class,
                            edu.nps.moves.dis.CommentPdu.class,
                            edu.nps.moves.dis.SimulationManagementWithReliabilityFamilyPdu.class,
                            edu.nps.moves.dis.CollisionPdu.class,
                            edu.nps.moves.dis.IffFundamentalData.class,
                            edu.nps.moves.dis.VariableDatum.class,
                            edu.nps.moves.dis.FirePdu.class,
                            edu.nps.moves.dis.SetDataPdu.class,
                            edu.nps.moves.dis.AcousticEmitterSystem.class,
                            edu.nps.moves.dis.RemoveEntityReliablePdu.class,
                            edu.nps.moves.dis.RepairCompletePdu.class,
                            edu.nps.moves.dis.CreateEntityPdu.class,
                            edu.nps.moves.dis.IntercomControlPdu.class,
                            edu.nps.moves.dis.ModulationType.class,
                            edu.nps.moves.dis.SixByteChunk.class,
                            edu.nps.moves.dis.IntercomSignalPdu.class,
                            edu.nps.moves.dis.AggregateID.class,
                            edu.nps.moves.dis.StartResumePdu.class,
                            edu.nps.moves.dis.ActionResponseReliablePdu.class,
                            edu.nps.moves.dis.SimulationManagementFamilyPdu.class,
                            edu.nps.moves.dis.SphericalHarmonicAntennaPattern.class,
                            edu.nps.moves.dis.ElectronicEmissionSystemData.class,
                            edu.nps.moves.dis.LayerHeader.class,
                            edu.nps.moves.dis.Point.class,
                            edu.nps.moves.dis.RecordQueryReliablePdu.class,
                            edu.nps.moves.dis.ResupplyOfferPdu.class,
                            edu.nps.moves.dis.ClockTime.class,
                            edu.nps.moves.dis.SignalPdu.class,
                            edu.nps.moves.dis.RadioEntityType.class,
                            edu.nps.moves.dis.AggregateStatePdu.class,
                            edu.nps.moves.dis.StartResumeReliablePdu.class,
                            edu.nps.moves.dis.DistributedEmissionsFamilyPdu.class,
                            edu.nps.moves.dis.TrackJamTarget.class,
                            edu.nps.moves.dis.AngularVelocityVector.class,
                            edu.nps.moves.dis.RepairResponsePdu.class,
                            edu.nps.moves.dis.Vector3Double.class,
                            edu.nps.moves.dis.AcknowledgePdu.class,
                            edu.nps.moves.dis.EntityStatePdu.class,
                            edu.nps.moves.dis.RemoveEntityPdu.class,
                            edu.nps.moves.dis.PduContainer.class,
                            edu.nps.moves.dis.GriddedDataPdu.class,
                            edu.nps.moves.dis.VectoringNozzleSystemData.class,
                            edu.nps.moves.dis.DataPdu.class,
                            edu.nps.moves.dis.ActionRequestReliablePdu.class,
                            edu.nps.moves.dis.EmitterSystem.class,
                            edu.nps.moves.dis.GridAxisRecordRepresentation0.class,
                            edu.nps.moves.dis.SeesPdu.class,
                            edu.nps.moves.dis.MinefieldFamilyPdu.class,
                            edu.nps.moves.dis.ObjectType.class,
                            edu.nps.moves.dis.ApaData.class,
                            edu.nps.moves.dis.BeamData.class,
                            edu.nps.moves.dis.PropulsionSystemData.class,
                            edu.nps.moves.dis.OneByteChunk.class,
                            edu.nps.moves.dis.AcousticEmitter.class,
                            edu.nps.moves.dis.TransmitterPdu.class,
                            edu.nps.moves.dis.EventID.class,
                            edu.nps.moves.dis.Marking.class,
                            edu.nps.moves.dis.AggregateMarking.class,
                            edu.nps.moves.dis.CollisionElasticPdu.class,
                            edu.nps.moves.dis.TwoByteChunk.class,
                            edu.nps.moves.dis.ResupplyCancelPdu.class,
                            edu.nps.moves.dis.RadioCommunicationsFamilyPdu.class,
                            edu.nps.moves.dis.LogisticsFamilyPdu.class,
                            edu.nps.moves.dis.SupplyQuantity.class);

                    Unmarshaller unmarshaller = context.createUnmarshaller();
                    PduContainer unmarshalledObject = unmarshalFromZipFile(unmarshaller, fileName, zipEntry);

                    if (unmarshalledObject == null) {
                        System.err.println("Error in DisReplaySupport unmarshalledObject is null");
                        fireAction(ACTION_COMMAND_FAIL);
                        return;
                    }

                    this.replayList(unmarshalledObject.getPdus());

                    // if want to stop
                    if (!this.isRunning()) {
                        return;
                    }
                }
            }

        } catch (Exception e) {
            System.err.println("Error in DisReplaySupport replayList " + e);
            fireAction(ACTION_COMMAND_FAIL);
        }
    }

    private PduContainer unmarshalFromZipFile(Unmarshaller unmarshaller, String fileName, ZipEntry entry) {
        PduContainer unmarshalledObject = null;
        try {
            unmarshalledObject = (PduContainer) unmarshaller.unmarshal(new BufferedInputStream(zipFile.getInputStream(entry)));
        } catch (IOException iOException) {
            System.err.println("in DisReplaySupport io exception " + iOException);
        } catch (JAXBException jAXBException) {
            System.err.println("in DisReplaySupport JAXB exception " + jAXBException);
        }

        if (unmarshalledObject == null) {
            System.out.println("Error processing zip file: " + fileName + " ... file entry: " + entry.getName());
            return null;
        } else {
            System.out.println("processing zip file: " + fileName + " ... file entry: " + entry.getName() + " ... with: " + unmarshalledObject.getNumberOfPdus() + " pdu");
            return unmarshalledObject;
        }
    }

    private void replayList(List pduList) {
        PduFactory pduFactory = new PduFactory();
        for (int idx = 0; idx < pduList.size(); idx++) {

            // if want to stop
            if (!this.isRunning()) {
                return;
            }

            // if want to pause for a while
            while (this.isPaused()) {
                try {
                    disSupportThread.sleep(getPauseCheckTime());
                } catch (InterruptedException e) {
                    System.err.println("in DisReplaySupport error in sleeping " + e);
                }
            }

            edu.nps.moves.dis.Pdu aPdu = (edu.nps.moves.dis.Pdu) pduList.get(idx);

            // check that the pdu is one we want and is within the monitoring area 
            if (!isPduWanted(aPdu) && DisAreaMonitor.getInstance().isWithingArea(aPdu)) {
                continue;
            }

            // give the pdu to the listerners
            for (BehaviorConsumerIF theListener : this.pduListeners) {
                if (this.isUseCopies()) {
                    theListener.receivePdu(pduFactory.createPdu(ByteBuffer.wrap(aPdu.marshal())));
                } else {
                    theListener.receivePdu(aPdu);
                }
            }

            // send the data to the socket
            try {
                byte[] data = aPdu.marshal();
                DatagramPacket packet = new DatagramPacket(data, data.length, this.getConnection().getInetAddress(), this.getConnection().getPort());
                this.getConnection().getDatagramSocket().send(packet);
                this.getPacketCounter().increment();
            } catch (Exception e) {
                System.err.println("in DisReplaySupport replayList " + e);
            }

            // wait a bit for refresh time
            try {
                disSupportThread.sleep(this.getRefreshTime());
            } catch (InterruptedException e) {
                System.err.println("Error in sleeping " + e);
            }
        }
    }

    @Override
    public void stop() {
        this.setRunning(false);
        this.fireAction(ACTION_COMMAND_STOP);
        this.getConnection().stopConnection();
        try {
            if (zipFile != null) {
                zipFile.close();
            }
        } catch (IOException ex) {
//            Exceptions.printStackTrace(ex);
        }
        System.out.println("..... reading and writing processes are stopped .....");
    }
}
