package com.kodekutters.dis.core;

/**
 *
 * @author R. Wathelet
 */
public interface DisDirectorySupport {

    public void setDirectoryName(String dirname);

    public String getDirectoryName();
}
