/**
 * DisSupport base class to support DIS socket reading and writing. 
 * 
 * Code base is
 * from various examples of reference:
 * http://open-dis.sourceforge.net/Open-DIS.html
 *
 * Ringo Wathelet
 *
 * @version 1.0, 2011
 */
package com.kodekutters.dis.core;

import com.kodekutters.dis.connection.DisConnectionService;
import edu.nps.moves.dis.EntityStatePdu;
import edu.nps.moves.disenum.PduType;
import edu.nps.moves.net.BehaviorConsumerIF;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import org.openide.util.Lookup;

public class DisSupport implements Runnable {

    public static final String ACTION_COMMAND_START = "START";
    public static final String ACTION_COMMAND_STOP = "STOP";
    public static final String ACTION_COMMAND_FAIL = "FAIL";
    //
    protected final List<BehaviorConsumerIF> pduListeners = new ArrayList<BehaviorConsumerIF>();
    public final List<ActionListener> actionListeners = new ArrayList<ActionListener>();
    private DisConnectionService disConnection;
    public Thread disSupportThread = null;
    private volatile boolean isRunning = false;
    private boolean paused = false;
    private int pauseCheckTime = 1000;  // millisec
    private boolean useCopies = false;
    private int refreshTime = 500;
    private int applicationId = -1;  // -1 = all applications
    private int siteId = -1;         // -1 = all sites
    private ArrayList<Integer> exerciseIdList = new ArrayList<>();
    private ArrayList<Integer> pduTypeList = new ArrayList<>();
    private SyncCounter packetCounter = new SyncCounter();

    /**
     * constructor using the default port and multicast address
     */
    public DisSupport() {
        this(DisConnectionService.DEFAULT_PORT, DisConnectionService.DEFAULT_MULTICAST_ADDRESS);
    }

    /**
     * constructor
     *
     * @param disPort the port number
     * @param disAddress the broadcast ip address
     */
    public DisSupport(int disPort, String disAddress) {
        disConnection = Lookup.getDefault().lookup(DisConnectionService.class);
        if (disConnection != null) {
            disConnection.setPort(disPort);
            disConnection.setAddress(disAddress);
        }
    }

    public List<ActionListener> getActionListeners() {
        return actionListeners;
    }

    public List<BehaviorConsumerIF> getPduListeners() {
        return pduListeners;
    }

    public void addListener(Object aListener) {
        if (aListener instanceof BehaviorConsumerIF) {
            if (!(pduListeners.contains((BehaviorConsumerIF) aListener))) {
                pduListeners.add((BehaviorConsumerIF) aListener);
            }
            return;
        }
        if (aListener instanceof ActionListener) {
            if (!(actionListeners.contains((ActionListener) aListener))) {
                actionListeners.add((ActionListener) aListener);
            }
            return;
        }
    }

    public void removeListener(Object aListener) {
        if (aListener instanceof BehaviorConsumerIF) {
            pduListeners.remove((BehaviorConsumerIF) aListener);
            return;
        }
        if (aListener instanceof ActionListener) {
            actionListeners.remove((ActionListener) aListener);
            return;
        }
    }

    public ArrayList<Integer> getExerciseIdList() {
        return exerciseIdList;
    }

    public void setExerciseIdList(ArrayList<Integer> exId) {
        this.exerciseIdList = exId;
    }

    public ArrayList<Integer> getPduTypeList() {
        return pduTypeList;
    }

    public void setPduTypeList(ArrayList<Integer> pduTypeList) {
        this.pduTypeList = pduTypeList;
    }

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int val) {
        applicationId = val;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int val) {
        siteId = val;
    }

    public int getTimeToLive() {
        return this.getConnection().getTimeToLive();
    }

    public void setTimeToLive(int timeToLive) {
        this.getConnection().setTimeToLive(timeToLive);
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public int getPauseCheckTime() {
        return pauseCheckTime;
    }

    public void setPauseCheckTime(int pauseCheckTime) {
        this.pauseCheckTime = pauseCheckTime;
    }

    /**
     * if true will make copies of the pdu before sending them to the listeners
     *
     * @return true if pdu copy is on
     */
    public boolean isUseCopies() {
        return useCopies;
    }

    /**
     * set true to make copies of the pdu read before sending them to the
     * listeners
     */
    public void setUseCopies(boolean onof) {
        this.useCopies = onof;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }

    public void start() { 
        fireAction(ACTION_COMMAND_START);
        disSupportThread = new Thread(this);
        disSupportThread.setName("DisSupportThread");
        disSupportThread.start();
    }

    /**
     * the workhorse, to be overridden by the subclasses
     */
    @Override
    public void run() {
        System.out.println("in DisSupport run");
    }

    public Thread getDisSupportThread() {
        return disSupportThread;
    }

    /*
     * determines if the pdu corresponds to the chosen selection, 
     * such as: exercise, application, site and pdu type
     */
    protected boolean isPduWanted(edu.nps.moves.dis.Pdu pdu) {
        if (pdu != null) {
            // must have the correct exercise number
            if (!getExerciseIdList().contains(Integer.valueOf(pdu.getExerciseID()))) {
                return false;
            }
            // must have the correct pdu type
            if (!getPduTypeList().contains(Integer.valueOf(pdu.getPduType()))) {
                return false;
            }
            // check the application and site for espdu type
            if (getPduTypeList().contains(Integer.valueOf(PduType.ENTITY_STATE.getValue()))
                    || getPduTypeList().contains(Integer.valueOf(PduType.ENTITY_STATE_UPDATE.getValue()))) {

                //  take all applications if = -1
                if (this.getApplicationId() != -1) {
                    // check the application number against the espdu
                    if (this.getApplicationId() != ((EntityStatePdu) pdu).getEntityID().getApplication()) {
                        return false;
                    }
                }
                // take all sites if = -1
                if (this.getSiteId() != -1) {
                    // check the site number against the espdu
                    if (this.getSiteId() != ((EntityStatePdu) pdu).getEntityID().getSite()) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    public DisConnectionService getConnection() {
        return this.disConnection;
    }

    public void setConnection(DisConnectionService con) {
        this.disConnection = con;
    }

    public int getRefreshTime() {
        return this.refreshTime;
    }

    public void setRefreshTime(int val) {
        this.refreshTime = val;
    }

    public SyncCounter getPacketCounter() {
        return packetCounter;
    }

    /**
     * @return the number of packets received so far
     */
    public long getNumberOfPackets() {
        return getPacketCounter().getValue();
    }

    public void stop() {
        this.fireAction(ACTION_COMMAND_STOP);
        this.setRunning(false);
        this.getConnection().stopConnection();        
    }

    public void fireAction(String action) {
        for (ActionListener theListener : this.actionListeners) {
            theListener.actionPerformed(new ActionEvent(this, 0, action));
        }
    }

    /**
     * a thread safe synchronised counter, see java tutorial
     */
    public class SyncCounter {

        private long c = 0;

        public synchronized void resetToZero() {
            c = 0;
        }

        public synchronized void increment() {
            c++;
        }

        public synchronized void decrement() {
            c--;
        }

        public synchronized long getValue() {
            return c;
        }

        public synchronized void setValue(long val) {
            c = val;
        }
    }
}
