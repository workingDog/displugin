package com.kodekutters.dis.core;

/**
 *
 * @author R. Wathelet
 */
public interface DisFileSupport {

    public String getFileToRead();

    public void setFileToRead(String fileToRead);

    public int getPdusPerFile();

    public void setPdusPerFile(int pdusPerFile);

    public boolean isAlsoWriteToFile();

    public void setAlsoWriteToFile(boolean alsoWriteToFile);

}
