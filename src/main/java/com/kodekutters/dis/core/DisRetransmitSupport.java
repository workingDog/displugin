/**
 * DisRetransmitSupport a class to retransmit the pdu read from the wire,
 * presumably with some changes before sending the pdu back to the wire.
 *
 *
 * this is for testing only
 *
 *
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
package com.kodekutters.dis.core;

import com.kodekutters.dis.layer.DisLayer;
import edu.nps.moves.dis.EntityStatePdu;
import edu.nps.moves.dis.Pdu;
import edu.nps.moves.net.BehaviorConsumerIF;
import java.net.DatagramPacket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DisRetransmitSupport extends DisSupport implements BehaviorConsumerIF, Runnable {

    public DisRetransmitSupport() {
        super();
    }

    public DisRetransmitSupport(int disPort, String disAddress) {
        super(disPort, disAddress);
    }

    public DisRetransmitSupport(Object aListerner) {
        this();
        this.addListener(aListerner);
    }

    @Override
    public void run() {
        System.out.println("retransmiting pdu to the connection...");
        try {
            this.getConnection().makeSocketConnection();
        } catch (Exception ex) {
            Logger.getLogger(DisSupport.class.getName()).log(Level.WARNING, "connection failed", ex);
            this.fireAction(ACTION_COMMAND_FAIL);
            this.stop();
            return;
        }
        this.setRunning(true);
    }

    @Override
    public void receivePdu(Pdu pdu) {
        // if want to stop
        if (!this.isRunning()) {
            return;
        }

        // if want to pause for a while
        while (this.isPaused()) {
            try {
                disSupportThread.sleep(getPauseCheckTime());
            } catch (InterruptedException e) {
                System.err.println("in DisRetransmitSupport error in sleeping " + e);
            }
        }

        // check that the pdu is one we want 
        if (!isPduWanted(pdu)) {
            return;
        }

        try {
            switch (pdu.getPduTypeEnum()) {
                case ENTITY_STATE:
                case ENTITY_STATE_UPDATE:
//                    System.out.println("ENTITY_STATE");
                    // an example of changing something before retransmitting
                    changeMarking((EntityStatePdu) pdu);
                    break;

                case REMOVE_ENTITY_R:
                case REMOVE_ENTITY:
//                    System.out.println("REMOVE_ENTITY");
                    break;

                case COMMENT:
//                    System.out.println("COMMENT");
                    break;

                case FIRE:
//                    System.out.println("FIRE");
                    break;

                case DETONATION:
//                    System.out.println("DETONATION");
                    break;

                case COLLISION:
//                    System.out.println("COLLISION");
                    break;

                case SERVICE_REQUEST:
//                    System.out.println("SERVICE_REQUEST");
                    break;

                case RESUPPLY_OFFER:
//                    System.out.println("RESUPPLY_OFFER");
                    break;

                case RESUPPLY_RECEIVED:
//                    System.out.println("RESUPPLY_RECEIVED");
                    break;

                case RESUPPLY_CANCEL:
//                    System.out.println("RESUPPLY_CANCEL");
                    break;

                case REPAIR_COMPLETE:
//                    System.out.println("REPAIR_COMPLETE");
                    break;

                case REPAIR_RESPONSE:
//                    System.out.println("REPAIR_RESPONSE");
                    break;

                case CREATE_ENTITY:
//                    System.out.println("CREATE_ENTITY");
                    break;

                case START_RESUME:
//                    System.out.println("START_RESUME");
                    break;

                case STOP_FREEZE:
//                    System.out.println("STOP_FREEZE");
                    break;

                case ACKNOWLEDGE:
//                    System.out.println("ACKNOWLEDGE");
                    break;

                case ACTION_REQUEST:
//                    System.out.println("ACTION_REQUEST");
                    break;

                default:
//                    System.out.println("non standard pdu received...ignoring it");
            }
        } catch (Exception e) {
            Logger.getLogger(DisLayer.class.getName()).log(Level.WARNING, "Problem processing PDU type", e);
        }

        // send the pdu
        try {
            byte[] data = pdu.marshal();
            DatagramPacket packet = new DatagramPacket(data, data.length, this.getConnection().getInetAddress(), this.getConnection().getPort());
            this.getConnection().getDatagramSocket().send(packet);
            this.getPacketCounter().increment();
        } catch (Exception e) {
            System.err.println("in DisRetransmitSupport replayList " + e);
        }

        // wait a bit for refresh time
        try {
            disSupportThread.sleep(this.getRefreshTime());
        } catch (InterruptedException e) {
            System.err.println("Error in sleeping " + e);
        }
    }

    @Override
    public void stop() {
        this.setRunning(false);
        this.fireAction(ACTION_COMMAND_STOP);
        this.getConnection().stopConnection();
        System.out.println("..... retransmit process is stopped .....");
    }

    /**
     * an example of changing the pdu marking (before retransmitting) 
     * 
     * replace all \ and / to _
     *
     * @param pdu the EntityStatePdu to be changed
     */
    private void changeMarking(EntityStatePdu pdu) {
        String name = new String(pdu.getMarking().getCharacters()).trim();
        name = name.replaceAll("\\\\", "_");
        name = name.replaceAll("/", "_");
        pdu.getMarking().setCharacters(name);
    }
}
