/**
 * DisViewSupport a class to read DIS pdu from the wire and write them to file.
 * Will pass the read pdu to the listeners through receivePdu.
 *
 * Code base is from various examples of reference:
 * http://open-dis.sourceforge.net/Open-DIS.html
 *
 * Ringo Wathelet
 *
 * @version 1.0, 2012
 */
package com.kodekutters.dis.core;

import com.kodekutters.dis.connection.DatagramRetriever;
import com.kodekutters.dis.util.DisAreaMonitor;
import edu.nps.moves.dis.Pdu;
import edu.nps.moves.disenum.PduType;
import edu.nps.moves.disutil.PduFactory;
import edu.nps.moves.net.BehaviorConsumerIF;
import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class DisViewSupport extends DisSupport implements DisDirectorySupport, DisFileSupport {

    DatagramRetriever datagramRetriever;  // this will read the datagrams from the wire
    protected int pduListSize = 1000;  // possible number of pdus to read in one go
    protected int pdusPerFile = 10000; // max number of pdu to write to one xml file
    protected boolean alsoWriteToFile = false;
    protected LogWriter pduWriter = null;
    protected Thread writerThread;
    protected String dirName = System.getProperty("user.home");
    protected String fileToRead = null;
    protected ArrayList<Pdu> currentPduList = new ArrayList<>(getPdusPerFile());

    /**
     * constructor using the default port and multicast address
     */
    public DisViewSupport() {
        super();
    }

    /**
     * constructor
     *
     * @param disPort the port number
     * @param disAddress the broadcast ip address
     */
    public DisViewSupport(int disPort, String disAddress) {
        super(disPort, disAddress);
    }

    /**
     * constructor passing in a listener
     * @param aListerner
     */
    public DisViewSupport(Object aListerner) {
        this();
        this.addListener(aListerner);
    }

    @Override
    public String getFileToRead() {
        return fileToRead;
    }

    @Override
    public void setFileToRead(String fileToRead) {
        this.fileToRead = fileToRead;
    }

    @Override
    public int getPdusPerFile() {
        return pdusPerFile;
    }

    @Override
    public void setPdusPerFile(int pdusPerFile) {
        this.pdusPerFile = pdusPerFile;
    }

    @Override
    public boolean isAlsoWriteToFile() {
        return alsoWriteToFile;
    }

    @Override
    public void setAlsoWriteToFile(boolean alsoWriteToFile) {
        this.alsoWriteToFile = alsoWriteToFile;
    }

    @Override
    public void setDirectoryName(String dirname) {
        if (dirname != null && !dirname.isEmpty()) {
            this.dirName = dirname;
        }
    }

    @Override
    public String getDirectoryName() {
        return this.dirName;
    }

    public LogWriter getPduWriter() {
        return pduWriter;
    }

    public void setPduWriter(LogWriter pduWriter) {
        this.pduWriter = pduWriter;
    }

    public Thread getWriterThread() {
        return writerThread;
    }

    public void setWriterThread(Thread writerThread) {
        this.writerThread = writerThread;
    }

    @Override
    public void run() {
        System.out.println("Reading pdu from the connection...");

        if (isAlsoWriteToFile()) {
            this.setPduWriter(new LogWriter(getDirectoryName()));
            this.setWriterThread(new Thread(this.getPduWriter()));
            this.getWriterThread().setDaemon(false);
            this.getWriterThread().start();
            Thread.yield();
        }

        try {
            // setup the reading of the datagram packets     
            datagramRetriever = new DatagramRetriever(this.getConnection());
            datagramRetriever.setActive(true);
        } catch (Exception ex) {
            System.out.println("..... error in DisViewSupport connection, maybe the port number is no good: " + ex.getMessage());
            this.fireAction(ACTION_COMMAND_FAIL);
            this.stop();
            return;
        }
        // launch the thread that will read the datagrams 
        new Thread(datagramRetriever).start();
        Thread.yield();

        this.setRunning(true);
        this.readFromSocket();
    }

    private void readFromSocket() {
        getPacketCounter().resetToZero();
        PduFactory pduFactory = new PduFactory();
        Pdu pdu;
        PduType pduTypeEnum = null;
        ArrayList<Pdu> pduList = new ArrayList<>(pduListSize);

        Vector newDatagrams = new Vector();

        while (this.isRunning()) {

            // check that we are still reading the datagrams
            if (!datagramRetriever.isActive()) {
                break;
            }
            // if want to temporarily pause 
            while (this.isPaused()) {
                try {
                    disSupportThread.sleep(getPauseCheckTime());
                } catch (InterruptedException e) {
                    System.err.println("Error in sleeping " + e);
                }
            }

            pduList.clear();

            // get a set of datagrams from the wire
            newDatagrams = datagramRetriever.getDatagrams();

            // create a pdu for each datagram and add it to the list of pdu
            for (int i = 0; i < newDatagrams.size(); i++) {
                DatagramPacket datagramPacket = (DatagramPacket) newDatagrams.get(i);
                ByteBuffer buff = ByteBuffer.wrap(datagramPacket.getData());

                // to try to speed things up, determine the type of the pdu before creating a pdu
                int pos = buff.position();
                if (pos + 2 > buff.limit()) {
                    continue;
                }
                buff.position(pos + 2);
                int pduType = buff.get() & 0xFF;
                buff.position(pos);
                // no good pdu type
                if (pduType < 0 || pduType > PduType.lookup.length) {
                    continue;
                }
                // if not a pdu type we want
                if (!getPduTypeList().contains(Integer.valueOf(pduType))) {
                    continue;
                }
   
                // this is to catch (before creation) any pdu type that is not in the lookup table
                try {
                    pduTypeEnum = PduType.lookup[pduType];
                } catch (Exception e) {
                    continue;  // unknown pdu type 
                }
 
                pdu = pduFactory.createPdu(buff);

                // check that the pdu is one we want and is within the augmented area 
                if (isPduWanted(pdu) && DisAreaMonitor.getInstance().isWithingAugmentedArea(pdu)) {
                    pduList.add(pdu);
                }
            }

            // add the pdus to the current list of pdus
            currentPduList.addAll(pduList);

            if (isAlsoWriteToFile()) {
                // if have enough pdus for one file
                if (currentPduList.size() >= getPdusPerFile()) {
                    // write the pdus to file               
                    List<Pdu> pdusToLog = currentPduList;
                    currentPduList = new ArrayList<Pdu>(getPdusPerFile());
                    pduWriter.setUnqueuedPdus(true);
                    pduWriter.addListToWriteQueue(pdusToLog);
                    pduWriter.setUnqueuedPdus(false);
                    Thread.yield();
                }
            }

            // give the pdus to the listerners
            for (Pdu aPdu : pduList) {
                getPacketCounter().increment();
                for (BehaviorConsumerIF theListener : this.pduListeners) {
                    if (this.isUseCopies()) {
                        theListener.receivePdu(pduFactory.createPdu(ByteBuffer.wrap(aPdu.marshal())));
                    } else {
                        theListener.receivePdu(aPdu);
                    }
                }
            }

            // wait a bit for refresh time
            try {
                disSupportThread.sleep(this.getRefreshTime());
            } catch (InterruptedException e) {
                System.err.println("Error in sleeping " + e);
            }
        }

    }

    private void flushPduToZipFile() {
        if (!currentPduList.isEmpty() && isAlsoWriteToFile()) {
            List<Pdu> pdusToLog = currentPduList;
            pduWriter.setUnqueuedPdus(true);
            pduWriter.addListToWriteQueue(pdusToLog);
            pduWriter.setUnqueuedPdus(false);
            Thread.yield();
        }
    }

    @Override
    public void stop() {
        flushPduToZipFile();
        super.stop();
        if (datagramRetriever != null) {
            datagramRetriever.stop();
        }
        if (pduWriter != null) {
            pduWriter.setWantToStop(true);
            writerThread.interrupt();  // force the pduWriter to get out of its loop 
        }
        System.out.println("..... reading and writing processes are stopped .....");
    }
}
