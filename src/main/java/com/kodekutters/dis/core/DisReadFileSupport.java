/**
 * DisReadFileSupport a class to support reading pdu from zip xml file.
 *
 * Code base is from various examples of reference:
 * http://open-dis.sourceforge.net/Open-DIS.html
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
package com.kodekutters.dis.core;

import com.kodekutters.dis.util.DisFileChecker;
import edu.nps.moves.dis.Pdu;
import edu.nps.moves.dis.PduContainer;
import edu.nps.moves.disenum.PduType;
import edu.nps.moves.disutil.PduFactory;
import edu.nps.moves.net.BehaviorConsumerIF;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class DisReadFileSupport extends DisSupport implements Runnable, DisFileSupport {

    public static String DATA_LOADED = "DATA_LOADED";
    private ZipFile zipFile = null;
    private List<Pdu> pduList = new ArrayList<Pdu>();
    protected String fileToRead = null;
    private PduFactory pduFactory = new PduFactory();

    public DisReadFileSupport() {
        super();
    }

    public DisReadFileSupport(Object aListerner) {
        this();
        this.addListener(aListerner);
    }

    public String getFileToRead() {
        return fileToRead;
    }

    public void setFileToRead(String fileToRead) {
        this.fileToRead = fileToRead;
    }

    @Override
    public void start() {
        fireAction(ACTION_COMMAND_START);
        disSupportThread = new Thread(this);
        disSupportThread.setName("DisReadFileSupportThread");
        disSupportThread.start();
    }

    @Override
    public void run() {
        loadData();
        disSupportThread.yield();
        fireAction(DATA_LOADED);
    }

    public void stop() {
        super.stop();
        pduList.clear();
    }

    public void loadData() {

        try {
            this.getPacketCounter().resetToZero();

            // another check of the file validity
            if (!DisFileChecker.isFileOk(this.getFileToRead())) {
                fireAction(ACTION_COMMAND_FAIL);
                return;
            }

            try {
                zipFile = new ZipFile(this.getFileToRead());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DisReadFileSupport.class.getName()).log(Level.SEVERE, null, ex);
                fireAction(ACTION_COMMAND_FAIL);
                return;
            }

            // for each file entry in the zip file
            Enumeration elist = zipFile.entries();
            while (elist.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) elist.nextElement();

                JAXBContext context = JAXBContext.newInstance(
                        edu.nps.moves.dis.MinefieldStatePdu.class,
                        edu.nps.moves.dis.AcknowledgeReliablePdu.class,
                        edu.nps.moves.dis.SyntheticEnvironmentFamilyPdu.class,
                        edu.nps.moves.dis.DesignatorPdu.class,
                        edu.nps.moves.dis.AcousticBeamData.class,
                        edu.nps.moves.dis.AcousticEmitterSystemData.class,
                        edu.nps.moves.dis.FundamentalParameterDataIff.class,
                        edu.nps.moves.dis.Relationship.class,
                        edu.nps.moves.dis.EntityManagementFamilyPdu.class,
                        edu.nps.moves.dis.FastEntityStatePdu.class,
                        edu.nps.moves.dis.DataReliablePdu.class,
                        edu.nps.moves.dis.BurstDescriptor.class,
                        edu.nps.moves.dis.EntityInformationFamilyPdu.class,
                        edu.nps.moves.dis.IffAtcNavAidsLayer2Pdu.class,
                        edu.nps.moves.dis.NamedLocation.class,
                        edu.nps.moves.dis.TransferControlRequestPdu.class,
                        edu.nps.moves.dis.EightByteChunk.class,
                        edu.nps.moves.dis.ElectronicEmissionsPdu.class,
                        edu.nps.moves.dis.CreateEntityReliablePdu.class,
                        edu.nps.moves.dis.LinearSegmentParameter.class,
                        edu.nps.moves.dis.EventReportPdu.class,
                        edu.nps.moves.dis.MinefieldResponseNackPdu.class,
                        edu.nps.moves.dis.FourByteChunk.class,
                        edu.nps.moves.dis.EntityStateUpdatePdu.class,
                        edu.nps.moves.dis.EnvironmentalProcessPdu.class,
                        edu.nps.moves.dis.GridAxisRecord.class,
                        edu.nps.moves.dis.IntercomCommunicationsParameters.class,
                        edu.nps.moves.dis.LinearObjectStatePdu.class,
                        edu.nps.moves.dis.EventReportReliablePdu.class,
                        edu.nps.moves.dis.ArealObjectStatePdu.class,
                        edu.nps.moves.dis.AcousticBeamFundamentalParameter.class,
                        edu.nps.moves.dis.Vector3Float.class,
                        edu.nps.moves.dis.StopFreezePdu.class,
                        edu.nps.moves.dis.ArticulationParameter.class,
                        edu.nps.moves.dis.FixedDatum.class,
                        edu.nps.moves.dis.CommentReliablePdu.class,
                        edu.nps.moves.dis.ServiceRequestPdu.class,
                        edu.nps.moves.dis.IsGroupOfPdu.class,
                        edu.nps.moves.dis.UaPdu.class,
                        edu.nps.moves.dis.PointObjectStatePdu.class,
                        edu.nps.moves.dis.FundamentalParameterData.class,
                        edu.nps.moves.dis.DataQueryReliablePdu.class,
                        edu.nps.moves.dis.SetRecordReliablePdu.class,
                        edu.nps.moves.dis.ElectronicEmissionBeamData.class,
                        edu.nps.moves.dis.DetonationPdu.class,
                        edu.nps.moves.dis.RecordSet.class,
                        edu.nps.moves.dis.ActionResponsePdu.class,
                        edu.nps.moves.dis.ShaftRPMs.class,
                        edu.nps.moves.dis.ActionRequestPdu.class,
                        edu.nps.moves.dis.IsPartOfPdu.class,
                        edu.nps.moves.dis.DeadReckoningParameter.class,
                        edu.nps.moves.dis.GridAxisRecordRepresentation2.class,
                        edu.nps.moves.dis.MinefieldQueryPdu.class,
                        edu.nps.moves.dis.SystemID.class,
                        edu.nps.moves.dis.MinefieldDataPdu.class,
                        edu.nps.moves.dis.SimulationAddress.class,
                        edu.nps.moves.dis.EntityID.class,
                        edu.nps.moves.dis.AntennaLocation.class,
                        edu.nps.moves.dis.DataQueryPdu.class,
                        edu.nps.moves.dis.EntityType.class,
                        edu.nps.moves.dis.IffAtcNavAidsLayer1Pdu.class,
                        edu.nps.moves.dis.GridAxisRecordRepresentation1.class,
                        edu.nps.moves.dis.BeamAntennaPattern.class,
                        edu.nps.moves.dis.ResupplyReceivedPdu.class,
                        edu.nps.moves.dis.Orientation.class,
                        edu.nps.moves.dis.Pdu.class,
                        edu.nps.moves.dis.ReceiverPdu.class,
                        edu.nps.moves.dis.AggregateType.class,
                        edu.nps.moves.dis.Environment.class,
                        edu.nps.moves.dis.WarfareFamilyPdu.class,
                        edu.nps.moves.dis.StopFreezeReliablePdu.class,
                        edu.nps.moves.dis.SetDataReliablePdu.class,
                        edu.nps.moves.dis.CommentPdu.class,
                        edu.nps.moves.dis.SimulationManagementWithReliabilityFamilyPdu.class,
                        edu.nps.moves.dis.CollisionPdu.class,
                        edu.nps.moves.dis.IffFundamentalData.class,
                        edu.nps.moves.dis.VariableDatum.class,
                        edu.nps.moves.dis.FirePdu.class,
                        edu.nps.moves.dis.SetDataPdu.class,
                        edu.nps.moves.dis.AcousticEmitterSystem.class,
                        edu.nps.moves.dis.RemoveEntityReliablePdu.class,
                        edu.nps.moves.dis.RepairCompletePdu.class,
                        edu.nps.moves.dis.CreateEntityPdu.class,
                        edu.nps.moves.dis.IntercomControlPdu.class,
                        edu.nps.moves.dis.ModulationType.class,
                        edu.nps.moves.dis.SixByteChunk.class,
                        edu.nps.moves.dis.IntercomSignalPdu.class,
                        edu.nps.moves.dis.AggregateID.class,
                        edu.nps.moves.dis.StartResumePdu.class,
                        edu.nps.moves.dis.ActionResponseReliablePdu.class,
                        edu.nps.moves.dis.SimulationManagementFamilyPdu.class,
                        edu.nps.moves.dis.SphericalHarmonicAntennaPattern.class,
                        edu.nps.moves.dis.ElectronicEmissionSystemData.class,
                        edu.nps.moves.dis.LayerHeader.class,
                        edu.nps.moves.dis.Point.class,
                        edu.nps.moves.dis.RecordQueryReliablePdu.class,
                        edu.nps.moves.dis.ResupplyOfferPdu.class,
                        edu.nps.moves.dis.ClockTime.class,
                        edu.nps.moves.dis.SignalPdu.class,
                        edu.nps.moves.dis.RadioEntityType.class,
                        edu.nps.moves.dis.AggregateStatePdu.class,
                        edu.nps.moves.dis.StartResumeReliablePdu.class,
                        edu.nps.moves.dis.DistributedEmissionsFamilyPdu.class,
                        edu.nps.moves.dis.TrackJamTarget.class,
                        edu.nps.moves.dis.AngularVelocityVector.class,
                        edu.nps.moves.dis.RepairResponsePdu.class,
                        edu.nps.moves.dis.Vector3Double.class,
                        edu.nps.moves.dis.AcknowledgePdu.class,
                        edu.nps.moves.dis.EntityStatePdu.class,
                        edu.nps.moves.dis.RemoveEntityPdu.class,
                        edu.nps.moves.dis.PduContainer.class,
                        edu.nps.moves.dis.GriddedDataPdu.class,
                        edu.nps.moves.dis.VectoringNozzleSystemData.class,
                        edu.nps.moves.dis.DataPdu.class,
                        edu.nps.moves.dis.ActionRequestReliablePdu.class,
                        edu.nps.moves.dis.EmitterSystem.class,
                        edu.nps.moves.dis.GridAxisRecordRepresentation0.class,
                        edu.nps.moves.dis.SeesPdu.class,
                        edu.nps.moves.dis.MinefieldFamilyPdu.class,
                        edu.nps.moves.dis.ObjectType.class,
                        edu.nps.moves.dis.ApaData.class,
                        edu.nps.moves.dis.BeamData.class,
                        edu.nps.moves.dis.PropulsionSystemData.class,
                        edu.nps.moves.dis.OneByteChunk.class,
                        edu.nps.moves.dis.AcousticEmitter.class,
                        edu.nps.moves.dis.TransmitterPdu.class,
                        edu.nps.moves.dis.EventID.class,
                        edu.nps.moves.dis.Marking.class,
                        edu.nps.moves.dis.AggregateMarking.class,
                        edu.nps.moves.dis.CollisionElasticPdu.class,
                        edu.nps.moves.dis.TwoByteChunk.class,
                        edu.nps.moves.dis.ResupplyCancelPdu.class,
                        edu.nps.moves.dis.RadioCommunicationsFamilyPdu.class,
                        edu.nps.moves.dis.LogisticsFamilyPdu.class,
                        edu.nps.moves.dis.SupplyQuantity.class);

                Unmarshaller unmarshaller = context.createUnmarshaller();
                PduContainer unmarshalledObject = unmarshalFromZipFile(unmarshaller, this.getFileToRead(), zipEntry);

//                pduList.addAll(unmarshalledObject.getPdus());

                filterPdu(unmarshalledObject.getPdus());

                this.getPacketCounter().setValue((long) pduList.size());
            }

        } catch (Exception e) {
            System.err.println("Error in DisReadFileSupport replayList " + e);
        }
        try {
            if (zipFile != null) {
                zipFile.close();
            }
        } catch (IOException ex) {
        }
    }

    // take only espdu ...... for testing re timestamp them to a simple sequence 
    private void filterPdu(List<Pdu> list) {
        long counter = 0L;
        for (Pdu pdu : list) {
            if ((pdu.getPduTypeEnum() == PduType.ENTITY_STATE) || (pdu.getPduTypeEnum() == PduType.ENTITY_STATE_UPDATE)) {
                pdu.setTimestamp(counter++);
                pduList.add(pdu);
            }
        }
    }

    public List<Pdu> getPduList() {
        return pduList;
    }

    public ZipFile getZipFile() {
        return zipFile;
    }

    private PduContainer unmarshalFromZipFile(Unmarshaller unmarshaller, String fileName, ZipEntry entry) {
        PduContainer unmarshalledObject = null;
        try {
            unmarshalledObject = (PduContainer) unmarshaller.unmarshal(new BufferedInputStream(zipFile.getInputStream(entry)));
        } catch (IOException iOException) {
            System.err.println("in DisReadFileSupport io exception " + iOException);
        } catch (JAXBException jAXBException) {
            System.err.println("in DisReadFileSupport JAXB exception --> " + jAXBException);
        }
        System.out.println("processing zip file: " + fileName + " ... file entry: " + entry.getName() + " ... with: " + unmarshalledObject.getNumberOfPdus() + " pdu");
        return unmarshalledObject;
    }

    public void replayListAt(int idx) {
        Pdu aPdu = (Pdu) pduList.get(idx);
        // give the pdu to the listerners
        for (BehaviorConsumerIF theListener : this.pduListeners) {
            if (this.isUseCopies()) {
                theListener.receivePdu(pduFactory.createPdu(ByteBuffer.wrap(aPdu.marshal())));
            } else {
                theListener.receivePdu(aPdu);
            }
        }
    }

    @Override
    public int getPdusPerFile() {
        return 0;
    }

    @Override
    public void setPdusPerFile(int pdusPerFile) {
    }

    @Override
    public boolean isAlsoWriteToFile() {
        return false;
    }

    @Override
    public void setAlsoWriteToFile(boolean alsoWriteToFile) {
    }
}
