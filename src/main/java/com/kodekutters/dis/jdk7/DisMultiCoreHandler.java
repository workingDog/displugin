package com.kodekutters.dis.jdk7;

//
///**
// * <p>DisMultiCoreHandler </p> <p>handles taking DIS pdu from the transfer queue
// * and passing them to the list of listeners</p>
// *
// * @author Ringo Wathelet
// * @version 1.0, 2012
// */
//import dis.core.DisViewSupport;
//import edu.nps.moves.dis.Pdu;
//import edu.nps.moves.net.BehaviorConsumerIF;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.TransferQueue;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//public class DisMultiCoreHandler extends DisViewSupport implements Runnable {
//
//    // the queue where the pdus arrive and are taken from 
//    private final TransferQueue queue;
//
//    public DisMultiCoreHandler(TransferQueue q) {
//        queue = q;
//    }
//
//    public DisMultiCoreHandler(TransferQueue q, List<BehaviorConsumerIF> theListeners) {
//        queue = q;
//        for (BehaviorConsumerIF aListerner : theListeners) {
//            this.addListener(aListerner);
//        }
//    }
//
//    @Override
//    public void run() {
//        this.setIsRunning(true);
//        currentPduList = new ArrayList<Pdu>(getPdusPerFile());
//        while (this.isRunning()) {
//            Pdu thePdu = null;
//            try {
//                // the pdu is taken from the queue
//                thePdu = (Pdu) queue.take();
//            } catch (InterruptedException ex) {
//                Logger.getLogger(DisMultiCoreHandler.class.getName()).log(Level.SEVERE, null, ex);
//                continue;
//            }
//            if (thePdu != null) {
//                currentPduList.add(thePdu);
//                if (isAlsoWriteToFile()) {
//                    // if have enough pdus for one file
//                    if (currentPduList.size() >= getPdusPerFile()) {
//                        // write the pdus to file               
//                        List<Pdu> pdusToLog = currentPduList;
//                        currentPduList = new ArrayList<Pdu>(getPdusPerFile());
//                        pduWriter.setUnqueuedPdus(true);
//                        pduWriter.addListToWriteQueue(pdusToLog);
//                        pduWriter.setUnqueuedPdus(false);
//                        Thread.yield();
//                    }
//                }
//                // give the pdu to the listeners
//                for (BehaviorConsumerIF theListener : this.pduListeners) {
//                    theListener.receivePdu(thePdu);
//                }
//            }
//        }
//    }
//}
