package com.kodekutters.dis.jdk7;

//
///**
// * <p>DisMultiCoreReader </p> <p>reads DIS pdus and put them on the transfer
// * queue </p>
// *
// * @author Ringo Wathelet
// * @version 1.0, 2012
// */
//import dis.connection.DatagramRetriever;
//import dis.core.DisViewSupport;
//import dis.core.LogWriter;
//import edu.nps.moves.dis.Pdu;
//import edu.nps.moves.disenum.PduType;
//import edu.nps.moves.disutil.PduFactory;
//import java.net.DatagramPacket;
//import java.nio.ByteBuffer;
//import java.util.Vector;
//import java.util.concurrent.TransferQueue;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//public class DisMultiCoreReader extends DisViewSupport implements Runnable {
//
//    DatagramRetriever datagramRetriever;
//    // the tranferQueue where the pdus will arrive and be taken from
//    private final TransferQueue tranferQueue;
//
//    /**
//     * constructor
//     *
//     * @param disPort the port number
//     * @param disAddress the broadcast ip address
//     * @param the transfer queue
//     */
//    public DisMultiCoreReader(int disPort, String disAddress, TransferQueue q) {
//        super(disPort, disAddress);
//        tranferQueue = q;
//    }
//
//    @Override
//    public void run() {
//        System.out.println("Reading pdu from the connection...");
//        // read the selected area
//        readSelectedArea();
//
//        if (isAlsoWriteToFile()) {
//            setPduWriter(new LogWriter(getDirectoryName()));
//            this.setWriterThread(new Thread(this.getPduWriter()));
//            this.getWriterThread().setDaemon(false);
//            this.getWriterThread().start();
//            Thread.yield();
//        }
//
//        try {
//            // setup the reading of the datagram packets     
//            datagramRetriever = new DatagramRetriever(this.getConnection());
//            datagramRetriever.setIsActive(true);
//        } catch (Exception ex) {
//            System.out.println("..... error in DisMultiCoreReader connection, maybe the port number is no good: " + ex.getMessage());
//            this.fireAction(ACTION_COMMAND_FAIL);
//            this.stop();
//            return;
//        }
//        // launch the thread that will read the datagrams 
//        new Thread(datagramRetriever).start();
//        Thread.yield();
//
//        this.setIsRunning(true);
//        this.readFromSocket();
//    }
//
//    protected void readFromSocket() {
//        PduFactory pduFactory = new PduFactory();
//        Pdu pdu;
//
//        Vector newDatagrams = new Vector();
//
//        while (this.isRunning()) {
//
//            // check that we are still reading the datagrams
//            if (!datagramRetriever.isActive()) {
//                break;
//            }
//            // if want to temporarily pause 
//            while (this.isPaused()) {
//                try {
//                    disSupportThread.sleep(getPauseCheckTime());
//                } catch (InterruptedException e) {
//                    System.err.println("Error in sleeping " + e);
//                }
//            }
//
//            // get a set of datagrams from the wire
//            newDatagrams = datagramRetriever.getDatagrams();
//
//            // create a pdu for each datagram and add it to the list of pdu
//            for (int i = 0; i < newDatagrams.size(); i++) {
//                DatagramPacket datagramPacket = (DatagramPacket) newDatagrams.get(i);
//                ByteBuffer buff = ByteBuffer.wrap(datagramPacket.getData());
//
//                // to speed things up, determine the type of the pdu before creating a pdu
//                int pos = buff.position();
//                if (pos + 2 > buff.limit()) {
//                    continue;
//                }
//                buff.position(pos + 2);
//                int pduType = buff.get() & 0xFF;
//                buff.position(pos);
//                // no good pdu types
//                if (pduType < 0 || pduType > PduType.lookup.length) {
//                    continue;
//                }
//                // if not a pdu type we want
//                if (!getPduTypeList().contains(Integer.valueOf(pduType))) {
//                    continue;
//                }
//
//                pdu = pduFactory.createPdu(buff);
//
//                // further check that the pdu is one we want and is within the selected area 
//                if (isPduWanted(pdu) && isWithingArea(pdu)) {
//                    try {
//                        // the pdu are put in the queue, they will be taken off by the DisMultiCoreHandlers
//                        tranferQueue.tryTransfer(pdu);
//                    } catch (Exception ex) {
//                        Logger.getLogger(DisMultiCoreReader.class.getName()).log(Level.SEVERE, null, ex);
//                        System.err.println("in DisMultiCoreReader error in processing pdu");
//                    }
//                }
//            }
//
//            // wait a bit for refresh time
//            try {
//                disSupportThread.sleep(this.getRefreshTime());
//            } catch (InterruptedException e) {
//                System.err.println("Error in sleeping " + e);
//            }
//        }
//    }
//}
