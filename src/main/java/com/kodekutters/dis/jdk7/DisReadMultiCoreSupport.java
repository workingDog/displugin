package com.kodekutters.dis.jdk7;

//
///**
// * <p>DisReadMultiCoreSupport </p> <p>reads DIS pdus using DisMultiCoreReader
// * that puts them into a transfer queue and </p> <p> set up a set of
// * DisMultiCoreHandlers in separate threads to take the pdus from the queue </p>
// * <p> and pass them to the listeners </p>
// *
// * @author Ringo Wathelet
// * @version 1.0, 2012
// */
//import dis.core.DisSupport;
//import dis.core.DisViewSupport;
//import java.util.Vector;
//import java.util.concurrent.LinkedTransferQueue;
//
//public class DisReadMultiCoreSupport extends DisViewSupport {
//
//    // the queue where the pdus will arrive and be taken from
//    private final LinkedTransferQueue tranferQueue = new LinkedTransferQueue();
//    private final Vector<DisMultiCoreHandler> disHandlerList = new Vector<DisMultiCoreHandler>();
//    private DisMultiCoreReader thePduReader;
//
//    public DisReadMultiCoreSupport(Object aListerner) {
//        super();
//        this.addListener(aListerner);
//    }
//
//    @Override
//    public void run() {
//        // put 8 threads on each processor
//        int nThreads = Runtime.getRuntime().availableProcessors() * 8;
//        this.setIsRunning(true);
//        // the reader that will read pdus and put them on the tranferQueue
//        thePduReader = new DisMultiCoreReader(
//                this.getConnection().getPort(),
//                this.getConnection().getAddress(), tranferQueue);
//
//        thePduReader.fireAction(DisSupport.ACTION_COMMAND_START);
//        // put the reader into its own thread and start it, that will put the pdus onto the tranferQueue
//        new Thread(thePduReader).start();
//
//        getPacketCounter().resetToZero();
//
//        // create and start the handlers, that will take the pdus from the tranferQueue and pass them to the pduListeners
//        for (int i = 0; i < nThreads; i++) {
//            DisMultiCoreHandler handler = new DisMultiCoreHandler(tranferQueue, this.pduListeners);
//            disHandlerList.add(handler);
//            // create a new thread for each handler
//            new Thread(handler).start();
//        }
//    }
//
//    @Override
//    public void stop() {
//        super.stop();
//        thePduReader.stop();
//        for (DisMultiCoreHandler handler : disHandlerList) {
//            handler.stop();
//        }
//    }
//}
