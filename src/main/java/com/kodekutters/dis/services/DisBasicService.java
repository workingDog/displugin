package com.kodekutters.dis.services;

import com.kodekutters.dis.core.DisSupport;
import edu.nps.moves.dis.Pdu;
import edu.nps.moves.net.BehaviorConsumerIF;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;

/**
 * DisBasicService an implementation of the abstract DisService
 *
 * @author R. Wathelet
 * @version 1.0
 */
@ServiceProvider(service = DisService.class)
public class DisBasicService extends DisService {

    private static final Logger LOG = Logger.getLogger(DisBasicService.class.getName());
    protected DisSupport disHandler = new DisSupport();
    private BehaviorConsumerIF autoArmListener = null;

    public DisBasicService() {

        // automatic start and stop processes
        autoArmListener = new BehaviorConsumerIF() {

            @Override
            public void receivePdu(Pdu pdu) {
                try {
                    switch (pdu.getPduTypeEnum()) {
                        case START_RESUME:
                            start();
                            break;
                        case STOP_FREEZE:
                            stop();
                            break;
                    }
                } catch (Exception e) {
                    LOG.log(Level.WARNING, "Problem receivePdu ", e);
                }
            }
        };

    }

    @Override
    public DisSupport getDisHandler() {
        return disHandler;
    }

    @Override
    public void setDisHandler(Object handler) {
        this.disHandler = (DisSupport) handler;
    }

    @Override
    public void start() {
        fireStartEvent();
        this.setDuration(0);
        this.getInfoTimer().start();
        // if already have a DisHandler running
        if (this.getDisHandler().isRunning()) {
            // stop the old thread first
            this.getDisHandler().stop();
        }
        // start 
        this.getDisHandler().getPacketCounter().resetToZero();
        this.getDisHandler().start();
    }

    @Override
    public void pause() {
        firePauseEvent();
        this.getInfoTimer().stop();
        if (this.getDisHandler().isRunning()) {
            this.getDisHandler().setPaused(true);
        }
    }

    @Override
    public void stop() {
        fireStopEvent();
        this.getInfoTimer().stop();
        if (this.getDisHandler().isRunning()) {
            this.getDisHandler().stop();
        }
    }

    @Override
    public void autoStart() {
        fireAutoStartEvent();
        this.setDuration(0);
        this.getInfoTimer().start();
        if (!this.getDisHandler().getPduListeners().contains(autoArmListener)) {
            this.getDisHandler().addListener(autoArmListener);
        }
    }

    @Override
    public void autoStop() {
        fireAutoStopEvent();
        this.getInfoTimer().stop();
        if (this.getDisHandler().getPduListeners().contains(autoArmListener)) {
            this.getDisHandler().removeListener(autoArmListener);
        }
    }

    @Override
    public void restart() {
        fireRestartEvent();
        this.getInfoTimer().restart();
        if (this.getDisHandler().isRunning()) {
            this.getDisHandler().setPaused(false);
        }
    }
}
