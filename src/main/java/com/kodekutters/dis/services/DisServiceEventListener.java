package com.kodekutters.dis.services;

import java.util.EventListener;

/**
 * DisServiceEventListener
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
public interface DisServiceEventListener extends EventListener {

    public void starting();
    
    public void restarting();

    public void stopping();

    public void pausing();

    public void autoStarting();

    public void autoStopping();
}
