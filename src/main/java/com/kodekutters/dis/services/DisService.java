package com.kodekutters.dis.services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.event.EventListenerList;
import org.openide.util.Lookup;

/**
 * DisService
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
public abstract class DisService implements ActionListener {

    private final EventListenerList listeners = new EventListenerList();
    private int duration = 0;
    private Timer infoTimer = null;

    public static DisService getDefault() {
        DisService service = Lookup.getDefault().lookup(DisService.class);
        if (service == null) {
            service = new DefaultDisService();
        }

        return service;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Timer getInfoTimer() {
        return infoTimer;
    }

    public void setInfoTimer(Timer infoTimer) {
        this.infoTimer = infoTimer;
    }

    public void addEventListener(DisServiceEventListener listener) {
        listeners.add(DisServiceEventListener.class, listener);
    }

    public void removeEventListener(DisServiceEventListener listener) {
        listeners.remove(DisServiceEventListener.class, listener);
    }

    protected final void fireStartEvent() {
        for (DisServiceEventListener listener : listeners.getListeners(DisServiceEventListener.class)) {
            listener.starting();
        }
    }

    protected final void fireRestartEvent() {
        for (DisServiceEventListener listener : listeners.getListeners(DisServiceEventListener.class)) {
            listener.restarting();
        }
    }

    protected final void fireStopEvent() {
        for (DisServiceEventListener listener : listeners.getListeners(DisServiceEventListener.class)) {
            listener.stopping();
        }
    }

    protected final void firePauseEvent() {
        for (DisServiceEventListener listener : listeners.getListeners(DisServiceEventListener.class)) {
            listener.pausing();
        }
    }

    protected final void fireAutoStartEvent() {
        for (DisServiceEventListener listener : listeners.getListeners(DisServiceEventListener.class)) {
            listener.autoStarting();
        }
    }

    protected final void fireAutoStopEvent() {
        for (DisServiceEventListener listener : listeners.getListeners(DisServiceEventListener.class)) {
            listener.autoStopping();
        }
    }

    public abstract void start();

    public abstract void restart();

    public abstract void pause();

    public abstract void stop();

    public abstract void autoStart();

    public abstract void autoStop();

    public abstract Object getDisHandler();

    public abstract void setDisHandler(Object handler);

    @Override
    public void actionPerformed(ActionEvent ae) {
        this.setDuration(this.getDuration() + this.getInfoTimer().getDelay() / 1000);
        this.getInfoTimer().restart();
    }

    private static class DefaultDisService extends DisService {

        @Override
        public void start() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void restart() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void pause() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void stop() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void autoStart() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void autoStop() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public int getDuration() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void setDuration(int d) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Timer getInfoTimer() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void setInfoTimer(Timer infoTimer) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Object getDisHandler() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void setDisHandler(Object handler) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
