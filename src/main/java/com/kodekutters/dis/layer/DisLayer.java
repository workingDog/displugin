package com.kodekutters.dis.layer;

import com.kodekutters.dis.core.DisSupport;
import com.kodekutters.dis.entity.Entity;
import com.kodekutters.dis.entity.EntityBeanNode;
import com.kodekutters.dis.system.EntityExplorerManager;
import com.kodekutters.dis.util.DisLayerEventListener;
import com.kodekutters.dis.util.DisAreaMonitor;
import edu.nps.moves.dis.EntityStatePdu;
import edu.nps.moves.dis.Pdu;
import edu.nps.moves.net.BehaviorConsumerIF;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.event.SelectListener;
import gov.nasa.worldwind.layers.AnnotationLayer;
import gov.nasa.worldwind.layers.RenderableLayer;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 * DisLayer a base layer for displaying DIS pdu
 *
 * @author R. Wathelet, March 2012
 * @version 1.0
 */
public class DisLayer extends RenderableIconLayer implements BehaviorConsumerIF {

    private final ExplorerManager mgr = EntityExplorerManager.SHARED_ENTITY_MANAGER;
    private DisSupport disSupport;
    private final AnnotationLayer anoLayer = new AnnotationLayer();
    private final RenderableLayer historyLayer = new RenderableLayer();
    private SelectListener anoSelection;
    private final ArrayList<DisLayerEventListener> listeners = new ArrayList<>();

    public DisLayer(String name) {
        super();
        this.setName(name);
        anoLayer.setPickEnabled(true);
        historyLayer.setPickEnabled(false);
    }

    public void setName(String name) {
        super.setName(name);
        if (getAnnotationLayer() != null) {
            getAnnotationLayer().setName(name + " annotations");
        }
        if (getHistoryLayer() != null) {
            getHistoryLayer().setName(name + " history");
        }
    }

    @Override
    public void receivePdu(Pdu pdu) {

        try {
            switch (pdu.getPduTypeEnum()) {
                case ENTITY_STATE:
                case ENTITY_STATE_UPDATE:
//                    System.out.println("ENTITY_STATE");
                    this.updateLayer((EntityStatePdu) pdu);
                    break;

                case REMOVE_ENTITY_R:
                case REMOVE_ENTITY:
//                    System.out.println("REMOVE_ENTITY");
                    this.removeEntity((EntityStatePdu) pdu);
                    break;

                case COMMENT:
//                    System.out.println("COMMENT");
                    break;

                case FIRE:
//                    System.out.println("FIRE");
                    break;

                case DETONATION:
//                    System.out.println("DETONATION");
                    break;

                case COLLISION:
//                    System.out.println("COLLISION");
                    break;

                case SERVICE_REQUEST:
//                    System.out.println("SERVICE_REQUEST");
                    break;

                case RESUPPLY_OFFER:
//                    System.out.println("RESUPPLY_OFFER");
                    break;

                case RESUPPLY_RECEIVED:
//                    System.out.println("RESUPPLY_RECEIVED");
                    break;

                case RESUPPLY_CANCEL:
//                    System.out.println("RESUPPLY_CANCEL");
                    break;

                case REPAIR_COMPLETE:
//                    System.out.println("REPAIR_COMPLETE");
                    break;

                case REPAIR_RESPONSE:
//                    System.out.println("REPAIR_RESPONSE");
                    break;

                case CREATE_ENTITY:
//                    System.out.println("CREATE_ENTITY");
                    break;

                case START_RESUME:
//                    System.out.println("START_RESUME");
                    break;

                case STOP_FREEZE:
//                    System.out.println("STOP_FREEZE");
                    break;

                case ACKNOWLEDGE:
//                    System.out.println("ACKNOWLEDGE");
                    break;

                case ACTION_REQUEST:
//                    System.out.println("ACTION_REQUEST");
                    break;

                default:
//                    System.out.println("non standard pdu received...ignoring it");
            }
        } catch (Exception e) {
            Logger.getLogger(DisLayer.class.getName()).log(Level.WARNING, "Problem processing PDU type", e);
        }
    }

    public void updateLayer(EntityStatePdu espdu) {
        Integer entityId = Entity.getHashcodeID(espdu);
        // if already have the entity just update its state
        if (this.getIconList().containsKey(entityId)) {
            Entity theEntity = (Entity) this.getIconList().get(entityId);
            theEntity.setEspdu(espdu);
            // now check that the entity would be within the selected area, remove it if not
            if (!DisAreaMonitor.getInstance().isWithingArea(theEntity)) {
                this.removeEntity(theEntity);
            }
        } else {
            // create the entity only if inside the area
            if (DisAreaMonitor.getInstance().isWithingArea(espdu)) {
                // create a new entity and add it to the layer
                this.addEntity(new Entity(espdu));
            }
        }
        this.firePropertyChange(AVKey.LAYER, true, false);
    }

    public synchronized void addEventListener(DisLayerEventListener listener) {
        listeners.add(listener);
    }

    public synchronized void removeEventListener(DisLayerEventListener listener) {
        listeners.remove(listener);
    }

    public RenderableLayer getHistoryLayer() {
        return historyLayer;
    }

    public AnnotationLayer getAnnotationLayer() {
        return anoLayer;
    }

    public SelectListener getSelectListener() {
        return anoSelection;
    }

    public void setSelectListener(SelectListener listener) {
        this.anoSelection = listener;
    }

    public DisSupport getDisSupport() {
        return this.disSupport;
    }

    public void setDisSupport(DisSupport disReadSupport) {
        this.disSupport = disReadSupport;
        this.disSupport.addListener(this);
    }

    public void removeEntity(Entity theEntity) {
        this.removeEntity(theEntity.getEspdu());
    }

    public void removeEntity(EntityStatePdu espdu) {
        Integer entityId = Entity.getHashcodeID(espdu);
        if (this.getIconList().containsKey(entityId)) {
            Entity theEntity = this.getIconList().get(entityId);
            this.fireRemoveEntityEvent(theEntity);
            getHistoryLayer().removeRenderable(theEntity.getHistoryPath());
            getAnnotationLayer().removeAnnotation(theEntity.getAnnotation());
            this.removeRenderable(theEntity);
            this.removeIcon(theEntity);
        }
    }

    public void resetLayer() {
        this.removeAllIcons();
        this.removeAllRenderables();
        getAnnotationLayer().removeAllAnnotations();
        getHistoryLayer().removeAllRenderables();
        this.fireResetLayerEvent();
    }

    public void addEntity(Entity entity) {
        this.addIcon(entity);
        this.addRenderable(entity);
        getHistoryLayer().addRenderable(entity.getHistoryPath());
        getAnnotationLayer().addAnnotation(entity.getAnnotation());
        this.fireAddEntityEvent(entity);

        // this is not the recommended way....
        try {
            Node theNode = new EntityBeanNode(entity);
            theNode.setDisplayName(entity.getDisplayInfo());
            mgr.getRootContext().getChildren().add(new Node[]{theNode});
        } catch (IntrospectionException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    protected final void fireRemoveEntityEvent(Entity entity) {
        for (DisLayerEventListener listener : listeners) {
            listener.removeEntity(entity);
        }
    }

    protected final void fireAddEntityEvent(Entity entity) {
        for (DisLayerEventListener listener : listeners) {
            listener.addEntity(entity);
        }
    }

    protected final void fireResetLayerEvent() {
        for (DisLayerEventListener listener : listeners) {
            listener.resetLayer();
        }
    }
}
