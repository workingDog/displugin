/**
 * DisAARLayer a layer for DIS after action replay
 *
 *
 * @author R. Wathelet
 * @version 1.0
 */
package com.kodekutters.dis.layer;

import com.kodekutters.dis.entity.Entity;
import edu.nps.moves.dis.EntityStatePdu;
import gov.nasa.worldwind.avlist.AVKey;

public class DisAARLayer extends DisLayer {

    public DisAARLayer(String name) {
        super(name);
    }

    @Override
    public void updateLayer(EntityStatePdu espdu) {
        Integer entityId = Entity.getHashcodeID(espdu);
        // if already have the entity just update its state
        if (this.getIconList().containsKey(entityId)) {
            ((Entity) this.getIconList().get(entityId)).setEspdu(espdu);
        } else {
            // create a new entity and add it to the layer, also turn off the history recording
            this.addEntity(new Entity(espdu, true));
        }
        this.firePropertyChange(AVKey.LAYER, true, false);
    }
}
