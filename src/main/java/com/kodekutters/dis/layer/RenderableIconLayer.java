/*
 Copyright (C) 2001, 2006 United States Government
 as represented by the Administrator of the
 National Aeronautics and Space Administration.
 All Rights Reserved.
 */
package com.kodekutters.dis.layer;

import com.kodekutters.dis.entity.Entity;
import com.kodekutters.dis.entity.EntityRenderer;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.*;
import gov.nasa.worldwind.util.Logging;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The
 * <code>IconLayer</code> class manages a collection of {@link gov.nasa.worldwind.render.Entity}
 * objects for rendering and picking.
 * <code>IconLayer</code> delegates to its internal {@link gov.nasa.worldwind.render.IconRenderer}
 * for rendering and picking operations.
 *
 * @author tag
 * @version $Id: IconLayer.java 7762 2008-11-19 19:07:14Z tgaskins $
 * @see gov.nasa.worldwind.render.Entity
 * @see gov.nasa.worldwind.render.IconRenderer
 *
 * modified by R. Wathelet to use Entity with its associated EntityRenderer
 * instead of WWIcon, extends RenderableLayer instead of AbstractLayer, use
 * private final Map<String, Entity> icons = new ConcurrentHashMap<String,
 * Entity>(); and added Map<String, Entity> getIconList() added
 * super.doRender(dc) in doRender(dc)
 */
public class RenderableIconLayer extends RenderableLayer {

    private final Map<Integer, Entity> icons = new ConcurrentHashMap<Integer, Entity>();
    private Iterable<Entity> iconsOverride;
    private EntityRenderer iconRenderer = new EntityRenderer();
    private Pedestal pedestal;

    /**
     * Creates a new
     * <code>RenderableIconLayer</code> with an empty collection of Icons.
     */
    public RenderableIconLayer() {
        super();
    }

    public EntityRenderer getIconRenderer() {
        return this.iconRenderer;
    }

    /**
     * Adds the specified
     * <code>icon</code> to this layer's internal collection. If this layer's
     * internal collection has been overriden with a call to {@link #setIcons},
     * this will throw an exception.
     *
     * @param icon Icon to add.
     * @throws IllegalArgumentException If
     * <code>icon</code> is null.
     * @throws IllegalStateException If a custom Iterable has been specified by
     * a call to
     * <code>setIcons</code>.
     */
    public void addIcon(Entity icon) {
        if (icon == null) {
            String msg = Logging.getMessage("nullValue.Icon");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }
        if (this.iconsOverride != null) {
            String msg = Logging.getMessage("generic.LayerIsUsingCustomIterable");
            Logging.logger().severe(msg);
            throw new IllegalStateException(msg);
        }
        this.icons.put(icon.getHashcodeID(), icon);
    }

    /**
     * Adds the contents of the specified
     * <code>icons</code> to this layer's internal collection. If this layer's
     * internal collection has been overriden with a call to {@link #setIcons},
     * this will throw an exception.
     *
     * @param icons Icons to add.
     * @throws IllegalArgumentException If
     * <code>icons</code> is null.
     * @throws IllegalStateException If a custom Iterable has been specified by
     * a call to
     * <code>setIcons</code>.
     */
    public void addIcons(Iterable<Entity> icons) {
        if (icons == null) {
            String msg = Logging.getMessage("nullValue.IterableIsNull");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }
        if (this.iconsOverride != null) {
            String msg = Logging.getMessage("generic.LayerIsUsingCustomIterable");
            Logging.logger().severe(msg);
            throw new IllegalStateException(msg);
        }

        for (Entity icon : icons) {
            // Internal list of icons does not accept null values.
            if (icon != null) {
                this.icons.put(icon.getHashcodeID(), icon);
            }
        }
    }

    /**
     * Removes the specified
     * <code>icon</code> from this layer's internal collection, if it exists. If
     * this layer's internal collection has been overriden with a call to {@link #setIcons},
     * this will throw an exception.
     *
     * @param icon Icon to remove.
     * @throws IllegalArgumentException If
     * <code>icon</code> is null.
     * @throws IllegalStateException If a custom Iterable has been specified by
     * a call to
     * <code>setIcons</code>.
     */
    public void removeIcon(Entity icon) {
        if (icon == null) {
            String msg = Logging.getMessage("nullValue.Icon");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }
        if (this.iconsOverride != null) {
            String msg = Logging.getMessage("generic.LayerIsUsingCustomIterable");
            Logging.logger().severe(msg);
            throw new IllegalStateException(msg);
        }

        this.icons.remove(icon.getHashcodeID());
    }

    /**
     * Clears the contents of this layer's internal Icon collection. If this
     * layer's internal collection has been overriden with a call to {@link #setIcons},
     * this will throw an exception.
     *
     * @throws IllegalStateException If a custom Iterable has been specified by
     * a call to
     * <code>setIcons</code>.
     */
    public void removeAllIcons() {
        if (this.iconsOverride != null) {
            String msg = Logging.getMessage("generic.LayerIsUsingCustomIterable");
            Logging.logger().severe(msg);
            throw new IllegalStateException(msg);
        }

        clearIcons();
    }

    private void clearIcons() {
        if (this.icons != null && this.icons.size() > 0) {
            this.icons.clear();
        }
    }

    /**
     * Returns the Iterable of Icons currently in use by this layer. If the
     * caller has specified a custom Iterable via
     * {@link #setIcons}, this will returns a reference to that Iterable. If the
     * caller passed
     * <code>setIcons</code> a null parameter, or if
     * <code>setIcons</code> has not been called, this returns a view of this
     * layer's internal collection of Icons.
     *
     * @return Iterable of currently active Icons.
     */
    public Iterable<Entity> getIcons() {
        return getActiveIcons();
    }

    public Map<Integer, Entity> getIconList() {
        return icons;
    }

    /**
     * Returns the Iterable of currently active Icons. If the caller has
     * specified a custom Iterable via {@link
     * #setIcons}, this will returns a reference to that Iterable. If the caller
     * passed
     * <code>setIcons</code> a null parameter, or if
     * <code>setIcons</code> has not been called, this returns a view of this
     * layer's internal collection of Icons.
     *
     * @return Iterable of currently active Icons.
     */
    private Iterable<Entity> getActiveIcons() {
        if (this.iconsOverride != null) {
            return this.iconsOverride;
        } else {
            // Return an unmodifiable reference to the internal list of icons.
            // This prevents callers from changing this list and invalidating any invariants we have established.
            return java.util.Collections.unmodifiableCollection(this.icons.values());
        }
    }

    /**
     * Overrides the collection of currently active Icons with the specified
     * <code>iconIterable</code>. This layer will maintain a reference to
     * <code>iconIterable</code> strictly for picking and rendering. This layer
     * will not modify the Iterable reference. However, this will clear the
     * internal collection of Icons, and will prevent any modification to its
     * contents via
     * <code>addIcon, addIcons, or removeIcons</code>.
     * <p/>
     * If the specified
     * <code>iconIterable</code> is null, this layer will revert to maintaining
     * its internal collection.
     *
     * @param iconIterable Iterable to use instead of this layer's internal
     * collection, or null to use this layer's internal collection.
     */
    public void setIcons(Iterable<Entity> iconIterable) {
        this.iconsOverride = iconIterable;
        // Clear the internal collection of Icons.
        clearIcons();
    }

    /**
     * Returns the
     * <code>Pedestal</code> used by this layers internal
     * <code>IconRenderer</code>.
     *
     * @return
     * <code>Pedestal</code> used by this layers internal
     * <code>IconRenderer</code>.
     */
    public Pedestal getPedestal() {
        return pedestal;
    }

    /**
     * Sets the
     * <code>Pedestal</code> used by this layers internal
     * <code>IconRenderer</code>.
     *
     * @param pedestal
     * <code>Pedestal</code> to be used by this layers internal
     * <code>IconRenderer</code>.
     */
    public void setPedestal(Pedestal pedestal) {
        this.pedestal = pedestal;
    }

    /**
     * Indicates whether horizon clipping is performed.
     *
     * @return
     * <code>true</code> if horizon clipping is performed, otherwise
     * <code>false</code>.
     *
     * @see #setHorizonClippingEnabled(boolean)
     */
    public boolean isHorizonClippingEnabled() {
        return this.iconRenderer.isHorizonClippingEnabled();
    }

    /**
     * Indicates whether to render icons beyond the horizon. If view culling is
     * enabled, the icon is also tested for view volume inclusion. The default
     * is
     * <code>false</code>, horizon clipping is not performed.
     *
     * @param horizonClippingEnabled
     * <code>true</code> if horizon clipping should be performed, otherwise
     * <code>false</code>.
     *
     * @see #setViewClippingEnabled(boolean)
     */
    public void setHorizonClippingEnabled(boolean horizonClippingEnabled) {
        this.iconRenderer.setHorizonClippingEnabled(horizonClippingEnabled);
    }

    /**
     * Indicates whether view volume clipping is performed.
     *
     * @return
     * <code>true</code> if view volume clipping is performed, otherwise
     * <code>false</code>.
     *
     * @see #setViewClippingEnabled(boolean)
     */
    public boolean isViewClippingEnabled() {
        return this.iconRenderer.isViewClippingEnabled();
    }

    /**
     * Indicates whether to render icons outside the view volume. This is
     * primarily to control icon visibility beyond the far view clipping plane.
     * Some important use cases demand that clipping not be performed. If
     * horizon clipping is enabled, the icon is also tested for horizon
     * clipping. The default is
     * <code>false</code>, view volume clipping is not performed.
     *
     * @param viewClippingEnabled
     * <code>true</code> if view clipping should be performed, otherwise
     * <code>false</code>.
     *
     * @see #setHorizonClippingEnabled(boolean)
     */
    public void setViewClippingEnabled(boolean viewClippingEnabled) {
        this.iconRenderer.setViewClippingEnabled(viewClippingEnabled);
    }

    /**
     * Indicates whether an icon's elevation is treated as an offset from the
     * terrain or an absolute elevation above sea level.
     *
     * @return
     * <code>true</code> if icon elevations are treated as absolute,
     * <code>false</code> if they're treated as offsets from the terrain.
     */
    public boolean isAlwaysUseAbsoluteElevation() {
        return this.iconRenderer.isAlwaysUseAbsoluteElevation();
    }

    /**
     * Normally, an icon's elevation is treated as an offset from the terrain
     * when it is less than the globe's maximum elevation. Setting
     * #setAlwaysUseAbsoluteElevation to
     * <code>true</code> causes the elevation to be treated as an absolute
     * elevation above sea level.
     *
     * @param alwaysUseAbsoluteElevation
     * <code>true</code> to treat icon elevations as absolute,
     * <code>false</code> to treat them as offsets from the terrain.
     */
    public void setAlwaysUseAbsoluteElevation(boolean alwaysUseAbsoluteElevation) {
        this.iconRenderer.setAlwaysUseAbsoluteElevation(alwaysUseAbsoluteElevation);
    }

    /**
     * Opacity is not applied to layers of this type. The icon image is assumed
     * to indicates its opacity.
     *
     * @param opacity the current opacity value, which is ignored by this layer.
     */
    @Override
    public void setOpacity(double opacity) {
        super.setOpacity(opacity);
    }

    /**
     * Returns the layer's opacity value, which is ignored by this layer the
     * icon's image is assumed to indicate its opacity.
     *
     * @return The layer opacity, a value between 0 and 1.
     */
    @Override
    public double getOpacity() {
        return super.getOpacity();
    }

    @Override
    protected void doPick(DrawContext dc, java.awt.Point pickPoint) {
        super.doPick(dc, pickPoint);
        this.iconRenderer.setPedestal(this.pedestal);
        this.iconRenderer.pick(dc, getActiveIcons(), pickPoint, this);
    }

    @Override
    protected void doRender(DrawContext dc) {        
        this.iconRenderer.setPedestal(this.pedestal);
        this.iconRenderer.render(dc, getActiveIcons());
//        super.doRender(dc);
    }

    @Override
    public String toString() {
        return Logging.getMessage("RenderableIconLayer.Name");
    }
}
