package com.kodekutters.dis.entity;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 *
 * @author Ringo Wathelet
 * @version 1.0 @date March , 2012
 */
public class EntityListRootNode extends AbstractNode {

    private final EntityChildFactory childFactory;

    public EntityListRootNode(EntityChildFactory childFactory) {
        super(Children.create(childFactory, true));
        this.childFactory = childFactory;
    }

    public void refresh() {
        this.childFactory.refresh();
    }
}