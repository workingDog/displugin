package com.kodekutters.dis.entity;

import java.beans.IntrospectionException;
import org.openide.nodes.BeanNode;
import org.openide.nodes.Children;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Ringo Wathelet
 * @version 1.0 @date March , 2012
 */
public class EntityBeanNode<Entity> extends BeanNode {

    public EntityBeanNode(Entity entity) throws IntrospectionException {
        super(entity, Children.LEAF, Lookups.singleton(entity));
    }
}
