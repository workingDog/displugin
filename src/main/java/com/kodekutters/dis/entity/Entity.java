package com.kodekutters.dis.entity;

import com.kodekutters.dis.util.BoundedDeque;
import com.kodekutters.dis.util.SelectableAnnotation;
import edu.nps.moves.deadreckoning.*;
import edu.nps.moves.dis.DeadReckoningParameter;
import edu.nps.moves.dis.EntityStatePdu;
import edu.nps.moves.disenum.CountryType;
import edu.nps.moves.disenum.EntityDomain;
import edu.nps.moves.disenum.EntityKind;
import edu.nps.moves.disenum.ForceID;
import edu.nps.moves.disutil.CoordinateConversions;
import edu.nps.moves.disutil.EulerConversions;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.apache.commons.math.geometry.Vector3D;
import org.joda.time.DateTime;

/**
 * a general test entity to be displayed in DisLayer. The main element
 * is the DIS espdu
 *
 * @author R. Wathelet, 2012
 * @version 1.0
 */
public class Entity extends UserFacingEntity implements Renderable, Serializable {

    private EntityStatePdu espdu = new EntityStatePdu();
    private DIS_DeadReckoning deadReckonAlgo = null;
    private DateTime lastReckonTime = new DateTime();
    private Timer deadReckonTimer = null;
    private int deadReckoningStopTime = 5;              // the DR will stop after x seconds of no espdu update
    private boolean isUnderDeadReckonMotion = false;    // entity is currently under dead reckoning motion
    private int deadReckonRefreshTime = 1000; // millisec    
    private double speed = 0.0;     // in km/hr
    private double heading = 0.0;   // heading angle from north in degrees
    private SelectableAnnotation annotation = null;
    transient static private BufferedImage selectionBackground = null;
    // the position history of the entity
    private final int maxHistoryCapacity = 100000;
    private final BoundedDeque<Position> positionHistory = new BoundedDeque(maxHistoryCapacity);
    private boolean disableHistory = false;
    private final Path historyPath;
    private Long creationTime = 0L;
    private Long terminationTime = 0L;

    {
        // set the initial dead reckoning parameter to 2, simple velocity / time algorithm   
        DeadReckoningParameter deadRecParam = new DeadReckoningParameter();
        deadRecParam.setDeadReckoningAlgorithm((short) 2);
        espdu.setDeadReckoningParameters(deadRecParam);

        selectionBackground = this.createBitmap(PatternFactory.PATTERN_CIRCLE, new Color(0.98f, 1f, 0.46f, 0.6f));
        this.setBackgroundScale(3.0);

        historyPath = new Path() {
            // to catch any errors from Path, in particular the null pointer and the threading stuff
            @Override
            protected void addTessellatedPosition(Position pstn, Color color, Integer intgr, Path.PathData pd) {
                try {
                    super.addTessellatedPosition(pstn, color, intgr, pd);
                } catch (Exception e) {
                    // do nothing with the errors, just catch them
                }
            }
        };

    }

    public Entity() {
        setTeeIcon();
        if (annotation == null) {
            annotation = new SelectableAnnotation("no name", getPosition(), this);
            annotation.setValue(AVKey.DISPLAY_NAME, "no name");
        }
        this.setValue(AVKey.DISPLAY_NAME, "no name");
        initHistoryPath();
    }

    public Entity(Position position) {
        super();
        setPosition(position);
        setTeeIcon();
        annotation = new SelectableAnnotation(getDisplayInfo(), getPosition(), this);
        this.setValue(AVKey.DISPLAY_NAME, getDisplayInfo());
        initHistoryPath();
    }

    public Entity(Object imageSource, Position position) {
        this(position);
        this.setImageSource(imageSource);
    }

    public Entity(EntityStatePdu epdu) {
        super();
        setEspdu(epdu);
        setTeeIcon();
        annotation = new SelectableAnnotation(getDisplayInfo(), getPosition(), this);
        this.setValue(AVKey.DISPLAY_NAME, getDisplayInfo());
        initHistoryPath();
        setCreationTime(epdu.getTimestamp());
    }

    public Entity(EntityStatePdu epdu, boolean disable) {
        this(epdu);
        disableHistory = disable;
    }

    public DateTime getLastReckonTime() {
        return lastReckonTime;
    }

    public void setLastReckonTime(DateTime lastReckonTime) {
        this.lastReckonTime = lastReckonTime;
    }

    public DIS_DeadReckoning getDeadReckonAlgo() {
        return deadReckonAlgo;
    }

    public void setDeadReckonAlgo(DIS_DeadReckoning deadReckonAlgo) {
        this.deadReckonAlgo = deadReckonAlgo;
    }

    public int getDeadReckonRefreshTime() {
        return deadReckonRefreshTime;
    }

    public void setDeadReckonRefreshTime(int deadReckonRefreshTime) {
        this.deadReckonRefreshTime = deadReckonRefreshTime;
    }

    public Timer getDeadReckonTimer() {
        return deadReckonTimer;
    }

    public void setDeadReckonTimer(Timer deadReckonTimer) {
        this.deadReckonTimer = deadReckonTimer;
    }

    public int getDeadReckoningStopTime() {
        return deadReckoningStopTime;
    }

    public void setDeadReckoningStopTime(int deadReckoningStopTime) {
        this.deadReckoningStopTime = deadReckoningStopTime;
    }

    public boolean isUnderDeadReckonMotion() {
        return isUnderDeadReckonMotion;
    }

    public void setIsUnderDeadReckonMotion(boolean isUnderDeadReckonMotion) {
        this.isUnderDeadReckonMotion = isUnderDeadReckonMotion;
    }

    public boolean isHistoryVisible() {
        return historyPath.isVisible();
    }

    public void setHistoryVisible(boolean historyState) {
        boolean oldValue = this.isHistoryVisible();
        historyPath.setVisible(historyState);
        changeSupport.firePropertyChange("history path", oldValue, historyState);
    }

    // this makes the entity pretend to be a renderable and appear in the layerManager
    @Override
    public void render(DrawContext dc) {
    }

    public SelectableAnnotation getAnnotation() {
        return annotation;
    }

    public void setAnnotation(SelectableAnnotation annotation) {
        SelectableAnnotation oldValue = this.getAnnotation();
        this.annotation = annotation;
        changeSupport.firePropertyChange("annotation", oldValue, annotation);
    }

    public double getHeading() {
        return heading;
    }

    public void setHeading(double heading) {
        double oldValue = this.getHeading();
        this.heading = heading;
        changeSupport.firePropertyChange("heading", oldValue, heading);
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        double oldValue = this.getSpeed();
        this.speed = speed;
        changeSupport.firePropertyChange("speed", oldValue, speed);
    }

    public String getMarking() {
        return new String(this.getEspdu().getMarking().getCharacters()).trim();
    }

    @Override
    public String getName() {
        return this.getMarking();
    }

    public String getDisplayInfo() {

        String country = "";
        if (CountryType.enumerationForValueExists(this.getEspdu().getEntityType().getCountry())) {
            country = CountryType.getDescriptionForValue(this.getEspdu().getEntityType().getCountry());
        }

        String domain = "";
        if (EntityDomain.enumerationForValueExists(this.getEspdu().getEntityType().getDomain())) {
            domain = EntityDomain.getDescriptionForValue(this.getEspdu().getEntityType().getDomain());
        }
        
        String entityType = "";
        if (EntityKind.enumerationForValueExists(this.getEspdu().getEntityType().getEntityKind())) {
            entityType = EntityKind.getDescriptionForValue(this.getEspdu().getEntityType().getEntityKind());
        }

        String name = new String(this.getEspdu().getMarking().getCharacters()).trim();
        String idVal = String.valueOf(this.getEspdu().getEntityID().getEntity());
        String sBuff = idVal + "_" + country + "_" + domain + "_" + entityType + "_" + name;

        return sBuff;
    }

    public EntityStatePdu getEspdu() {
        return espdu;
    }

    /**
     * a quick way to get an unique id given its espdu
     *
     * @return the espdu.getEntityID().hashCode() as an Integer
     *
     */
    public Integer getHashcodeID() {
        return Entity.getHashcodeID(espdu);
    }

    /**
     * static convenience method for a quick way to get an unique id given an
     * espdu
     *
     * @param espdu the espdu we want the id for
     * @return the espdu.getEntityID().hashCode() as an Integer
     */
    public static Integer getHashcodeID(EntityStatePdu espdu) {
        return espdu.getEntityID().hashCode();
    }

    @Override
    /**
     * get the lat,lon position of the entity from the espdu position
     *
     * @return the position of the entity in lat,lon
     */
    public Position getPosition() {

        if (espdu == null) {
            return super.getPosition();
        }

        // the coordinates in the pdu are in ECEF coordinate system (radians),
        // so first convert them into geodetic coordinates (radians).
        double[] xyz = {
            espdu.getEntityLocation().getX(),
            espdu.getEntityLocation().getY(),
            espdu.getEntityLocation().getZ()
        };
        double[] lla = CoordinateConversions.xyzToLatLonRadians(xyz);

        //       Position pos = Position.fromRadians(lla[0], lla[1], lla[2]);
        // get elevation of the earth at this location
        //       double groundElevation = wwm.getWorldWindow().getModel().getGlobe().getElevation(pos.getLatitude(), pos.getLongitude());
        //       double agl = pos.getAltitude() - groundElevation;

        // TODO
        // set the appropriate altitude, .... do this for now, but this may not be correct...
        Position pos;
//        if (this.espdu.getEntityType().getDomain() == EntityDomain.LAND.getValue()
//                || this.espdu.getEntityType().getDomain() == EntityDomain.SURFACE.getValue()) {
//            pos = Position.fromRadians(lla[0], lla[1], 0);
//        } else {  // SUBSURFACE, AIR, SPACE, OTHER
//            pos = Position.fromRadians(lla[0], lla[1], lla[2]);
//        }
        pos = Position.fromRadians(lla[0], lla[1], lla[2]);
        if (annotation != null) {
            annotation.moveTo(pos);
        }
        return pos;
    }

    @Override
    public void setPosition(Position pos) {
        super.setPosition(pos);
        if (espdu != null) {
            setEspduPosition(pos);
        }
        // add to the entity position history
        this.positionHistory.add(pos);
    }

    /**
     * set the position of the espdu from the entity position in lat,lon
     *
     * @param pos the entity position in lat,lon
     */
    private void setEspduPosition(Position pos) {

        if (espdu == null) {
            super.setPosition(pos);
            return;
        }

        Position oldValue = this.getPosition();

        double[] posRadians = pos.asRadiansArray();

        // the pdu coordinates need to be in ECEF (radians)
        double[] xyz = CoordinateConversions.getXYZfromLatLonRadians(posRadians[0], posRadians[1], pos.getElevation());

        espdu.getEntityLocation().setX(xyz[0]);
        espdu.getEntityLocation().setY(xyz[1]);
        espdu.getEntityLocation().setZ(xyz[2]);

        changeSupport.firePropertyChange("position", oldValue, pos);

    }

    public ArrayList<Position> getHistoryPositions() {
        if (this.positionHistory.size() == 0) {
            return new ArrayList<Position>();
        }
        return new ArrayList<Position>(this.positionHistory);
    }

    private void addToHistory(final Position pos) {
        // all this is to try to avoid the 
        // "java.lang.IllegalArgumentException: Point is null" error
        // and still it may not work
        if (pos != null) {
            synchronized (positionHistory) {
                positionHistory.add(pos);
            }
            Runnable runer = new Runnable() {
                public void run() {
                    synchronized (historyPath) {
                        historyPath.setPositions(positionHistory);
                    }
                }
            };
            SwingUtilities.invokeLater(runer);
        }
    }

    /**
     * set the espdu to pdu. Also set the heading and speed of the entity based
     * on this pdu. In addition translate the espdu position and set the entity
     * to the lat,lon position
     *
     * @param pdu the pdu we want this.espdu to become
     */
    public void setEspdu(EntityStatePdu pdu) {
        espdu = pdu;

        // if the marking has changed reflect the new marking
        if (annotation != null) {
            annotation.setText(getDisplayInfo());
            this.setValue(AVKey.DISPLAY_NAME, getDisplayInfo());
        }

        // get the position from the espdu coordinates
        final Position pos = getPosition();

        moveTo(pos);

        // add to the position history
        if (!disableHistory) {
            addToHistory(pos);
        }

        if (!this.isUnderDeadReckonMotion()) {
            setDeadReckonningAlgorithm();
        }

//        this.setPitch(EulerConversions.getPitchFromEuler(pos.getLatitude().radians, pos.getLongitude().radians,
//                espdu.getEntityOrientation().getPsi(), espdu.getEntityOrientation().getTheta()));
//
//        this.setRoll(EulerConversions.getRollFromEuler(pos.getLatitude().radians, pos.getLongitude().radians,
//                espdu.getEntityOrientation().getPsi(), espdu.getEntityOrientation().getTheta(), espdu.getEntityOrientation().getPhi()));

        // the heading direction from the ECEF angles
        heading = EulerConversions.getOrientationFromEuler(pos.getLatitude().radians, pos.getLongitude().radians,
                espdu.getEntityOrientation().getPsi(), espdu.getEntityOrientation().getTheta());

//        this.setYaw(heading);

        if (espdu.getEntityLinearVelocity() != null) {
            // speed from metres per second in DIS to km per hour
            double[] speed = {
                espdu.getEntityLinearVelocity().getX() * 3.6,
                espdu.getEntityLinearVelocity().getY() * 3.6,
                espdu.getEntityLinearVelocity().getZ() * 3.6
            };
            // the entity speed value in the heading direction from the (ECEF) velocity vector
            setSpeed(new Vector3D(speed[0], speed[1], speed[2]).getNorm());
        }

        // the dead reckoning position update
        if (!this.isUnderDeadReckonMotion()) {
            startDeadReckonning();
        }

    }

    private void setDeadReckonningAlgorithm() {
        if (espdu.getDeadReckoningParameters().getDeadReckoningAlgorithm() > (short) 1) {
            this.isUnderDeadReckonMotion = true;
            if (espdu.getDeadReckoningParameters().getDeadReckoningAlgorithm() == (short) 2) {
                this.setDeadReckonAlgo(new DIS_DR_FPW_02());
            } else if (espdu.getDeadReckoningParameters().getDeadReckoningAlgorithm() == (short) 3) {
                this.setDeadReckonAlgo(new DIS_DR_RPW_03b());
            } else if (espdu.getDeadReckoningParameters().getDeadReckoningAlgorithm() == (short) 4) {
                this.setDeadReckonAlgo(new DIS_DR_RVW_04b());
            } else if (espdu.getDeadReckoningParameters().getDeadReckoningAlgorithm() == (short) 5) {
                this.setDeadReckonAlgo(new DIS_DR_FVW_05());
            } else if (espdu.getDeadReckoningParameters().getDeadReckoningAlgorithm() == (short) 6) {
                this.setDeadReckonAlgo(new DIS_DR_FPB_06());
            } else if (espdu.getDeadReckoningParameters().getDeadReckoningAlgorithm() == (short) 7) {
                this.setDeadReckonAlgo(new DIS_DR_RPB_07());
            } else if (espdu.getDeadReckoningParameters().getDeadReckoningAlgorithm() == (short) 8) {
                this.setDeadReckonAlgo(new DIS_DR_RVB_08());
            } else if (espdu.getDeadReckoningParameters().getDeadReckoningAlgorithm() == (short) 9) {
                this.setDeadReckonAlgo(new DIS_DR_FVB_09());
            } else {
                System.out.println("in Entity setEspdu DeadReckoningAlgorithm number > 9, default is 2");
                this.isUnderDeadReckonMotion = false;
            }
        } else {
            this.isUnderDeadReckonMotion = false;
        }
    }
    
    // the dead reckoning position update
    private void doDeadReckonMove() {
        // check that we still want to be under DR
        if (this.isUnderDeadReckonMotion && (this.getDeadReckonAlgo() != null)) {
            double[] update = this.getDeadReckonAlgo().getUpdatedPositionOrientation();
            double[] xyz = {update[0], update[1], update[2]};
            double[] lla = CoordinateConversions.xyzToLatLonRadians(xyz);
            this.moveTo(Position.fromRadians(lla[0], lla[1], lla[2]));
            // if did not get an update for x seconds, stop the DR 
            if (new DateTime().getSecondOfDay() - this.getLastReckonTime().getSecondOfDay() > this.getDeadReckoningStopTime()) {
                stopDeadReckoning();
            }
        } else {
            stopDeadReckoning();
        }
    }

    public void stopDeadReckoning() {
        if (this.deadReckonTimer != null) {
            this.deadReckonTimer.stop();
            this.deadReckonTimer = null;
            this.isUnderDeadReckonMotion = false;
        }
    }

    private void startDeadReckonning() {
        if (this.isUnderDeadReckonMotion) {
            // the dead reckoning position update thread
            this.deadReckonTimer = new Timer(this.getDeadReckonRefreshTime(), new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    doDeadReckonMove();
                }
            });
            this.deadReckonTimer.start();
        }
    }

   
    private void setTeeIcon() {
        try {
            String ext = "-tee.png";   // for a "normal" entity
            if (espdu.getEntityType().getEntityKind() == EntityKind.MUNITION.getValue()) {
                ext = "-dot.png";      // for a "munition" entity
            }
            // default icon is yellow
            BufferedImage imgIcon = ImageIO.read(getClass().getResource("/com/kodekutters/dis/image/resources/yellow" + ext));
            if (espdu != null) {
                if (espdu.getForceId() == ForceID.FRIENDLY.getValue()) {
                    imgIcon = ImageIO.read(getClass().getResource("/com/kodekutters/dis/image/resources/blue" + ext));
                } else if (espdu.getForceId() == ForceID.OPPOSING.getValue()) {
                    imgIcon = ImageIO.read(getClass().getResource("/com/kodekutters/dis/image/resources/red" + ext));
                } else if (espdu.getForceId() == ForceID.NEUTRAL.getValue()) {
                    imgIcon = ImageIO.read(getClass().getResource("/com/kodekutters/dis/image/resources/green" + ext));
                }
            }
            this.setImageSource(imgIcon);
        } catch (IOException e) {
            System.out.println("ERROR reading the icon file " + e.getMessage());
        }
    }

    @Override
    public void setVisible(boolean state) {
        super.setVisible(state);
        this.getAnnotation().getAttributes().setVisible(state);
        this.setHistoryVisible(state);
    }

    @Override
    public void setSelected(boolean onof) {
        super.setSelected(onof);
        if (this.isSelected()) {
            this.setBackgroundImage(selectionBackground);
        } else {
            this.setBackgroundImage(null);
        }
        annotation.setSelected(onof, true);
    }

    private BufferedImage createBitmap(String pattern, Color color) {
        BufferedImage image = PatternFactory.createPattern(pattern, new Dimension(128, 128), 0.7f,
                color, new Color(color.getRed(), color.getGreen(), color.getBlue(), 0));
        image = PatternFactory.blur(image, 13);
        image = PatternFactory.blur(image, 13);
        image = PatternFactory.blur(image, 13);
        image = PatternFactory.blur(image, 13);
        return image;
    }

    public Path getHistoryPath() {
        return historyPath;
    }

    private void initHistoryPath() {
        ArrayList<Position> linePos = getHistoryPositions();
        if (linePos.isEmpty() || (linePos.size() < 2)) {
            linePos.clear();
            // need at least 2 points for a path
            if (this.getPosition() != null) {
                linePos.add(this.getPosition());
                linePos.add(this.getPosition());
            } else {
                linePos.add(Position.ZERO);
                linePos.add(Position.ZERO);
            }
        }

        ShapeAttributes attrs = new BasicShapeAttributes();
        attrs.setOutlineMaterial(new Material(Color.yellow));
        attrs.setOutlineWidth(2d);
        attrs.setEnableLighting(false);

        historyPath.setPositions(linePos);
        historyPath.setAttributes(attrs);
        historyPath.setPathType(AVKey.LINEAR);
        historyPath.setExtrude(false);
        historyPath.setVisible(true);
        historyPath.setAltitudeMode(WorldWind.ABSOLUTE);

        historyPath.setValue(AVKey.DISPLAY_NAME, getDisplayInfo());
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public boolean isDisableHistory() {
        return disableHistory;
    }

    public void setDisableHistory(boolean disableHistory) {
        this.disableHistory = disableHistory;
    }

    public Long getTerminationTime() {
        return terminationTime;
    }

    public void setTerminationTime(Long terminationTime) {
        this.terminationTime = terminationTime;
    }
}
