package com.kodekutters.dis.entity;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.*;
import org.openide.util.Exceptions;

/**
 *
 * @author Ringo Wathelet
 * @version 1.0 @date March , 2012
 */
public class EntityChildFactory extends ChildFactory<Entity> {

    private final ArrayList<Entity> theList;

    public EntityChildFactory(ArrayList<Entity> aList) {
        theList = aList;
    }

    public EntityChildFactory(Collection<Entity> aList) {
        theList = new ArrayList<Entity>();
        for (Entity entity : aList) {
            theList.add(entity);            
        }
    }

    public void refresh() {
        refresh(true);
    }

    @Override
    protected boolean createKeys(List<Entity> toPopulate) {
        toPopulate.addAll(theList);
        return true;
    }

    @Override
    protected Node createNodeForKey(Entity key) {
        Node result = null;
        try {
            result = new EntityBeanNode(key);
            result.setDisplayName(key.getDisplayInfo());
        } catch (IntrospectionException ex) {
            Exceptions.printStackTrace(ex);
        }
        return result;
    }
}
