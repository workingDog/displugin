package com.kodekutters.dis.entity;

import gov.nasa.worldwind.Disposable;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.UserFacingIcon;
import gov.nasa.worldwind.util.Logging;
import java.awt.Dimension;
import java.awt.Image;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;
import javax.imageio.ImageIO;

/**
 * A wrapper for the UserFacingIcon
 *
 * @author Wathelet R
 */
public class UserFacingEntity extends UserFacingIcon implements EntityType, Disposable {

    protected PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private final UUID id;
    private int minIconWidthSize = 10;
    private boolean selected = false;
    private String name = "";

    public UserFacingEntity() {
        super();
        id = UUID.randomUUID();
    }

    public UserFacingEntity(Object imageSource, Position position) {
        super(imageSource, position);
        id = UUID.randomUUID();
    }

    public UserFacingEntity(String iconPath, Position iconPosition) {
        super(iconPath, iconPosition);
        id = UUID.randomUUID();
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    public void setMinIconWidthSize(int val) {
        int oldValue = this.minIconWidthSize;
        this.minIconWidthSize = val;
        changeSupport.firePropertyChange("minIconWidthSize", oldValue, this.minIconWidthSize);
    }

    // value can be -ve
    @Override
    public void setSize(Dimension value) {
        Dimension size = this.getSize();
        double width = size != null ? size.getWidth() : this.getIconImage().getWidth(null);
        double height = size != null ? size.getHeight() : this.getIconImage().getHeight(null);
        int newWidth = (int) (width + value.width);
        int newHeight = (int) (height + value.height);
        if ((newWidth < this.minIconWidthSize) || (newHeight < this.minIconWidthSize)) {
            super.setSize(new Dimension((int) width, (int) height));
        } else {
            super.setSize(new Dimension(newWidth, newHeight));
        }
        changeSupport.firePropertyChange("size", size, this.getSize());
    }

    private Image getIconImage() {
        Image image = null;
        if (this.getImageSource() instanceof String) {
            try {
                image = ImageIO.read(new BufferedInputStream(new FileInputStream((String) this.getImageSource())));
            } catch (IOException iOException) {
                System.out.println("error in UserFacingEntity getIconImage() " + "could not read the imagesource file " + iOException);
            }
        } else {
            if (this.getImageSource() instanceof Image) {
                image = (Image) this.getImageSource();
            } else {
                System.out.println("error in UserFacingEntity getIconImage() is incorrect");
            }
        }
        return image;
    }

    // value can be -ve
    public void adjustAltitude(double value) {
        this.setPosition(new Position(this.getPosition(), this.getPosition().elevation + value));
    }

    @Override
    public void dispose() {
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void setSelected(boolean selected) {
        boolean oldValue = this.selected;
        this.selected = selected;
        changeSupport.firePropertyChange("selected", oldValue, this.selected);
    }

    public UUID getId() {
        return id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String val) {
        String oldValue = this.getName();
        this.name = val;
        changeSupport.firePropertyChange("name", oldValue, val);
    }

    @Override
    public void move(Position position) {
        if (position == null) {
            String msg = Logging.getMessage("nullValue.PositionIsNull");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }
        this.setPosition(this.getPosition().add(position));
    }

    @Override
    public void moveTo(Position position) {
        if (position == null) {
            String msg = Logging.getMessage("nullValue.PositionIsNull");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }
        this.setPosition(position);
    }

    @Override
    public void setPosition(Position pos) {
        Position oldValue = this.getPosition();
        super.setPosition(pos);
        changeSupport.firePropertyChange("position", oldValue, pos);
    }
}
