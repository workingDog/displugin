/**
 * DisConnectPanel, a panel that deals with the DIS connection input parameters
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
package com.kodekutters.dis.gui;

import com.kodekutters.dis.connection.DisConnectionService;
import com.kodekutters.dis.core.DisSupport;
import com.kodekutters.dis.services.DisBasicService;
import com.kodekutters.dis.services.DisService;
import com.kodekutters.dis.util.RegexFormatter;
import edu.nps.moves.disenum.PduType;
import edu.nps.moves.net.BehaviorConsumerIF;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.*;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DisConnectPanel extends JPanel {

    protected DisService disService = null;
    protected DisSupport disSupport = null;
    protected BehaviorConsumerIF autoArmListener;
    protected Color exerciseIdTableBackground;
    protected Color pduTypeTableBackground;
    protected Color portFieldBackground;
    protected Color applicationBackground;
    protected Color siteBackground;
    protected Color refreshBackground;
    protected Color ipAddressBackground;
    protected Color warningColor = Color.pink;

    public DisConnectPanel() {

        // get a dis service 
        disService = new DisBasicService();

        initComponents();

        localhostButton.setEnabled(true);

        // set the ip address text field to the localhost address
        localhostButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ipAdressTextField.setText(InetAddress.getLocalHost().getHostAddress().toString());
                } catch (UnknownHostException ex) {
                    Logger.getLogger(DisConnectPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        // set the ip address text field to the broadcast address
        broadcastButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String prevText = ipAdressTextField.getText();
                try {
                    String ipAddress = disSupport.getConnection().calculateBroadcastAddress();
                    if ((ipAddress == null) || ipAddress.isEmpty()) {
                        ipAdressTextField.setText(prevText);
                    } else {
                        ipAdressTextField.setText(ipAddress);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(DisConnectPanel.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("....... error in setting broadcast address ");
                    ipAdressTextField.setText(prevText);
                }
            }
        });

        // record the normal background colors
        portFieldBackground = portField.getBackground();
        pduTypeTableBackground = pduTypeTable1.getBackground();
        exerciseIdTableBackground = exerciseIdTable.getBackground();
        applicationBackground = applicationField.getBackground();
        siteBackground = siteField.getBackground();
        refreshBackground = refreshTimeField.getBackground();
        ipAddressBackground = ipAdressTextField.getBackground();

        populateExerciseTable();

        exerciseIdTable.getColumnModel().getColumn(0).setPreferredWidth(180);

        populatePduTypeTables();

        pduTypeTable1.getColumnModel().getColumn(0).setPreferredWidth(30);
        pduTypeTable1.getColumnModel().getColumn(1).setPreferredWidth(200);

        pduTypeTable2.getColumnModel().getColumn(0).setPreferredWidth(30);
        pduTypeTable2.getColumnModel().getColumn(1).setPreferredWidth(200);

    }

    public DisService getDisService() {
        return this.disService;
    }

    public void setDisService(DisService disService) {
        this.disService = disService;
    }

    public DisSupport getDisSupport() {
        return this.disSupport;
    }

    public void setDisSupport(DisSupport handler) {
        this.disSupport = handler;
        this.disService.setDisHandler(handler);
    }

    private void populateExerciseTable() {
        for (int i = 0; i < 100; i++) {
            Object[] aRow = {false, i};
            ((DefaultTableModel) exerciseIdTable.getModel()).addRow(aRow);
        }
        // set exercise id=1 on as default
        ((DefaultTableModel) exerciseIdTable.getModel()).setValueAt(true, 1, 0);
    }

    private void populatePduTypeTables() {
        int count = 0;
        // put 1/2 of the types in each of the 2 tables
        int oneHalf = PduType.values().length / 2;
        // for all pdu types
        for (PduType pType : PduType.values()) {
            boolean state = false;
            // turn on these two types as default
            if (pType == PduType.ENTITY_STATE || pType == PduType.ENTITY_STATE_UPDATE) {
                state = true;
            }
            // note: the table entries will contain the pdu type plus the description separated by a blank space
            String pduString = String.valueOf(pType.getValue()) + " " + pType.getDescription();
            Object[] aRow = {state, pduString};
            if (count < oneHalf) {
                ((DefaultTableModel) pduTypeTable1.getModel()).addRow(aRow);
            } else if (count < PduType.values().length) {
                ((DefaultTableModel) pduTypeTable2.getModel()).addRow(aRow);
            }
            count++;
        }
    }

    /*
     * convenience method to set the start and stop pdu types on or off in the
     * pdu type table
     */
    protected void setStartStopPduTypes(boolean onof) {
        String pduStringStart = String.valueOf(PduType.START_RESUME.getValue()) + " " + PduType.START_RESUME.getDescription();
        String pduStringStop = String.valueOf(PduType.STOP_FREEZE.getValue()) + " " + PduType.STOP_FREEZE.getDescription();
        for (int i = 0; i < ((DefaultTableModel) pduTypeTable1.getModel()).getRowCount(); i++) {
            String tableVal = (String) ((DefaultTableModel) pduTypeTable1.getModel()).getValueAt(i, 1);
            if (tableVal.equals(pduStringStart) || tableVal.equals(pduStringStop)) {
                ((DefaultTableModel) pduTypeTable1.getModel()).setValueAt(onof, i, 0);
                pduTypeTable1.repaint();
            }
        }
    }

    /**
     * set all parameter fields enabled or disabled so that the user can/cannot
     * edit them
     *
     * @param onof set all parameter fields to true or false
     */
    protected void setEnableAll(boolean onof) {
        multicastButton.setEnabled(onof);
        broadcastButton.setEnabled(onof);
        unicastButton.setEnabled(onof);
        localhostButton.setEnabled(onof);
        applicationField.setEnabled(onof);
        exerciseIdTable.setEnabled(onof);
        ipAdressTextField.setEnabled(onof);
        pduTypeTable1.setEnabled(onof);
        pduTypeTable2.setEnabled(onof);
        portField.setEnabled(onof);
        refreshTimeField.setEnabled(onof);
        siteField.setEnabled(onof);
        timeToLiveTextField.setEnabled(onof);
        selectAllIdCheckBox.setEnabled(onof);
        selectAllPduCheckBox.setEnabled(onof);
    }

    /**
     * check that all parameter fields settings for the connection are correct.
     * This is a partial check, many errors are still not caught
     *
     * @return true if all parameter fields settings for the connection are
     * correct else false
     */
    protected boolean isConnectionOk() {
        boolean result = true;
        int portN = DisConnectionService.DEFAULT_PORT;
        try {
            portN = Integer.valueOf(portField.getText()).intValue();
            // basic check, some port numbers will still not work, and will probably
            // crash the system, choose something sensible here
            if (portN > 65535 || portN < 1024) {
                portField.setBackground(warningColor);
                result = false;
            } else {
                getDisSupport().getConnection().setPort(portN);
                portField.setBackground(portFieldBackground);
            }
        } catch (Exception e) {
            System.err.println("Error parsing portNumberField " + e);
            portField.setBackground(warningColor);
            result = false;
        }

        try {
            ipAdressTextField.setBackground(ipAddressBackground);
            getDisSupport().getConnection().setAddress(ipAdressTextField.getText());
            // a quick test to see if the inetAddress can be obtained
            if (getDisSupport().getConnection().getInetAddress() == null) {
                ipAdressTextField.setBackground(warningColor);
                result = false;
            }
        } catch (Exception e) {
            System.err.println("Error parsing ipAdressTextField " + e);
            ipAdressTextField.setBackground(warningColor);
            result = false;
        }

        if (!refreshTimeField.getText().isEmpty()) {
            try {
                int tVal = Integer.valueOf(refreshTimeField.getText()).intValue();
                if (tVal <= 0) {
                    refreshTimeField.setBackground(warningColor);
                    result = false;
                } else {
                    getDisSupport().setRefreshTime(tVal);
                    refreshTimeField.setBackground(refreshBackground);
                }
            } catch (Exception e) {
                System.err.println("Error parsing refreshTimeField " + e);
                refreshTimeField.setBackground(warningColor);
                result = false;
            }
        } else {
            refreshTimeField.setBackground(warningColor);
            result = false;
        }

        if (!siteField.getText().isEmpty()) {
            try {
                getDisSupport().setSiteId(Integer.valueOf(siteField.getText()).intValue());
                siteField.setBackground(siteBackground);
            } catch (Exception e) {
                System.err.println("Error parsing siteField " + e);
                siteField.setBackground(warningColor);
                result = false;
            }
        } else {
            siteField.setBackground(warningColor);
            result = false;
        }

        if (!applicationField.getText().isEmpty()) {
            try {
                getDisSupport().setApplicationId(Integer.valueOf(applicationField.getText()).intValue());
                applicationField.setBackground(applicationBackground);
            } catch (Exception e) {
                System.err.println("Error parsing applicationField " + e);
                applicationField.setBackground(warningColor);
                result = false;
            }
        } else {
            applicationField.setBackground(warningColor);
            result = false;
        }

        ArrayList<Integer> theExIdList = getSelectedExerciseId();
        if (!theExIdList.isEmpty()) {
            getDisSupport().setExerciseIdList(theExIdList);
            exerciseIdTable.setBackground(exerciseIdTableBackground);
        } else {
            exerciseIdTable.setBackground(warningColor);
            result = false;
        }

        ArrayList<Integer> thepduTypeList = getSelectedPduTypes();
        if (!thepduTypeList.isEmpty()) {
            getDisSupport().setPduTypeList(thepduTypeList);
            pduTypeTable1.setBackground(pduTypeTableBackground);
            pduTypeTable2.setBackground(pduTypeTableBackground);
        } else {
            pduTypeTable1.setBackground(warningColor);
            pduTypeTable2.setBackground(warningColor);
            result = false;
        }

        if (broadcastButton.isSelected()) {
            getDisSupport().getConnection().setConnectionType(DisConnectionService.CONNECTION_TYPE.BROADCAST);
        }
        if (multicastButton.isSelected()) {
            getDisSupport().getConnection().setConnectionType(DisConnectionService.CONNECTION_TYPE.MULTICAST);
            // a quick test to check if have a valid multicast address
            if (!getDisSupport().getConnection().getInetAddress().isMulticastAddress()) {
                ipAdressTextField.setBackground(warningColor);
                result = false;
            }
        }
        if (unicastButton.isSelected()) {
            getDisSupport().getConnection().setConnectionType(DisConnectionService.CONNECTION_TYPE.UNICAST);
        }
        if (localhostButton.isSelected()) {
            getDisSupport().getConnection().setConnectionType(DisConnectionService.CONNECTION_TYPE.LOCALHOST);
        }
        return result;
    }

    /**
     * extract the Integer getValue of the pdu type
     *
     * @param pduString the string of the getValue+blankSpace+description of the
     * pdu type
     * @return the Integer getValue of the pdu type
     */
    private Integer getPduTypeValueFrom(String pduString) {
        Integer iVal;
        try {
            iVal = Integer.valueOf(new StringTokenizer(pduString, " ").nextToken());
        } catch (Exception e) {
            iVal = -1;
        }
        return iVal;
    }

    /**
     * get all selected pdu types
     *
     * @return ArrayList<Integer> of all pdu types that are selected
     */
    private ArrayList<Integer> getSelectedPduTypes() {
        ArrayList<Integer> theList = new ArrayList<Integer>();
        addToPduTypeList(theList, pduTypeTable1);
        addToPduTypeList(theList, pduTypeTable2);
        return theList;
    }

    private void addToPduTypeList(ArrayList<Integer> theList, JTable table) {
        for (Object entry : ((DefaultTableModel) table.getModel()).getDataVector()) {
            Vector vec = (Vector) entry;
            if ((Boolean) vec.elementAt(0)) {
                theList.add(getPduTypeValueFrom((String) vec.elementAt(1)));
            }
        }
    }

    /**
     * get all selected exercise id
     *
     * @return ArrayList<Integer> of all exercise id that are selected
     */
    private ArrayList<Integer> getSelectedExerciseId() {
        ArrayList<Integer> theList = new ArrayList<Integer>();
        for (Object entry : ((DefaultTableModel) exerciseIdTable.getModel()).getDataVector()) {
            Vector vec = (Vector) entry;
            if ((Boolean) vec.elementAt(0)) {
                theList.add((Integer) vec.elementAt(1));
            }
        }
        return theList;
    }

    private void initComponents() {

        protocolButtonGroup = new javax.swing.ButtonGroup();
        backPanel = new javax.swing.JPanel();
        connectionPanel = new javax.swing.JPanel();
        portLabel = new javax.swing.JLabel();
        ipAdressLabel = new javax.swing.JLabel();
        ipAdressTextField = new JFormattedTextField(new RegexFormatter("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}"));
        refreshTimeLabel = new javax.swing.JLabel();
        applicationLabel = new javax.swing.JLabel();
        siteLabel = new javax.swing.JLabel();
        selectAllIdCheckBox = new javax.swing.JCheckBox();
        excerciceScrollPane = new javax.swing.JScrollPane();
        exerciseIdTable = new javax.swing.JTable();
        multicastButton = new javax.swing.JRadioButton();
        broadcastButton = new javax.swing.JRadioButton();
        unicastButton = new javax.swing.JRadioButton();
        localhostButton = new javax.swing.JRadioButton();
        timeToLiveLabel = new javax.swing.JLabel();
        timeToLiveTextField = new javax.swing.JFormattedTextField();
        refreshTimeField = new javax.swing.JFormattedTextField();
        siteField = new javax.swing.JFormattedTextField();
        applicationField = new javax.swing.JFormattedTextField();
        portField = new javax.swing.JFormattedTextField();
        pduTypePanel = new javax.swing.JPanel();
        pduScrollPane2 = new javax.swing.JScrollPane();
        pduTypeTable2 = new javax.swing.JTable();
        pduScrollPane1 = new javax.swing.JScrollPane();
        pduTypeTable1 = new javax.swing.JTable();
        selectAllPduCheckBox = new javax.swing.JCheckBox();

        setPreferredSize(new java.awt.Dimension(380, 540));
        setLayout(null);

        backPanel.setPreferredSize(new java.awt.Dimension(380, 540));
        backPanel.setLayout(null);

        connectionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Dis connection"));
        connectionPanel.setPreferredSize(new java.awt.Dimension(350, 270));
        connectionPanel.setSize(new java.awt.Dimension(350, 270));
        connectionPanel.setLayout(null);

        portLabel.setText("Port");
        connectionPanel.add(portLabel);
        portLabel.setBounds(161, 22, 25, 16);

        ipAdressLabel.setText("Ip address");
        connectionPanel.add(ipAdressLabel);
        ipAdressLabel.setBounds(12, 22, 68, 16);

        ipAdressTextField.setText("239.255.255.250");
        ipAdressTextField.setBounds(12, 44, 123, 28);
        ((RegexFormatter) ipAdressTextField.getFormatter()).setOverwriteMode(false);
        ((RegexFormatter) ipAdressTextField.getFormatter()).setAllowsInvalid(true);
        connectionPanel.add(ipAdressTextField);


        refreshTimeLabel.setText("Refresh time");
        connectionPanel.add(refreshTimeLabel);
        refreshTimeLabel.setBounds(12, 158, 83, 16);
        
        javax.swing.JLabel refreshTimeUnit = new javax.swing.JLabel("millisec");
        connectionPanel.add(refreshTimeUnit);
        refreshTimeUnit.setBounds(165, 158, 50, 16);         

        applicationLabel.setText("Application");
        connectionPanel.add(applicationLabel);
        applicationLabel.setBounds(63, 84, 72, 16);

        siteLabel.setText("Site");
        connectionPanel.add(siteLabel);
        siteLabel.setBounds(18, 84, 33, 16);

        selectAllIdCheckBox.setText("Select all ID");
        selectAllIdCheckBox.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectAllIdCheckBoxActionPerformed(evt);
            }
        });
        connectionPanel.add(selectAllIdCheckBox);
        selectAllIdCheckBox.setBounds(245, 227, 106, 23);

        excerciceScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        excerciceScrollPane.setPreferredSize(new java.awt.Dimension(100, 170));

        exerciseIdTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "Exercise", "ID"
                }) {

            Class[] types = new Class[]{
                java.lang.Boolean.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean[]{
                true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        excerciceScrollPane.setViewportView(exerciseIdTable);

        exerciseIdTable.getColumnModel().getColumn(0).setHeaderValue("Exercise");
        exerciseIdTable.getColumnModel().getColumn(1).setHeaderValue("ID");

        connectionPanel.add(excerciceScrollPane);
        excerciceScrollPane.setBounds(245, 22, 121, 199);

        protocolButtonGroup.add(multicastButton);
        multicastButton.setSelected(true);
        multicastButton.setText("Multicast");
        connectionPanel.add(multicastButton);
        multicastButton.setBounds(12, 198, 90, 23);

        protocolButtonGroup.add(broadcastButton);
        broadcastButton.setText("Broadcast");
        connectionPanel.add(broadcastButton);
        broadcastButton.setBounds(12, 233, 93, 23);

        protocolButtonGroup.add(unicastButton);
        unicastButton.setText("Unicast");
        connectionPanel.add(unicastButton);
        unicastButton.setBounds(111, 198, 79, 23);

        protocolButtonGroup.add(localhostButton);
        localhostButton.setText("Localhost");
        connectionPanel.add(localhostButton);
        localhostButton.setBounds(111, 233, 93, 23);

        timeToLiveLabel.setText("Time to live");
        connectionPanel.add(timeToLiveLabel);
        timeToLiveLabel.setBounds(153, 84, 74, 16);

        timeToLiveTextField.setText("255");
        connectionPanel.add(timeToLiveTextField);
        timeToLiveTextField.setBounds(153, 106, 51, 28);

        refreshTimeField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        refreshTimeField.setText("1000");
        connectionPanel.add(refreshTimeField);
        refreshTimeField.setBounds(101, 152, 54, 28);

        siteField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        siteField.setText("-1");
        connectionPanel.add(siteField);
        siteField.setBounds(12, 106, 39, 28);

        applicationField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        applicationField.setText("-1");
        connectionPanel.add(applicationField);
        applicationField.setBounds(63, 106, 59, 28);

        portField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        portField.setText("3000");
        connectionPanel.add(portField);
        portField.setBounds(147, 44, 60, 28);

        backPanel.add(connectionPanel);
        connectionPanel.setBounds(0, 0, 378, 270);

        pduTypePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Pdu types"));
        pduTypePanel.setPreferredSize(new java.awt.Dimension(350, 250));
        pduTypePanel.setLayout(null);

        pduScrollPane2.setPreferredSize(new java.awt.Dimension(160, 4));

        pduTypeTable2.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{"", "PDU type"}) {

            Class[] types = new Class[]{
                java.lang.Boolean.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        pduScrollPane2.setViewportView(pduTypeTable2);
        pduTypeTable2.getColumnModel().getColumn(0).setHeaderValue("");
        pduTypeTable2.getColumnModel().getColumn(1).setHeaderValue("PDU type");

        pduTypePanel.add(pduScrollPane2);
        pduScrollPane2.setBounds(195, 28, 170, 210);

        pduTypeTable1.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{"", "PDU type"}) {

            Class[] types = new Class[]{
                java.lang.Boolean.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        pduScrollPane1.setViewportView(pduTypeTable1);
        pduTypeTable1.getColumnModel().getColumn(0).setHeaderValue("");
        pduTypeTable1.getColumnModel().getColumn(1).setHeaderValue("PDU type");
        pduTypePanel.add(pduScrollPane1);
        pduScrollPane1.setBounds(12, 28, 167, 182);

        selectAllPduCheckBox.setText("Select all PDU types");
        selectAllPduCheckBox.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectAllPduCheckBoxActionPerformed(evt);
            }
        });
        pduTypePanel.add(selectAllPduCheckBox);
        selectAllPduCheckBox.setBounds(12, 215, 171, 23);

        backPanel.add(pduTypePanel);
        pduTypePanel.setBounds(0, 270, 377, 250);

        add(backPanel);
        backPanel.setBounds(0, 0, 380, 530);
    }

    private void selectAllIdCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
        for (int i = 0; i < ((DefaultTableModel) exerciseIdTable.getModel()).getRowCount(); i++) {
            ((DefaultTableModel) exerciseIdTable.getModel()).setValueAt(selectAllIdCheckBox.isSelected(), i, 0);
        }
    }

    private void selectAllPduCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
        for (int i = 0; i < ((DefaultTableModel) pduTypeTable1.getModel()).getRowCount(); i++) {
            ((DefaultTableModel) pduTypeTable1.getModel()).setValueAt(selectAllPduCheckBox.isSelected(), i, 0);
        }
        for (int i = 0; i < ((DefaultTableModel) pduTypeTable2.getModel()).getRowCount(); i++) {
            ((DefaultTableModel) pduTypeTable2.getModel()).setValueAt(selectAllPduCheckBox.isSelected(), i, 0);
        }
    }
    private javax.swing.JFormattedTextField applicationField;
    private javax.swing.JLabel applicationLabel;
    private javax.swing.JPanel backPanel;
    private javax.swing.JRadioButton broadcastButton;
    private javax.swing.JPanel connectionPanel;
    private javax.swing.JScrollPane excerciceScrollPane;
    private javax.swing.JTable exerciseIdTable;
    private javax.swing.JLabel ipAdressLabel;
    private javax.swing.JFormattedTextField ipAdressTextField;
    private javax.swing.JRadioButton localhostButton;
    private javax.swing.JRadioButton multicastButton;
    private javax.swing.JScrollPane pduScrollPane1;
    private javax.swing.JScrollPane pduScrollPane2;
    private javax.swing.JPanel pduTypePanel;
    private javax.swing.JTable pduTypeTable1;
    private javax.swing.JTable pduTypeTable2;
    private javax.swing.JFormattedTextField portField;
    private javax.swing.JLabel portLabel;
    private javax.swing.ButtonGroup protocolButtonGroup;
    private javax.swing.JFormattedTextField refreshTimeField;
    private javax.swing.JLabel refreshTimeLabel;
    private javax.swing.JCheckBox selectAllIdCheckBox;
    private javax.swing.JCheckBox selectAllPduCheckBox;
    private javax.swing.JFormattedTextField siteField;
    private javax.swing.JLabel siteLabel;
    private javax.swing.JLabel timeToLiveLabel;
    private javax.swing.JFormattedTextField timeToLiveTextField;
    private javax.swing.JRadioButton unicastButton;
}
