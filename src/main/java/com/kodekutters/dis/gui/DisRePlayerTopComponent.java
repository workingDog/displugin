/**
 * DisRePlayerTopComponent
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
package com.kodekutters.dis.gui;

import com.terramenta.globe.WorldWindManager;
import com.kodekutters.dis.core.DisDirectorySupport;
import com.kodekutters.dis.core.DisReplaySupport;
import com.kodekutters.dis.core.DisRetransmitSupport;
import com.kodekutters.dis.entity.Entity;
import com.kodekutters.dis.entity.EntityChildFactory;
import com.kodekutters.dis.entity.EntityListRootNode;
import com.kodekutters.dis.layer.DisLayer;
import com.kodekutters.dis.util.AnnotationDragger;
import com.kodekutters.dis.util.EntitySelector;
import com.kodekutters.dis.system.EntityExplorerManager;
import com.kodekutters.dis.util.DisFileChecker;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.*;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.ListView;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle.Messages;

/**
 * Top component that sets-up the DIS connection parameters, reading from file
 * and writing to the wire
 */
@ConvertAsProperties(dtd = "-//com.kodekutters.dis.gui//DisRePlayer//EN",
autostore = false)
@TopComponent.Description(preferredID = "DisRePlayerTopComponent",
iconBase = "com/kodekutters/dis/image/resources/signal.png",
persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "rightSide", openAtStartup = true)
@ActionID(category = "Window", id = "com.kodekutters.dis.gui.DisRePlayerTopComponent")
@ActionReference(path = "Menu/Window" /*
 * , position = 333
 */)
@TopComponent.OpenActionRegistration(displayName = "#CTL_DisRePlayerAction",
preferredID = "DisRePlayerTopComponent")
@Messages({
    "CTL_DisRePlayerAction=Dis Replay",
    "CTL_DisRePlayerTopComponent=Dis Replay",
    "HINT_DisRePlayerTopComponent=Dis Replay setup"
})
public final class DisRePlayerTopComponent extends TopComponent implements ExplorerManager.Provider, Lookup.Provider {

    protected DisLayer disLayer;
    public DisConnectPanel disPanel;
    private static final ForceChooserTopComponent forceChooser = Lookup.getDefault().lookup(ForceChooserTopComponent.class);
    private static final WorldWindManager wwm = Lookup.getDefault().lookup(WorldWindManager.class);
    // this is enough to setup the entity selector
    private static final EntitySelector entitySelector = Lookup.getDefault().lookup(EntitySelector.class);
    private final ExplorerManager mgr = EntityExplorerManager.SHARED_ENTITY_MANAGER;
    private javax.swing.JScrollPane explorerPanel = new ListView();

    public DisRePlayerTopComponent() {

        disPanel = new DisConnectPanel();
        disPanel.setDisSupport(new DisReplaySupport());

        disLayer = (DisLayer) wwm.getWorldWindow().getModel().getLayers().getLayerByName("Dis replayer");
        if (disLayer == null) {
            disLayer = new DisLayer("Dis replayer");
            wwm.getLayers().add(disLayer);
            wwm.getLayers().add(disLayer.getAnnotationLayer());
            wwm.getLayers().add(disLayer.getHistoryLayer());
        }

        // give the DisSupport to the disLayer
        disLayer.setDisSupport(disPanel.getDisSupport());
        disLayer.getDisSupport().addListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                // when all the data has been processed or if there is a fault
                if (ev.getActionCommand().equals(DisReplaySupport.END_OF_DATA)
                        || ev.getActionCommand().equals(DisReplaySupport.ACTION_COMMAND_FAIL)) {
                    stopButtonActionPerformed(null);
                }
            }
        });

        wwm.getWorldWindow().addSelectListener(new AnnotationDragger(wwm.getWorldWindow()));

        if ((forceChooser != null) && !ForceChooserTopComponent.getDisLayerList().contains(disLayer)) {
            forceChooser.addLayer(disLayer);
        }

        // listen to the info timer events
        ActionListener timerEventListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                disPanel.getDisService().actionPerformed(ae);
                showDurationAsString(disPanel.getDisService().getDuration());
                packetCount.setText(String.valueOf(disPanel.getDisSupport().getNumberOfPackets()));
            }
        };

        // give the DisSupport a new info timer, and we will listen for it here
        disPanel.getDisService().setInfoTimer(new Timer(1000, timerEventListener));

        initComponents();

        setName(Bundle.CTL_DisRePlayerTopComponent());
        setToolTipText(Bundle.HINT_DisRePlayerTopComponent());

        // this is the ListView of mgr
        explorerPanel.setVisible(false); // hide the ListView
        explorerPanel.setBounds(5, 680, 370, 200);
        explorerPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        backPanel.add(explorerPanel);

        // puts the entities in the shared mgr via the explorerPanel
        associateLookup(ExplorerUtils.createLookup(mgr, getActionMap()));
        mgr.setRootContext(new EntityListRootNode(new EntityChildFactory(new ArrayList<Entity>())));

        disPanel.setBounds(0, 0, 380, 540);
        backPanel.add(disPanel);

        stopButton.setEnabled(false);
        startButton.setEnabled(true);
        pauseButton.setEnabled(false);
        pauseButton.setSelected(false);

        dirTextBackground = dirTextField.getBackground();
    }

    public DisLayer getDisLayer() {
        return disLayer;
    }

    public void setDisLayer(DisLayer layer) {
        disLayer = layer;
    }

    private void setEnableAll(boolean onof) {
        disPanel.setEnableAll(onof);
        dirTextField.setEnabled(onof);
        fileBrowserButton.setEnabled(onof);
        retransmit.setEnabled(onof);
    }

    private boolean isFileForReplayOk() {
        // if have a valid directory or a valid file
        if (DisFileChecker.isDirectoryOk(dirTextField.getText().trim()) || DisFileChecker.isFileOk(dirTextField.getText().trim())) {
            ((DisDirectorySupport) disPanel.getDisSupport()).setDirectoryName(dirTextField.getText().trim());
            dirTextField.setBackground(dirTextBackground);
            return true;
        } else {
            dirTextField.setBackground(disPanel.warningColor);
            return false;
        }
    }

    // test for valid file and connection parameters
    private boolean haveValidInput() {
        return this.isFileForReplayOk() && disPanel.isConnectionOk();
    }

    private void initComponents() {

        this.setLayout(new BorderLayout());

        protocolButtonGroup = new javax.swing.ButtonGroup();
        maiScrollPane = new javax.swing.JScrollPane();
        backPanel = new javax.swing.JPanel();
        controlPanel = new javax.swing.JPanel();
        startButton = new javax.swing.JToggleButton();
        pauseButton = new javax.swing.JToggleButton();
        stopButton = new javax.swing.JToggleButton();
        filePanel = new javax.swing.JPanel();
        directoryLabel = new javax.swing.JLabel();
        dirTextField = new javax.swing.JTextField();
        packetLabel = new javax.swing.JLabel();
        packetCount = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();
        timeUnitLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();
        fileBrowserButton = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(400, 680));

        maiScrollPane.setPreferredSize(new java.awt.Dimension(380, 690));

        backPanel.setPreferredSize(new java.awt.Dimension(380, 680));
        backPanel.setLayout(null);

        controlPanel.setPreferredSize(new java.awt.Dimension(350, 80));
        controlPanel.setLayout(null);

        startButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/kodekutters/dis/image/resources/Button Play.png"))); // NOI18N
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });
        controlPanel.add(startButton);
        startButton.setBounds(22, 6, 48, 48);

        pauseButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/kodekutters/dis/image/resources/Button Pause.png"))); // NOI18N
        pauseButton.setEnabled(false);
        pauseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pauseButtonActionPerformed(evt);
            }
        });
        controlPanel.add(pauseButton);
        pauseButton.setBounds(76, 6, 48, 48);

        stopButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/kodekutters/dis/image/resources/Button Stop.png"))); // NOI18N
        stopButton.setEnabled(false);
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopButtonActionPerformed(evt);
            }
        });
        controlPanel.add(stopButton);
        stopButton.setBounds(130, 6, 48, 48);

        backPanel.add(controlPanel);
        controlPanel.setBounds(0, 526, 377, 64);

        packetCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        packetCount.setText("0");
        controlPanel.add(packetCount);
        packetCount.setBounds(210, 4, 55, 16);

        packetLabel.setText("packets");
        controlPanel.add(packetLabel);
        packetLabel.setBounds(290, 4, 49, 16);

        controlPanel.add(progressBar);
        progressBar.setBounds(210, 27, 130, 15);

        timeLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        timeLabel.setText("0");
        controlPanel.add(timeLabel);
        timeLabel.setBounds(210, 50, 40, 16);

        timeUnitLabel.setText("seconds");
        controlPanel.add(timeUnitLabel);
        timeUnitLabel.setBounds(290, 50, 61, 16);

        filePanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        filePanel.setPreferredSize(new java.awt.Dimension(350, 140));
        filePanel.setLayout(null);

        directoryLabel.setText("File");
        filePanel.add(directoryLabel);
        directoryLabel.setBounds(8, 23, 34, 16);
        dirTextField.setText(System.getProperty("user.home"));
        filePanel.add(dirTextField);
        dirTextField.setBounds(40, 18, 213, 28);

        fileBrowserButton.setText("Browse...");
        fileBrowserButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileBrowserButtonActionPerformed(evt);
            }
        });
        filePanel.add(fileBrowserButton);
        fileBrowserButton.setBounds(260, 18, 95, 29);

        retransmit = new JCheckBox("Retransmit DIS reading");
        retransmit.setSelected(false);
        retransmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retransmitActionPerformed(evt);
            }
        });
        retransmit.setBounds(8, 50, 220, 29);
        filePanel.add(retransmit);

        filePanel.setBounds(0, 596, 377, 90);
        backPanel.add(filePanel);

        maiScrollPane.setViewportView(backPanel);

        add(maiScrollPane, java.awt.BorderLayout.CENTER);
    }

    private void retransmitActionPerformed(java.awt.event.ActionEvent evt) {
        fileBrowserButton.setEnabled(!retransmit.isSelected());
        dirTextField.setEnabled(!retransmit.isSelected());
        if (retransmit.isSelected()) {
            DisLayer disReadingLayer = (DisLayer) wwm.getWorldWindow().getModel().getLayers().getLayerByName("Dis viewer");
            if (disReadingLayer != null) {
                disPanel.setDisSupport(new DisRetransmitSupport());
                // let our DisRetransmitSupport listen to the disReadingLayer
                disReadingLayer.getDisSupport().addListener(disPanel.getDisSupport());
            } else {
                System.out.println("....ERROR.... you must have a Dis Viewer component installed");
            }
        } else {
            // stop the old support if any
            if ((disPanel.getDisSupport() != null) && (disPanel.getDisSupport() instanceof DisRetransmitSupport) && disPanel.getDisSupport().isRunning()) {
                disPanel.getDisSupport().stop();
            }
            // put a "normal" dis replay support
            disPanel.setDisSupport(new DisReplaySupport());
        }
    }

    private void enableStart(boolean test) {
        // start only if the test is true
        if (test) {
            stopButton.setEnabled(true);
            startButton.setEnabled(false);
            pauseButton.setEnabled(true);
            pauseButton.setSelected(false);

            setEnableAll(false);
            progressBar.setBackground(Color.cyan);
            progressBar.setForeground(Color.green);
            progressBar.setIndeterminate(true);

            // remove all previous icons from the layers
            if (getDisLayer() != null) {
                getDisLayer().resetLayer();
            }

            disPanel.getDisService().start();

        } else {
            stopButton.setEnabled(false);
            startButton.setEnabled(true);
            pauseButton.setEnabled(false);
            pauseButton.setSelected(false);
            showDurationAsString(0);
            packetCount.setText("0");
            setEnableAll(true);
        }
    }

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {
        // if the pause button is pressed, do a re-start
        if (pauseButton.isSelected()) {
            pauseButton.setEnabled(true);
            startButton.setEnabled(false);
            stopButton.setEnabled(true);

            disPanel.getDisService().restart();
            return;
        }

        // start only if the parameters are valid
        if (retransmit.isSelected()) {
            enableStart(disPanel.isConnectionOk());
        } else {
            enableStart(haveValidInput());
        }
    }

    private void pauseButtonActionPerformed(java.awt.event.ActionEvent evt) {
        stopButton.setEnabled(true);
        startButton.setEnabled(true);
        pauseButton.setEnabled(false);
        pauseButton.setSelected(true);  // <-----

        disPanel.getDisService().pause();
    }

    private void stopButtonActionPerformed(java.awt.event.ActionEvent evt) {
        stopButton.setEnabled(false);
        startButton.setEnabled(true);
        pauseButton.setEnabled(false);
        pauseButton.setSelected(false);

        setEnableAll(true);
        progressBar.setIndeterminate(false);

        disPanel.getDisService().stop();

        if (retransmit.isSelected()) {
            fileBrowserButton.setEnabled(false);
            dirTextField.setEnabled(false);
        }
    }

    private void fileBrowserButtonActionPerformed(java.awt.event.ActionEvent evt) {
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fc.setMultiSelectionEnabled(false);
        if (fc.showOpenDialog(this) == JFileChooser.OPEN_DIALOG) {
            dirTextField.setText(fc.getSelectedFile().getAbsolutePath().trim().toString());
        }
    }
    private javax.swing.JPanel backPanel;
    private javax.swing.JPanel controlPanel;
    private javax.swing.JLabel directoryLabel;
    private javax.swing.JTextField dirTextField;
    private javax.swing.JButton fileBrowserButton;
    private javax.swing.JPanel filePanel;
    private javax.swing.JScrollPane maiScrollPane;
    private javax.swing.JLabel packetCount;
    private javax.swing.JLabel packetLabel;
    private javax.swing.JToggleButton pauseButton;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.ButtonGroup protocolButtonGroup;
    private javax.swing.JToggleButton startButton;
    private javax.swing.JToggleButton stopButton;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JLabel timeUnitLabel;
    private Color dirTextBackground;
    private javax.swing.JCheckBox retransmit;

    @Override
    public void componentOpened() {
    }

    @Override
    public void componentClosed() {
        forceChooser.removeLayer(disLayer);
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    /**
     * show the duration in timeLabel and a string seconds, minutes or hours in
     * timeUnitLabel
     *
     * @param duration in seconds
     */
    private void showDurationAsString(double duration) {
        if (duration < 60) {
            timeUnitLabel.setText("seconds");
        } else if ((duration >= 60) && (duration < 3600)) {
            duration = duration / 60;
            timeUnitLabel.setText("minutes");
        } else if ((duration >= 3600)) {
            duration = duration / 3600;
            timeUnitLabel.setText("hours");
        }
        timeLabel.setText(String.valueOf(new DecimalFormat("#.#").format(duration)));
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return mgr;
    }
}
