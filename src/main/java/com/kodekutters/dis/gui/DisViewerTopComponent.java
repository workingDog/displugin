/**
 * DisViewerTopComponent
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
package com.kodekutters.dis.gui;

import com.terramenta.globe.WorldWindManager;
import com.kodekutters.dis.core.DisDirectorySupport;
import com.kodekutters.dis.core.DisFileSupport;
import com.kodekutters.dis.core.DisViewSupport;
import com.kodekutters.dis.entity.Entity;
import com.kodekutters.dis.entity.EntityChildFactory;
import com.kodekutters.dis.entity.EntityListRootNode;
import com.kodekutters.dis.layer.DisLayer;
import com.kodekutters.dis.system.EntityExplorerManager;
import com.kodekutters.dis.util.AnnotationDragger;
import com.kodekutters.dis.util.DisAreaMonitor;
import com.kodekutters.dis.util.EntitySelector;
import com.kodekutters.dis.util.PropertiesReader;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.ListView;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle.Messages;

/**
 * Visual Top component which sets-up the DIS connection parameters, reading from the
 * wire and writing the DIS packets to file.
 */
@ConvertAsProperties(dtd = "-//com.kodekutters.dis.gui//DisViewer//EN",
autostore = false)
@TopComponent.Description(preferredID = "DisViewerTopComponent",
iconBase = "com/kodekutters/dis/image/resources/globe.png",
persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "rightSide", openAtStartup = true)
@ActionID(category = "Window", id = "com.kodekutters.dis.gui.DisViewerTopComponent")
@ActionReference(path = "Menu/Window" /*
 * , position = 333
 */)
@TopComponent.OpenActionRegistration(displayName = "#CTL_DisViewerAction",
preferredID = "DisViewerTopComponent")
@Messages({
    "CTL_DisViewerAction=Dis Viewer",
    "CTL_DisViewerTopComponent=Dis Viewer",
    "HINT_DisViewerTopComponent=Dis Viewer setup"
})
public final class DisViewerTopComponent extends TopComponent implements ExplorerManager.Provider, Lookup.Provider {

    protected DisLayer disLayer;
    public DisConnectPanel disPanel;
    private static final ForceChooserTopComponent forceChooser = Lookup.getDefault().lookup(ForceChooserTopComponent.class);
    private static final WorldWindManager wwm = Lookup.getDefault().lookup(WorldWindManager.class);
    // this is enough to setup the entity selector
    private static final EntitySelector entitySelector = Lookup.getDefault().lookup(EntitySelector.class);
    private final ExplorerManager mgr = EntityExplorerManager.SHARED_ENTITY_MANAGER;
    private final javax.swing.JScrollPane explorerPanel = new ListView();

    public DisViewerTopComponent() {
        
        String homeDir = System.getProperty("user.home");
      
        // load the properties from file
        PropertiesReader.getInstance().loadProperties(homeDir + "/Dis.properties");
        //
        // the first part of this topcomponent is the connection panel
        disPanel = new DisConnectPanel();
        disPanel.setDisSupport(new DisViewSupport());

        disLayer = (DisLayer) wwm.getWorldWindow().getModel().getLayers().getLayerByName("Dis viewer");
        if (disLayer == null) {
            disLayer = new DisLayer("Dis viewer");
            wwm.getLayers().add(disLayer);
            wwm.getLayers().add(disLayer.getAnnotationLayer());
            wwm.getLayers().add(disLayer.getHistoryLayer());
        }

        // give the DisSupport to the disLayer
        disLayer.setDisSupport(disPanel.getDisSupport());

        wwm.getWorldWindow().addSelectListener(new AnnotationDragger(wwm.getWorldWindow()));

        if ((forceChooser != null) && !ForceChooserTopComponent.getDisLayerList().contains(disLayer)) {
            forceChooser.addLayer(disLayer);
        }

        // to listen to the info timer events
        ActionListener timerEventListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                disPanel.getDisService().actionPerformed(ae);
                showDurationAsString(disPanel.getDisService().getDuration());
                packetCount.setText(String.valueOf(disPanel.getDisSupport().getNumberOfPackets()));
            }
        };

        // give the DisSupport a new info timer, and we will listen for it here
        disPanel.getDisService().setInfoTimer(new Timer(1000, timerEventListener));

        initComponents();
        setName(Bundle.CTL_DisViewerTopComponent());
        setToolTipText(Bundle.HINT_DisViewerTopComponent());

        disPanel.setBounds(0, 0, 380, 540);
        backPanel.add(disPanel);

        stopButton.setEnabled(false);
        startButton.setEnabled(true);
        pauseButton.setEnabled(false);
        browseButton.setEnabled(true);

        dirTextBackground = dirTextField.getBackground();
        nPduPerFileBackground = nPduPerFileTextField.getBackground();

        // this is the ListView of mgr
        explorerPanel.setVisible(false); // hide the ListView
        explorerPanel.setBounds(5, 750, 340, 200);
        explorerPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        backPanel.add(explorerPanel);

        // put the entities in the shared mgr via the explorerPanel
        associateLookup(ExplorerUtils.createLookup(mgr, getActionMap()));
        mgr.setRootContext(new EntityListRootNode(new EntityChildFactory(new ArrayList<Entity>())));

    }

    public DisLayer getDisLayer() {
        return disLayer;
    }

    public void setDisLayer(DisLayer layer) {
        disLayer = layer;
    }

    /**
     *
     * check that the file in the fileName field is correct and the number of
     * pdu per file is valid
     *
     * @return true if the file and number are valid
     */
    private boolean isWriteFileOk() {
        // check things only if we want to save to file
        if (savePduCheckBox.isSelected()) {
            if (dirTextField.getText().isEmpty()) {
                dirTextField.setBackground(disPanel.warningColor);
                return false;
            } else {
                // check the number of pdu per file 
                if (!nPduPerFileTextField.getText().isEmpty()) {
                    try {
                        int tVal = Integer.valueOf(nPduPerFileTextField.getText()).intValue();
                        if (tVal <= 0) {
                            nPduPerFileTextField.setBackground(disPanel.warningColor);
                            return false;
                        } else {
                            ((DisFileSupport) disPanel.getDisSupport()).setPdusPerFile(tVal);
                            nPduPerFileTextField.setBackground(nPduPerFileBackground);
                        }
                    } catch (Exception e) {
                        System.err.println("Error parsing nPduPerFileTextField " + e);
                        nPduPerFileTextField.setBackground(disPanel.warningColor);
                        return false;
                    }
                } else {
                    nPduPerFileTextField.setBackground(disPanel.warningColor);
                    return false;
                }
                File dirToWriteTo = new File(dirTextField.getText());
                if (dirToWriteTo != null) {
                    ((DisDirectorySupport) disPanel.getDisSupport()).setDirectoryName(dirToWriteTo.getAbsolutePath());
                    dirTextField.setBackground(dirTextBackground);
                    return true;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    // test for valid file and connection parameters
    private boolean haveValidInput() {
       return this.isWriteFileOk() && disPanel.isConnectionOk();     
    }

    private void initComponents() {

        this.setLayout(new BorderLayout());

        maiScrollPane = new javax.swing.JScrollPane();
        backPanel = new javax.swing.JPanel();
        controlPanel = new javax.swing.JPanel();
        startButton = new javax.swing.JToggleButton();
        pauseButton = new javax.swing.JToggleButton();
        stopButton = new javax.swing.JToggleButton();
        browseButton = new javax.swing.JButton();
        autoStartButton = new javax.swing.JToggleButton();
        packetLabel = new javax.swing.JLabel();
        packetCount = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();
        timeUnitLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();
        filePanel = new javax.swing.JPanel();
        directoryLabel = new javax.swing.JLabel();
        dirTextField = new javax.swing.JTextField();
        nPduLabel = new javax.swing.JLabel();
        nPduPerFileTextField = new javax.swing.JFormattedTextField();
        savePduCheckBox = new javax.swing.JCheckBox();
        disAreaCheckBox = new javax.swing.JCheckBox();

        setPreferredSize(new java.awt.Dimension(400, 760));

        maiScrollPane.setPreferredSize(new java.awt.Dimension(380, 770));

        backPanel.setPreferredSize(new java.awt.Dimension(380, 770));
        backPanel.setLayout(null);

        controlPanel.setPreferredSize(new java.awt.Dimension(380, 80));
        controlPanel.setLayout(null);

        startButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/kodekutters/dis/image/resources/Button Play.png"))); // NOI18N
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });
        controlPanel.add(startButton);
        startButton.setBounds(6, 6, 48, 48);

        pauseButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/kodekutters/dis/image/resources/Button Pause.png"))); // NOI18N
        pauseButton.setEnabled(false);
        pauseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pauseButtonActionPerformed(evt);
            }
        });
        controlPanel.add(pauseButton);
        pauseButton.setBounds(66, 6, 40, 48);

        stopButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/kodekutters/dis/image/resources/Button Stop.png"))); // NOI18N
        stopButton.setEnabled(false);
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopButtonActionPerformed(evt);
            }
        });
        controlPanel.add(stopButton);
        stopButton.setBounds(118, 6, 40, 48);

        autoStartButton.setText("Auto start/stop is off");
        autoStartButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoStartButtonActionPerformed(evt);
            }
        });
        controlPanel.add(autoStartButton);
        autoStartButton.setBounds(0, 60, 179, 29);

        packetCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        packetCount.setText("0");
        controlPanel.add(packetCount);
        packetCount.setBounds(210, 6, 55, 16);

        packetLabel.setText("packets");
        controlPanel.add(packetLabel);
        packetLabel.setBounds(290, 6, 49, 16);

        controlPanel.add(progressBar);
        progressBar.setBounds(210, 30, 130, 15);

        timeLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        timeLabel.setText("0");
        controlPanel.add(timeLabel);
        timeLabel.setBounds(210, 53, 40, 16);

        timeUnitLabel.setText("seconds");
        controlPanel.add(timeUnitLabel);
        timeUnitLabel.setBounds(290, 53, 61, 16);

        backPanel.add(controlPanel);
        controlPanel.setBounds(0, 530, 370, 100);

        filePanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        filePanel.setPreferredSize(new java.awt.Dimension(380, 105));
        filePanel.setLayout(null);

        directoryLabel.setText("Directory");
        filePanel.add(directoryLabel);
        directoryLabel.setBounds(8, 43, 71, 16);

        dirTextField.setText(System.getProperty("user.home"));
        filePanel.add(dirTextField);
        dirTextField.setBounds(80, 37, 178, 28);

        browseButton.setText("Browse...");
        filePanel.add(browseButton);
        browseButton.setBounds(265, 37, 90, 28);
        browseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseButtonActionPerformed(evt);
            }
        });

        nPduLabel.setText("Number of PDU per file");
        filePanel.add(nPduLabel);
        nPduLabel.setBounds(8, 77, 149, 16);

        nPduPerFileTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#"))));
        nPduPerFileTextField.setText("10000");
        filePanel.add(nPduPerFileTextField);
        nPduPerFileTextField.setBounds(169, 71, 104, 28);

        savePduCheckBox.setSelected(false);
        ((DisFileSupport) disPanel.getDisSupport()).setAlsoWriteToFile(false);
        savePduCheckBox.setText("Save PDU to file");
        savePduCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savePduCheckBoxActionPerformed(evt);
            }
        });
        filePanel.add(savePduCheckBox);
        savePduCheckBox.setBounds(2, 8, 131, 23);
        
        disAreaCheckBox.setSelected(false);
        disAreaCheckBox.setText("Show monitoring area");
        disAreaCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DisAreaMonitor.getInstance().showArea(disAreaCheckBox.isSelected());
            }
        });
        backPanel.add(disAreaCheckBox);
        disAreaCheckBox.setBounds(2, 740, 200, 30);
        
        
        backPanel.add(filePanel);
        filePanel.setBounds(0, 630, 370, 105);

        maiScrollPane.setViewportView(backPanel);

        add(maiScrollPane, BorderLayout.CENTER);
    }

    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt) {
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setMultiSelectionEnabled(false);
        if (fc.showOpenDialog(this) == JFileChooser.OPEN_DIALOG) {
            dirTextField.setText(fc.getSelectedFile().getAbsolutePath().toString());
        }
    }

    private void autoStartButtonActionPerformed(java.awt.event.ActionEvent evt) {
        pauseButton.setEnabled(false);
        startButton.setEnabled(true);
        stopButton.setEnabled(false);
        pauseButton.setSelected(false);  
        browseButton.setEnabled(true);
        
        if (autoStartButton.isSelected()) {
            // start only if the input parameters are valid
            if (haveValidInput()) {
                disPanel.setEnableAll(false);
                // must tick the start and stop pdu types in the table
                disPanel.setStartStopPduTypes(true);
                disPanel.getDisService().autoStart();
                showDurationAsString(0);
                packetCount.setText("0");
                autoStartButton.setText("Auto start/stop is on");

                savePduCheckBox.setEnabled(false);
                dirTextField.setEnabled(false);
                nPduPerFileTextField.setEnabled(false);

                startButton.setEnabled(false);
                browseButton.setEnabled(false);
                progressBar.setIndeterminate(true);
                return;
            } else {
                autoStartButton.setSelected(false);
            }
        } else {
            disPanel.getDisService().autoStop();
        }

        progressBar.setIndeterminate(false);
        savePduCheckBox.setEnabled(true);
        dirTextField.setEnabled(true);
        nPduPerFileTextField.setEnabled(true);

        disPanel.setEnableAll(true);
        autoStartButton.setText("Auto start/stop is off");
    }

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {
        // if the pause button is currently pressed, do a re-start
        if (pauseButton.isSelected()) {
            pauseButton.setEnabled(true);
            startButton.setEnabled(false);
            stopButton.setEnabled(true);
            autoStartButton.setEnabled(false);
            browseButton.setEnabled(false);

            disPanel.getDisService().restart();
            return;
        }

        // start only if the parameters are valid
        if (haveValidInput()) {
            stopButton.setEnabled(true);
            startButton.setEnabled(false);
            pauseButton.setEnabled(true);
            pauseButton.setSelected(false);
            autoStartButton.setEnabled(false);
            browseButton.setEnabled(false);
            savePduCheckBox.setEnabled(false);
            dirTextField.setEnabled(false);
            nPduPerFileTextField.setEnabled(false);

            disPanel.setEnableAll(false);
            progressBar.setIndeterminate(true);

            // remove all previous icons from the layer
            if (getDisLayer() != null) {
                getDisLayer().resetLayer();
            }

            // clear all nodes
            mgr.setRootContext(new EntityListRootNode(new EntityChildFactory(new ArrayList<Entity>())));

            disPanel.getDisService().start();
        } else {
            stopButton.setEnabled(false);
            startButton.setEnabled(true);
            browseButton.setEnabled(true);
            pauseButton.setEnabled(false);
            pauseButton.setSelected(false);
            autoStartButton.setEnabled(true);
            savePduCheckBox.setEnabled(true);
            dirTextField.setEnabled(true);
            nPduPerFileTextField.setEnabled(true);
            showDurationAsString(0);
            packetCount.setText("0");
            disPanel.setEnableAll(true);
        }
    }

    private void pauseButtonActionPerformed(java.awt.event.ActionEvent evt) {
        stopButton.setEnabled(true);
        startButton.setEnabled(true);
        pauseButton.setEnabled(false);
        pauseButton.setSelected(true);  // <-----
        autoStartButton.setEnabled(false);
        browseButton.setEnabled(false);

        disPanel.getDisService().pause();
    }

    private void savePduCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {
        if (savePduCheckBox.isSelected()) {
            ((DisFileSupport) disPanel.getDisSupport()).setAlsoWriteToFile(true);
            dirTextField.setEnabled(true);
            nPduPerFileTextField.setEnabled(true);
        } else {
            ((DisFileSupport) disPanel.getDisSupport()).setAlsoWriteToFile(false);
            dirTextField.setEnabled(false);
            nPduPerFileTextField.setEnabled(false);
        }
        nPduPerFileTextField.setBackground(nPduPerFileBackground);
    }

    private void stopButtonActionPerformed(java.awt.event.ActionEvent evt) {
        stopButton.setEnabled(false);
        startButton.setEnabled(true);
        pauseButton.setEnabled(false);
        pauseButton.setSelected(false);
        autoStartButton.setEnabled(true);
        browseButton.setEnabled(true);
        savePduCheckBox.setEnabled(true);
        dirTextField.setEnabled(true);

        disPanel.setEnableAll(true);
        progressBar.setIndeterminate(false);

        disPanel.getDisService().stop();
    }
    private javax.swing.JToggleButton autoStartButton;
    private javax.swing.JPanel backPanel;
    private javax.swing.JPanel controlPanel;
    private javax.swing.JLabel directoryLabel;
    private javax.swing.JTextField dirTextField;
    private javax.swing.JPanel filePanel;
    private javax.swing.JScrollPane maiScrollPane;
    private javax.swing.JLabel nPduLabel;
    private javax.swing.JFormattedTextField nPduPerFileTextField;
    private javax.swing.JLabel packetCount;
    private javax.swing.JLabel packetLabel;
    private javax.swing.JToggleButton pauseButton;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JCheckBox savePduCheckBox;   
    private javax.swing.JCheckBox disAreaCheckBox;
    private javax.swing.JToggleButton startButton;
    private javax.swing.JToggleButton stopButton;
    private javax.swing.JButton browseButton;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JLabel timeUnitLabel;
    private Color dirTextBackground;
    private Color nPduPerFileBackground;

    @Override
    public void componentOpened() {
    }

    @Override
    public void componentClosed() {
        forceChooser.removeLayer(disLayer);
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    /**
     * show the duration in timeLabel and a string seconds, minutes or hours in
     * timeUnitLabel
     *
     * @param duration in seconds
     */
    protected void showDurationAsString(double duration) {
        if (duration < 60) {
            timeUnitLabel.setText("seconds");
        } else if ((duration >= 60) && (duration < 3600)) {
            duration = duration / 60;
            timeUnitLabel.setText("minutes");
        } else if ((duration >= 3600)) {
            duration = duration / 3600;
            timeUnitLabel.setText("hours");
        }
        timeLabel.setText(String.valueOf(new DecimalFormat("#.#").format(duration)));
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return mgr;
    }
}
