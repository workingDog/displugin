/**
 * DisAARTopComponent
 *
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
package com.kodekutters.dis.gui;

import com.terramenta.globe.WorldWindManager;
import com.kodekutters.dis.core.DisReadFileSupport;
import com.kodekutters.dis.entity.Entity;
import com.kodekutters.dis.entity.EntityChildFactory;
import com.kodekutters.dis.entity.EntityListRootNode;
import com.kodekutters.dis.layer.DisLayer;
import com.kodekutters.dis.layer.DisAARLayer;
import com.kodekutters.dis.system.EntityExplorerManager;
import com.kodekutters.dis.util.AnnotationDragger;
import com.kodekutters.dis.util.DisFileChecker;
import com.kodekutters.dis.util.EntitySelector;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.ListView;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle.Messages;

/**
 * Top component for after action replay (AAR), reads pdu from a file and show
 * them on the map, does not send the pdu to the wire
 */
@ConvertAsProperties(dtd = "-//com.kodekutters.dis.gui//DisAAR//EN",
autostore = false)
@TopComponent.Description(preferredID = "DisAARTopComponent",
iconBase = "com/kodekutters/dis/image/resources/view_only.png",
persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "rightSide", openAtStartup = true)
@ActionID(category = "Window", id = "com.kodekutters.dis.gui.DisAARTopComponent")
@ActionReference(path = "Menu/Window" /*
 * , position = 333
 */)
@TopComponent.OpenActionRegistration(displayName = "#CTL_DisAARAction",
preferredID = "DisAARTopComponent")
@Messages({
    "CTL_DisAARAction=Dis After Action Replay",
    "CTL_DisAARTopComponent=Dis AAR",
    "HINT_DisAARTopComponent=Dis After Action Replay"
})
public final class DisAARTopComponent extends TopComponent implements ActionListener, ExplorerManager.Provider, Lookup.Provider {

    private static final ForceChooserTopComponent forceChooser = Lookup.getDefault().lookup(ForceChooserTopComponent.class);
    private static final WorldWindManager wwm = Lookup.getDefault().lookup(WorldWindManager.class);
    // this is enough to setup the entity selector
    private static final EntitySelector entitySelector = Lookup.getDefault().lookup(EntitySelector.class);
    private final ExplorerManager mgr = EntityExplorerManager.SHARED_ENTITY_MANAGER;
    private javax.swing.JScrollPane explorerPanel = new ListView();
    protected DisLayer disLayer;
    private int currentPduIndex = 0;
    private long numberOfPdu = 0;
    private Timer playTimer = null;   // play speed timer 
    private int playSpeed = 1000;     // play speed in milliseconds
    private boolean ready = false;
    private Color fileBackground;
    private Color warningColor = Color.pink;

    public DisAARTopComponent() {

        initComponents();
        fileTextField.setText(System.getProperty("user.home"));
        setName(Bundle.CTL_DisAARTopComponent());
        setToolTipText(Bundle.HINT_DisAARTopComponent());

        // the timing device for playing, we listen to its events here, see actionPerformed
        playSpeed = smoothSpeedValue(speedSlider.getValue());
        playTimer = new Timer(playSpeed, this);

        disLayer = (DisAARLayer) wwm.getWorldWindow().getModel().getLayers().getLayerByName("Dis AAR");
        if (disLayer == null) {
            disLayer = new DisAARLayer("Dis AAR");
            wwm.getLayers().add(disLayer);
            wwm.getLayers().add(disLayer.getAnnotationLayer());
//            wwm.getLayers().add(disLayer.getHistoryLayer());
        }

        disLayer.setDisSupport(new DisReadFileSupport(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {

                // if there was an error somewhere, usually the zip file is not a dis file
                if (ev.getActionCommand().equals(DisReadFileSupport.ACTION_COMMAND_FAIL)) {
                    stopButton.setEnabled(false);
                    startButton.setEnabled(false);
                    loadFileButton.setEnabled(true);
                    browseButton.setEnabled(true);
                    fileTextField.setEnabled(true);
                    setReady(false);
                    showWaiting(fileTextField, false);
                    return;
                }

                // when all the data is loaded, we are ready
                if (ev.getActionCommand().equals(DisReadFileSupport.DATA_LOADED)) {
                    numberOfPdu = disLayer.getDisSupport().getNumberOfPackets();
                    totalPdu.setText(String.valueOf(numberOfPdu));

                    // if did not get any pdu after loading
                    if (numberOfPdu <= 0L) {
                        stopButton.setEnabled(false);
                        startButton.setEnabled(false);
                        loadFileButton.setEnabled(true);
                        browseButton.setEnabled(true);
                        fileTextField.setEnabled(true);
                        setReady(false);
                        showWaiting(fileTextField, false);
                        return;
                    }

                    currentPduIndex = 0;
                    timeLabel.setText("1");
                    stopButton.setEnabled(false);
                    startButton.setEnabled(true);

                    pduSlider.setMaximum((int) numberOfPdu - 1);
                    pduSlider.setMinimum(0);
                    pduSlider.setMajorTickSpacing((int) (numberOfPdu / 10));
                    pduSlider.setValue(0);
                    pduSlider.setEnabled(true);

                    speedSlider.setEnabled(true);

                    loadFileButton.setEnabled(true);
                    browseButton.setEnabled(true);
                    fileTextField.setEnabled(true);

                    setReady(true);
                    showWaiting(fileTextField, false);

                    // display the first pdu on the map
                    ((DisReadFileSupport) disLayer.getDisSupport()).replayListAt(currentPduIndex);
                }
            }
        }));

        wwm.getWorldWindow().addSelectListener(new AnnotationDragger(wwm.getWorldWindow()));

        if ((forceChooser != null) && !ForceChooserTopComponent.getDisLayerList().contains(disLayer)) {
            forceChooser.addLayer(disLayer);
        }
        // this is the hidden ListView of mgr
        explorerPanel.setVisible(false);

        // puts the entities in the shared mgr via the explorerPanel
        associateLookup(ExplorerUtils.createLookup(mgr, getActionMap()));
        mgr.setRootContext(new EntityListRootNode(new EntityChildFactory(new ArrayList<Entity>())));

        // setup the speed slider
        Hashtable labelTable = new Hashtable();
        labelTable.put(new Integer(speedSlider.getMinimum()), new JLabel("Fast"));
        labelTable.put(new Integer(speedSlider.getMaximum()), new JLabel("Slow"));
        speedSlider.setLabelTable(labelTable);
        speedSlider.setPaintLabels(true);
        speedSlider.setInverted(true);
        speedSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (speedSlider.getValue() > 0) {
                    playSpeed = smoothSpeedValue(speedSlider.getValue());
                    playTimer.setInitialDelay(playSpeed);
                }
            }
        });

        // deal with the change in the pdu slider bar
        pduSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (isReady()) {
                    currentPduIndex = pduSlider.getValue();
                    timeLabel.setText(String.valueOf(currentPduIndex + 1));
                    // the layer at the currentPduIndex index, without removing past or future entities
                    ((DisReadFileSupport) disLayer.getDisSupport()).replayListAt(currentPduIndex);
                    // when the slidding stops, clean the layer of unwanted entities
                    if (pduSlider.getValueIsAdjusting() == false) {
                        // tally the list of the entities that should not be present at this index
                        List<Entity> listToRemove = new ArrayList<Entity>();
                        for (Entry<Integer, Entity> entry : disLayer.getIconList().entrySet()) {
                            // if the entity creation time is in the future, then
                            // the entity should not be present now
                            if (entry.getValue().getCreationTime() > currentPduIndex) {
                                listToRemove.add(entry.getValue());
                            }
                            // should also check that entities may have been 
                            // sent the "REMOVE_ENTITY" message
                        }
                        // remove the entities that should not be present at this time
                        for (Entity entity : listToRemove) {
                            disLayer.removeEntity(entity);
                        }
                        wwm.getWorldWindow().redrawNow();
                    }
                }
            }
        });

        stopButton.setEnabled(false);
        startButton.setEnabled(false);
        fileBackground = fileTextField.getBackground();

    }

    // exponential shapping of the speed slider values
    private int smoothSpeedValue(int val) {
        // want the result to be between 1 and 3000
//      double minv = Math.log(1);
//      double maxv = Math.log(3000);
//      double scale = (maxv - minv) / (speedSlider.getMaximum() - speedSlider.getMinimum());
//      return (int) Math.exp(minv + scale * (val - speedSlider.getMinimum()));        
        //
        return (int) Math.exp(0.00266879 * (double) val);  // hardwire this for now
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public DisLayer getDisLayer() {
        return disLayer;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        controlButtonGroup = new javax.swing.ButtonGroup();
        pduPanel = new javax.swing.JPanel();
        pduSlider = new javax.swing.JSlider();
        speedPanel = new javax.swing.JPanel();
        speedSlider = new javax.swing.JSlider();
        controlPanel = new javax.swing.JPanel();
        startButton = new javax.swing.JToggleButton();
        stopButton = new javax.swing.JToggleButton();
        progressBar = new javax.swing.JProgressBar();
        jLabel1 = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();
        filePanel = new javax.swing.JPanel();
        fileTextField = new javax.swing.JTextField();
        browseButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        totalPdu = new javax.swing.JLabel();
        loadFileButton = new javax.swing.JButton();

        pduPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("PDU"));

        pduSlider.setMajorTickSpacing(20);
        pduSlider.setPaintTicks(true);
        pduSlider.setValue(0);

        javax.swing.GroupLayout pduPanelLayout = new javax.swing.GroupLayout(pduPanel);
        pduPanel.setLayout(pduPanelLayout);
        pduPanelLayout.setHorizontalGroup(
            pduPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pduPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pduSlider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pduPanelLayout.setVerticalGroup(
            pduPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pduPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pduSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        speedPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Speed"));

        speedSlider.setMajorTickSpacing(300);
        speedSlider.setMaximum(3000);
        speedSlider.setMinimum(1);
        speedSlider.setPaintTicks(true);
        speedSlider.setValue(2500);

        javax.swing.GroupLayout speedPanelLayout = new javax.swing.GroupLayout(speedPanel);
        speedPanel.setLayout(speedPanelLayout);
        speedPanelLayout.setHorizontalGroup(
            speedPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, speedPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(speedSlider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        speedPanelLayout.setVerticalGroup(
            speedPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(speedPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(speedSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        controlPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        controlButtonGroup.add(startButton);
        startButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/kodekutters/dis/image/resources/Button Play.png"))); // NOI18N
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        controlButtonGroup.add(stopButton);
        stopButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/kodekutters/dis/image/resources/Button Pause.png"))); // NOI18N
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, "Pdu:");

        org.openide.awt.Mnemonics.setLocalizedText(timeLabel, "0");

        javax.swing.GroupLayout controlPanelLayout = new javax.swing.GroupLayout(controlPanel);
        controlPanel.setLayout(controlPanelLayout);
        controlPanelLayout.setHorizontalGroup(
            controlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(controlPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(controlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(controlPanelLayout.createSequentialGroup()
                        .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(stopButton, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(controlPanelLayout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(timeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        controlPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {startButton, stopButton});

        controlPanelLayout.setVerticalGroup(
            controlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(controlPanelLayout.createSequentialGroup()
                .addGroup(controlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(controlPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(controlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(stopButton)
                            .addComponent(startButton)))
                    .addGroup(controlPanelLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(controlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(timeLabel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        controlPanelLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {startButton, stopButton});

        filePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("File"));

        org.openide.awt.Mnemonics.setLocalizedText(browseButton, "Browse...");
        browseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, "Total PDU:");

        org.openide.awt.Mnemonics.setLocalizedText(totalPdu, "0");

        org.openide.awt.Mnemonics.setLocalizedText(loadFileButton, "Load file");
        loadFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadFileButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout filePanelLayout = new javax.swing.GroupLayout(filePanel);
        filePanel.setLayout(filePanelLayout);
        filePanelLayout.setHorizontalGroup(
            filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(filePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(filePanelLayout.createSequentialGroup()
                        .addComponent(fileTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(browseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(filePanelLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(totalPdu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(loadFileButton)))
                .addContainerGap())
        );
        filePanelLayout.setVerticalGroup(
            filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(filePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fileTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(browseButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(totalPdu)
                    .addComponent(loadFileButton))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(filePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(controlPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pduPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(speedPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(filePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(controlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pduPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(speedPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void stopButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopButtonActionPerformed
        if (isReady()) {
            pduSlider.setEnabled(true);
            playTimer.stop();
            pduSlider.setValueIsAdjusting(false);
            pduSlider.setValue(currentPduIndex);
            startButton.setEnabled(true);
            stopButton.setEnabled(false);
            loadFileButton.setEnabled(true);
            browseButton.setEnabled(true);
            fileTextField.setEnabled(true);
            progressBar.setIndeterminate(false);
        }
    }//GEN-LAST:event_stopButtonActionPerformed

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        if (isReady()) {
            // if at the end of the pdu list
            if (currentPduIndex == (int) (numberOfPdu - 1)) {
                pduSlider.setEnabled(true);
                return;
            }
            browseButton.setEnabled(false);
            fileTextField.setEnabled(false);
            loadFileButton.setEnabled(false);
            pduSlider.setEnabled(false);
            pduSlider.setValue(currentPduIndex);
            playTimer.setInitialDelay(playSpeed);
            playTimer.start();
            stopButton.setEnabled(true);
            startButton.setEnabled(false);
            progressBar.setIndeterminate(true);
        }
    }//GEN-LAST:event_startButtonActionPerformed

    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseButtonActionPerformed
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setMultiSelectionEnabled(false);
        if (fc.showOpenDialog(this) == JFileChooser.OPEN_DIALOG) {
            fileTextField.setText(fc.getSelectedFile().getAbsolutePath().trim().toString());
        }
    }//GEN-LAST:event_browseButtonActionPerformed

    private void loadFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadFileButtonActionPerformed
        if (isReadFileOk()) {
            stopButton.setEnabled(false);
            startButton.setEnabled(false);
            loadFileButton.setEnabled(false);
            pduSlider.setEnabled(false);
            browseButton.setEnabled(false);
            fileTextField.setEnabled(false);
            speedSlider.setEnabled(false);
            disLayer.getDisSupport().stop();
            disLayer.resetLayer();
            disLayer.getDisSupport().start();
            showWaiting(fileTextField, true);
        }
    }//GEN-LAST:event_loadFileButtonActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton browseButton;
    private javax.swing.ButtonGroup controlButtonGroup;
    private javax.swing.JPanel controlPanel;
    private javax.swing.JPanel filePanel;
    private javax.swing.JTextField fileTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton loadFileButton;
    private javax.swing.JPanel pduPanel;
    private javax.swing.JSlider pduSlider;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JPanel speedPanel;
    private javax.swing.JSlider speedSlider;
    private javax.swing.JToggleButton startButton;
    private javax.swing.JToggleButton stopButton;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JLabel totalPdu;
    // End of variables declaration//GEN-END:variables

    @Override
    public void componentOpened() {
        // TODO add custom code on component opening
    }

    @Override
    public void componentClosed() {
        forceChooser.removeLayer(disLayer);
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    private boolean isReadFileOk() {
        if (DisFileChecker.isFileOk(fileTextField.getText().trim())) {
            ((DisReadFileSupport) disLayer.getDisSupport()).setFileToRead(fileTextField.getText().trim());
            fileTextField.setBackground(fileBackground);
            return true;
        } else {
            fileTextField.setBackground(warningColor);
            return false;
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (isReady()) {
            currentPduIndex++;
            if (currentPduIndex >= numberOfPdu) {
                currentPduIndex = (int) (numberOfPdu - 1);
                playTimer.stop();
                stopButton.setEnabled(false);
                startButton.setEnabled(true);
                progressBar.setIndeterminate(false);
                pduSlider.setEnabled(true);
                return;
            }
            //
            ((DisReadFileSupport) disLayer.getDisSupport()).replayListAt(currentPduIndex);
            //
            pduSlider.setValueIsAdjusting(true);
            pduSlider.setValue(currentPduIndex);

            timeLabel.setText(String.valueOf(currentPduIndex + 1));
            playTimer.restart();
        }
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return mgr;
    }

    private void showWaiting(JComponent c, boolean on) {
        if (on) {
            c.setBackground(Color.green);
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        } else {
            c.setBackground(fileBackground);
            this.setCursor(Cursor.getDefaultCursor());
        }
    }
}
