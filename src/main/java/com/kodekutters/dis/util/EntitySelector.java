package com.kodekutters.dis.util;

import com.kodekutters.dis.entity.Entity;
import com.kodekutters.dis.system.EntityExplorerManager;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import com.terramenta.globe.WorldWindManager;

/**
 * EntitySelector
 *
 * @author Ringo Wathelet
 * @version 1.0  March, 2012
 */
@ServiceProvider(service = EntitySelection.class)
public class EntitySelector implements EntitySelection, SelectListener {

    public static final String ENTITY_ADDED = "ENTITY_ADDED";
    public static final String ENTITY_REMOVED = "ENTITY_REMOVED";
    private Entity currentSelection = null;
    private final Set<Entity> selectedEntities = new LinkedHashSet<>();
    private final List<ActionListener> actionListeners = new ArrayList<>();
    private static final WorldWindManager wwm = Lookup.getDefault().lookup(WorldWindManager.class);
    private final ExplorerManager mgr = EntityExplorerManager.SHARED_ENTITY_MANAGER;

    public EntitySelector() {
        this.wwm.getWorldWindow().addSelectListener(this);
    }

    public void fireAction(String action) {
        for (ActionListener theListener : this.actionListeners) {
            theListener.actionPerformed(new ActionEvent(this, 0, action));
        }
    }

    public void addListener(ActionListener aListener) {
        actionListeners.add(aListener);
    }

    public void removeListener(ActionListener aListener) {
        actionListeners.remove((ActionListener) aListener);
    }

    public Entity getCurrentSelection() {
        return currentSelection;
    }

    public void setCurrentSelection(Entity selection) {
        this.currentSelection = selection;
    }

    private Entity getEntityClickedOn(SelectEvent e) {
        Entity theEntity = null;
        // the entity is clicked on
        if (e.getTopObject() instanceof Entity) {
            theEntity = (Entity) e.getTopObject();
        } else // the entity annotation is clicked on
        if (e.getTopObject() instanceof SelectableAnnotation) {
            Object relatedObject = ((SelectableAnnotation) e.getTopObject()).getSelectableObject();
            if (relatedObject instanceof Entity) {
                theEntity = (Entity) relatedObject;
            }
        }
        return theEntity;
    }

    @Override
    public void selected(SelectEvent e) {
        Entity theEntity = getEntityClickedOn(e);
        if (theEntity != null) {
            if (e.isLeftClick()) {
                // toggle the entity selection
                theEntity.setSelected(!theEntity.isSelected());
                if (theEntity.isSelected()) {
                    deselectAllOthers(theEntity);
                    addCurrentSelection();
                    setCurrentSelection(theEntity);
                    showInPropertiesPanel(theEntity);
                } else {
                    removeCurrentSelection();
                    setCurrentSelection(null);
                    clearPropertiesPanel();
                }
            } else if (e.isRightClick()) {
                JPopupMenu popup = new JPopupMenu();
                popup.add(new JMenuItem(new EntitySelector.ShowHistoryAction(theEntity)));
                popup.show((Component) e.getSource(), e.getMouseEvent().getX(), e.getMouseEvent().getY());
            }
        }
//     e.consume();
    }

    // this will struggle with large numbers of entities....TODO...
    private void deselectAllOthers(Entity theEntity) {
        for (Node node : mgr.getRootContext().getChildren().getNodes()) {
            Entity testEntity = node.getLookup().lookup(Entity.class);
            if (testEntity != null) {
                if (!testEntity.getId().equals(theEntity.getId())) {
                    testEntity.setSelected(false);
                }
            }
        }
    }

    // find the node myself because 
    // mgr.getRootContext().getChildren().findChild(theEntity.getName());
    // does not work 
    private Node findNodeWith(Entity theEntity) {
        for (Node node : mgr.getRootContext().getChildren().getNodes()) {
            Entity testEntity = node.getLookup().lookup(Entity.class);
            if (testEntity != null) {
                if (testEntity.getId().equals(theEntity.getId())) {
                    return node;
                }
            }
        }
        return null;
    }

    private void showInPropertiesPanel(Entity theEntity) {
//        Node theNode = mgr.getRootContext().getChildren().findChild(theEntity.getDisplayInfo());
        Node theNode = findNodeWith(theEntity);
        if (theNode != null) {
            try {
                // this will make the node appear in the properties panel
                mgr.setSelectedNodes(new Node[]{theNode});
            } catch (PropertyVetoException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    private void clearPropertiesPanel() {
        try {
            mgr.setSelectedNodes(new Node[]{});
        } catch (PropertyVetoException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public void removeCurrentSelection() {
        if (currentSelection != null) {
            selectedEntities.remove(currentSelection);
            fireAction(ENTITY_REMOVED);
        }
    }

    public void addCurrentSelection() {
        if (currentSelection != null) {
            if (!selectedEntities.contains(currentSelection)) {
                selectedEntities.add(currentSelection);
            }
            fireAction(ENTITY_ADDED);
        }
    }

    @Override
    public Set<Entity> getSelectedEntities() {
        return selectedEntities;
    }

    public class ShowHistoryAction extends AbstractAction {

        private final Entity theEntity;

        public ShowHistoryAction(Entity entity) {
            super();
            this.theEntity = entity;
            putValue(NAME, "Show/hide history");
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            theEntity.setHistoryVisible(!theEntity.isHistoryVisible());
            wwm.getWorldWindow().redrawNow();
        }
    }
}
