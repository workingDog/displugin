/*
 * A modification (a new add method) of LinkedBlockingDeque.
 * New elements will be added at the end of the queue,
 * if there is no space available an element at the begining of the queue will be remove
 * to make room to the new element.
 * 
 * Typical use is BoundedDeque(int capacity)
 */
package com.kodekutters.dis.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.LinkedBlockingDeque;

/**
 *
 * @author R. Wathelet 
 */
public class BoundedDeque<E> extends LinkedBlockingDeque<E> implements Serializable{

    public BoundedDeque() {
        super();
    }

    public BoundedDeque(Collection<? extends E> c) {
        super(c);
    }

    public BoundedDeque(int capacity) {
        super(capacity);
    }

    public boolean add(E e) {
        if (this.remainingCapacity() < 1) {
            this.removeFirst();
            return super.add(e);
        } else {
            return super.add(e);
        }
    }
    // addAll is not modified.... todo
}
