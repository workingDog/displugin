package com.kodekutters.dis.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author wathelet ringo
 */
public class PropertiesReader extends Properties {

    static private PropertiesReader instance = null;

    public PropertiesReader() {
    }

    static public PropertiesReader getInstance() {
        if (instance == null) {
            instance = new PropertiesReader();
        }
        return instance;
    }

    public void loadProperties(String filename) {
        File file = new File(filename);
        if (file.exists() && file.canRead() && file.isFile()) {
            try {
                this.load(new BufferedInputStream(new FileInputStream(file)));
                System.out.println(file.getAbsolutePath() + " properties file loaded.");
            } catch (FileNotFoundException e) {
                System.out.println("File not found:" + file.getAbsolutePath());
                e.printStackTrace();
            } catch (IOException e) {
                System.out.println("Error loading properties:" + file.getAbsolutePath());
                e.printStackTrace();
            }
        } else {
            System.out.println("^^^^^ ERROR in PropertiesReader loadProperties ^^^^^ " + file.getAbsolutePath() + " NOT loaded ");
        }
    }

    public int getInt(String key) {
        try {
            return Integer.parseInt(getProperty(key));
        } catch (Exception ex) {
            return 0;
        }
    }

    public double getDouble(String key) {
        try {
            return Double.parseDouble(getProperty(key));
        } catch (Exception ex) {
            return 0.0;
        }
    }

    public String getProperty(String key) {
        if (key == null) {
            return null;
        }
        return super.getProperty(key);
    }

    public String getProperty(String key, String defaultValue) {
        try {
            String value = getProperty(key);
            if (value == null) {
                return defaultValue;
            }
            return value;
        } catch (Exception ex) {
            return defaultValue;
        }
    }

}
