package com.kodekutters.dis.util;

import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.event.*;
import gov.nasa.worldwind.util.Logging;
import java.awt.*;

/**
 *
 * R. Wathelet, 2012, to drag SelectableAnnotation, needs a bit more work
 *
 */
public class AnnotationDragger implements SelectListener {

    public AnnotationDragger(WorldWindow wwd) {
        if (wwd == null) {
            String msg = Logging.getMessage("nullValue.WorldWindowGLCanvas");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    public void selected(SelectEvent event) {
        if (event == null) {
            return;
        }

        Object topObject = event.getTopObject();
        if ((topObject == null) || !(topObject instanceof SelectableAnnotation)) {
            return;
        }

        if (event.getEventAction().equals(SelectEvent.DRAG)) {
            DragSelectEvent dragEvent = (DragSelectEvent) event;

            int dx = dragEvent.getPickPoint().x - dragEvent.getPreviousPickPoint().x;
            int dy = dragEvent.getPickPoint().y - dragEvent.getPreviousPickPoint().y;

            Point offset = ((SelectableAnnotation) topObject).getAttributes().getDrawOffset();
            offset.x += dx / 4;
            offset.y -= dy / 4;
        }

        event.consume();
    }
}
