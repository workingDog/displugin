package com.kodekutters.dis.util;

import edu.nps.moves.dis.Vector3Double;
import edu.nps.moves.disutil.CoordinateConversions;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.coords.MGRSCoord;
import gov.nasa.worldwind.globes.Globe;


/**
 * DisCoordinateConversions
 *
 * @author wathelet ringo
 */
public class DisCoordinateConversions {

    public static double EQUATORIAL_EARTH_RADIUS = 6378137.0; //WGS84 earth equatorial radius in metres

    public static Position xyzToPosition(double[] xyz) {
        double[] lla = CoordinateConversions.xyzToLatLonRadians(xyz);
        return Position.fromRadians(lla[0], lla[1], lla[2]);
    }

    public static Position xyzToPosition(Vector3Double vec) {
        double[] xyz = {vec.getX(), vec.getY(), vec.getZ()};
        double[] lla = CoordinateConversions.xyzToLatLonRadians(xyz);
        return Position.fromRadians(lla[0], lla[1], lla[2]);
    }

    public static Vector3Double PositionToxyz(Position pos) {
        double[] xyz = CoordinateConversions.getXYZfromLatLonDegrees(pos.getLatitude().degrees, pos.getLongitude().degrees, pos.getAltitude());
        Vector3Double vec = new Vector3Double();
        vec.setX(xyz[0]);
        vec.setY(xyz[1]);
        vec.setZ(xyz[2]);
        return vec;
    }

    /**
     * convert a distance in metres to decimal degrees
     *
     * @param m the distance in metres
     * @return the number of degrees the distance spans
     */
    public static double convertDistToDegrees(double m) {
        return ((180.0 / Math.PI) * m / EQUATORIAL_EARTH_RADIUS);
    }

    /**
     * read a string representation of a location and return a Position from it
     *
     * @param location the string representation of a location
     * @return a Position from the string representation
     */
    public static Position getPositionFromPoint(String locationString) {
        double lat = 0.0;
        double lon = 0.0;
        Position pos = null;
        String latString;
        String lonString;

        String location = locationString.trim();

        // if have a comma separated decimal degrees lat,lon
        if (location.contains(",")) {
            // example -25.12345,123.45678
            String[] posArray = location.split(",");
            if (posArray.length == 2) {
                latString = posArray[0].trim();
                lonString = posArray[1].trim();
            } else {
                return null;
            }

            try {
                lat = Double.valueOf(latString);
                lon = Double.valueOf(lonString);
            } catch (Exception ex) {
                System.out.println("----ERROR location should be comma separated decimal degrees lat,lon " + location + " " + ex);
                return null;
            }

            if (!isValidLatDeg(lat) || !isValidLonDeg(lon)) {
                System.out.println("----ERROR in location is: " + latString + ", " + lonString);
                return null;
            } else {
                return Position.fromDegrees(lat, lon, 0.0);
            }
        }

        // if 11 chars
        if (location.length() == 11) {
            // example 1044N04623E
            latString = location.substring(0, 5);
            lonString = location.substring(5, 11);

            try {
                double deg = Double.valueOf(latString.substring(0, 2));
                double minute = Double.valueOf(latString.substring(2, 4));
                lat = deg + (minute * 60.0) / 3600.0;
                if (!latString.substring(4, 5).equalsIgnoreCase("N")) {
                    lat = -lat;
                }
            } catch (Exception ex) {
                System.out.println("----ERROR in latitude, location is: " + latString + " " + ex);
                return null;
            }

            try {
                double deg = Double.valueOf(lonString.substring(0, 3));
                double minute = Double.valueOf(lonString.substring(3, 5));
                lon = deg + (minute * 60.0) / 3600.0;
                if (!lonString.substring(5, 6).equalsIgnoreCase("E")) {
                    lon = -lon;
                }
            } catch (Exception ex) {
                System.out.println("----ERROR in longitude, location is: " + lonString + " " + ex);
                return null;
            }

            if (!isValidLatDeg(lat) || !isValidLonDeg(lon)) {
                System.out.println("----ERROR in location is: " + latString + ", " + lonString);
                return null;
            } else {
                return Position.fromDegrees(lat, lon, 0.0);
            }
        }

        // if 15 characters
        if (location.length() == 15) {
            // example 104401N0462302E
            latString = location.substring(0, 7);
            lonString = location.substring(7, 15);

            try {
                double deg = Double.valueOf(latString.substring(0, 2));
                double minute = Double.valueOf(latString.substring(2, 4));
                double seconds = Double.valueOf(latString.substring(4, 6));
                lat = deg + (minute * 60.0 + seconds) / 3600.0;
                if (!latString.substring(6, 7).equalsIgnoreCase("N")) {
                    lat = -lat;
                }
            } catch (Exception ex) {
                System.out.println("----ERROR in latitude, location is: " + latString + " " + ex);
                return null;
            }

            try {
                double deg = Double.valueOf(lonString.substring(0, 3));
                double minute = Double.valueOf(lonString.substring(3, 5));
                double seconds = Double.valueOf(lonString.substring(5, 7));
                lon = deg + (minute * 60.0 + seconds) / 3600.0;
                if (!lonString.substring(7, 8).equalsIgnoreCase("E")) {
                    lon = -lon;
                }
            } catch (Exception ex) {
                System.out.println("----ERROR in longitude, location is: " + lonString + " " + ex);
                return null;
            }

            if (!isValidLatDeg(lat) || !isValidLonDeg(lon)) {
                System.out.println("----ERROR in location is: " + latString + ", " + lonString);
                return null;
            } else {
                return Position.fromDegrees(lat, lon, 0.0);
            }
        }

        // if 12 chars, it could be 11 char with a blank space in between lat long
        if (location.length() == 12) {
            // example 1044N 04623E
            latString = location.substring(0, 5);
            lonString = location.substring(6, 12);
            String text = latString + lonString;
            return getPositionFromPoint(text);
        }

        // if 16 chars, it could be 15 char with a blank space in between lat long
        if (location.length() == 16) {
            // example 104401N 0462302E
            latString = location.substring(0, 7);
            lonString = location.substring(8, 16);
            String text = latString + lonString;
            return getPositionFromPoint(text);
        }

        return pos;
    }

    public static boolean isValidLatDeg(double val) {
        return (val >= -90 && val <= 90);
    }

    public static boolean isValidLonDeg(double val) {
        return (val >= -180 && val <= 180);
    }

    public static Position getPositionFromMGRS(String location, Globe theGlobe) {
        MGRSCoord mgrsCoord = MGRSCoord.fromString(location, theGlobe);
        Position pos = Position.fromDegrees(mgrsCoord.getLatitude().degrees, mgrsCoord.getLongitude().degrees, 0.0);
        return pos;
    }
}