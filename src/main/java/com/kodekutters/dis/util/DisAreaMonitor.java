package com.kodekutters.dis.util;

import com.terramenta.globe.WorldWindManager;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.operation.buffer.BufferOp;
import com.vividsolutions.jts.operation.buffer.BufferParameters;
import com.kodekutters.dis.entity.Entity;
import com.kodekutters.dis.layer.DisLayer;
import edu.nps.moves.dis.EntityStatePdu;
import edu.nps.moves.dis.Pdu;
import edu.nps.moves.disenum.PduType;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.ShapeAttributes;
import gov.nasa.worldwind.render.SurfacePolygon;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import org.openide.util.Lookup;

/**
 * DisAreaMonitor, a geographic area that is used to filter the dis packets. 
 * see also DisViewSupport and DisLayer
 *
 * @author R. Wathelet, 2012
 * @version 1.0
 */
public class DisAreaMonitor {

    private static DisAreaMonitor instance = null;
    private final DisLayer disLayer;
    private static final WorldWindManager wwm = Lookup.getDefault().lookup(WorldWindManager.class);
    private java.awt.Polygon disAreaPoly = new java.awt.Polygon();
    private final java.awt.Polygon augmentedAreaPoly = new java.awt.Polygon();
    private final SurfacePolygon[] areaDrawings = new SurfacePolygon[2];
    private final int factor = 1000;

    public DisAreaMonitor() {
        this.readSelectedArea();
        disLayer = (DisLayer) wwm.getWorldWindow().getModel().getLayers().getLayerByName("Dis viewer");
    }

    public static DisAreaMonitor getInstance() {
        if (instance == null) {
            instance = new DisAreaMonitor();
        }
        return instance;
    }

    /**
     * read the selected area (to monitor pdus) from the properties file typically "Dis.properties"
     */
    private void readSelectedArea() {
        ArrayList<Position> augmentedAreaPositions = new ArrayList<>();
        ArrayList<Position> disAreaPositions = new ArrayList<>();
        // use java polygon so we can use its contains method
        disAreaPoly = new java.awt.Polygon();
        // note: numbers must start with dis.area.Point000
        for (int i = 0; i < 999; i++) {
            String p = "dis.area.Point";
            if (i <= 9) {
                p = "dis.area.Point00";
            }
            if (i > 9 && i <= 99) {
                p = "dis.area.Point0";
            }
            String point = PropertiesReader.getInstance().getProperty(p + String.valueOf(i));
            if (point == null || point.isEmpty() || point.equalsIgnoreCase("")) {
                break;
            }
            Position pos = DisCoordinateConversions.getPositionFromPoint(point);
            if (pos != null) {
                disAreaPositions.add(pos);
                // note: x=lon, y=lat
                disAreaPoly.addPoint((int) (pos.longitude.degrees * factor), (int) (pos.latitude.degrees * factor));
            }
        }

        if (disAreaPositions.size() >= 3) {
            int dist = Integer.parseInt(PropertiesReader.getInstance().getProperty("dis.area.buffer", "2000"));
            double distDeg = DisCoordinateConversions.convertDistToDegrees(dist);
            // close the shape
            disAreaPositions.add(disAreaPositions.get(0));
            // this is a Java Topology Suite Polygon
            Polygon poly = this.asJtsPolygon(disAreaPositions);
            // from JTS, this produces a shape that is the buffer (Minkowski sum) of poly
            Polygon outerBox = (Polygon) BufferOp.bufferOp(poly, distDeg, BufferParameters.CAP_SQUARE);
            for (Coordinate coord : outerBox.getExteriorRing().getCoordinates()) {
                augmentedAreaPositions.add(Position.fromDegrees(coord.y, coord.x, 0.0));
                // note: x=lon, y=lat
                augmentedAreaPoly.addPoint((int) (coord.x * factor), (int) (coord.y * factor));

            }
            areaDrawings[0] = makeAreaShape(disAreaPositions, Color.red);
            areaDrawings[1] = makeAreaShape(augmentedAreaPositions, Color.yellow);
        }
    }

    public boolean isWithingAugmentedArea(Entity theEntity) {
        // if don't have an area return true
        if ((augmentedAreaPoly == null) || (augmentedAreaPoly.npoints < 3)) {
            return true;
        }
        return augmentedAreaPoly.contains((int) (theEntity.getPosition().longitude.degrees * factor), (int) (theEntity.getPosition().latitude.degrees * factor));
    }

    public boolean isWithingArea(Entity theEntity) {
        // if don't have an area return true
        if ((disAreaPoly == null) || (disAreaPoly.npoints < 3)) {
            return true;
        }
        return disAreaPoly.contains((int) (theEntity.getPosition().longitude.degrees * factor), (int) (theEntity.getPosition().latitude.degrees * factor));
    }

    private Polygon asJtsPolygon(ArrayList<? extends Position> posList) {
        Coordinate[] ringCoords = new Coordinate[posList.size()];
        int i = 0;
        for (Position pos : posList) {
            ringCoords[i++] = new Coordinate(pos.getLongitude().degrees, pos.getLatitude().degrees, pos.getElevation());
        }
        // a jts polygon
        return new GeometryFactory().createPolygon(new GeometryFactory().createLinearRing(ringCoords), null);
    }

    public boolean isWithingArea(Pdu pdu) {
        // if there is no area
        if ((disAreaPoly == null) || (disAreaPoly.npoints < 3)) {
            return true;
        }
        Position pos = null;
        if (pdu.getPduTypeEnum() == PduType.ENTITY_STATE) {
            pos = DisCoordinateConversions.xyzToPosition(((EntityStatePdu) pdu).getEntityLocation());
        }
        if (pos == null) {
            return false;
        }
        return disAreaPoly.contains((int) (pos.longitude.degrees * factor), (int) (pos.latitude.degrees * factor));
    }

    public boolean isWithingAugmentedArea(Pdu pdu) {
        // if there is no area
        if ((augmentedAreaPoly == null) || (augmentedAreaPoly.npoints < 3)) {
            return true;
        }
        Position pos = null;
        if (pdu.getPduTypeEnum() == PduType.ENTITY_STATE) {
            pos = DisCoordinateConversions.xyzToPosition(((EntityStatePdu) pdu).getEntityLocation());
        }
        if (pos == null) {
            return false;
        }
        return augmentedAreaPoly.contains((int) (pos.longitude.degrees * factor), (int) (pos.latitude.degrees * factor));
    }

    public void showArea(boolean onof) {
        if (disAreaPoly.npoints >= 3) {
            if (onof && (disLayer != null)) {
                disLayer.addRenderable(areaDrawings[0]);    // the area
//                this.addRenderable(areaDrawings[1]);      // the augmented area
            } else {
                disLayer.removeRenderable(areaDrawings[0]); 
//                this.removeRenderable(areaDrawings[1]);   
            }
        }
        // just to tell the layer has changed
        disLayer.firePropertyChange(new PropertyChangeEvent(this, "", true, false));
    }

    private SurfacePolygon makeAreaShape(ArrayList<Position> positions, Color col) {
        ShapeAttributes attr = new BasicShapeAttributes();
        attr.setDrawInterior(false);
        attr.setOutlineMaterial(new Material(col));
        attr.setOutlineWidth(2);

        SurfacePolygon shape = new SurfacePolygon();
        shape.setAttributes(attr);
        shape.setValue(AVKey.DISPLAY_NAME, "Dis area");
        shape.setEnableBatchPicking(false);
        shape.setLocations(positions);
        return shape;
    }
}
