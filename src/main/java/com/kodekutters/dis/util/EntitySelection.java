package com.kodekutters.dis.util;

import com.kodekutters.dis.entity.Entity;
import java.util.Set;

/**
 *
 * @author R. Wathelet 
 */
public interface EntitySelection {

    public Set<Entity> getSelectedEntities();

    public Entity getCurrentSelection();
}
