package com.kodekutters.dis.util;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

/**
 * DisFileChecker
 *
 * A basic helper class, DIS file and directory checker.
 * Try to determine if the file is a valid dis pdu file
 *
 * @author R. Wathelet
 * @version 1.0 @date May, 2012
 */
public class DisFileChecker {

    public DisFileChecker(String fileName) {
    }

    public static boolean isFileOk(String fileName) {
        if (fileName.isEmpty()) {
            return false;
        } else {
            File theFile = new File(fileName);
            if (theFile.isDirectory()) {
                return false;
            }
            if (theFile != null) {
                if (!theFile.exists()) {
                    System.out.println("file " + fileName + " does not exist");
                    return false;
                } else {
                    if (!theFile.getAbsolutePath().toLowerCase().endsWith(".zip")) {
                        System.out.println("cannot process file : " + fileName + " because it does not ends with .zip");
                        return false;
                    }
                    if (!isDisFile(theFile)) {
                        System.out.println("file " + fileName + " is not a valid DIS packet file");
                        return false;
                    }
                    return true;
                }
            } else {
                return false;
            }
        }
    }

    public static boolean isDirectoryOk(String fileName) {
        if (fileName.isEmpty()) {
            return false;
        } else {
            File dirFile = new File(fileName);
            if (dirFile.isDirectory() && dirFile.exists()) {
                if (dirFile.list().length == 0) {
                    return false;
                }
                return true;
            }
            return false;
        }
    }

    public static boolean isDisFile(File theFile) {
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(theFile.getAbsoluteFile());
        } catch (ZipException ex) {
            System.out.println("error opening file " + theFile.getAbsoluteFile() + " " + ex.getMessage());
            return false;
        } catch (IOException ex) {
            System.out.println("error opening file " + theFile.getAbsoluteFile() + " " + ex.getMessage());
            return false;
        }
        if (zipFile == null) {
            System.out.println("error file " + theFile.getAbsoluteFile() + " is NULL");
            return false;
        }
        Enumeration elist = zipFile.entries();
        if (!elist.hasMoreElements()) {
            System.out.println("zip file has nothing in it");
            return false;
        } else {
            while (elist.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) elist.nextElement();
                if (!zipEntry.getName().toLowerCase().endsWith(".xml")) {
                    System.out.println("zip file entry " + zipEntry.getName() + " does not end with .xml");
                    return false;
                }
            }
            // should do more checks here...todo
        }
        return true;
    }
}
