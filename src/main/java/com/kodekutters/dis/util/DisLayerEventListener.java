package com.kodekutters.dis.util;

import com.kodekutters.dis.entity.Entity;
import java.util.EventListener;

/**
 * DisLayerEventListener
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
public interface DisLayerEventListener extends EventListener {

    public void addEntity(Entity entity);
    
    public void removeEntity(Entity entity);

    public void resetLayer();

}