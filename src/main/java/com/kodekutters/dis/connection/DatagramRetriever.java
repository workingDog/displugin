package com.kodekutters.dis.connection;

import java.net.*;
import java.util.*;
import java.io.*;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;

/**
 * DatagramRetriever simply reads datagrams from the wire. To be used in its own
 * thread, e.g. new Thread(datagramRetriever).start(); then collect the
 * datagrams with datagramRetriever.getDatagrams().
 * 
 * This is a rehash of
 * ref: see DatagramStreamBuffer code from DIS-MIL-NAVY, AUTHOR: DMcG
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
@ServiceProvider(service = DatagramRetriever.class)
public class DatagramRetriever extends DisConnectionService implements Runnable {

    private Vector datagramVec = new Vector();
    private volatile boolean isActive = false;
    private int timeOutValue = 1000;   // millisec

    public DatagramRetriever() {
    }

    public DatagramRetriever(String theAddress, int thePort, CONNECTION_TYPE conectionType) {
        setPort(thePort);
        setAddress(theAddress);
        this.setConnectionType(conectionType);
    }

    public DatagramRetriever(DisConnectionService conection) {
        this(conection.getAddress(), conection.getPort(), conection.getConnectionType());
        this.setTimeToLive(conection.getTimeToLive());
    }

    @Override
    public void run() {
        byte[] buf;
        DatagramPacket packet;

        try {
            makeSocketConnection();
        } catch (IOException ex) {
            System.out.println("..... error in DatagramRetriever connection, maybe the port number is wrong: " + ex.getMessage());
            this.stopConnection();
            return;
        }

        // need to have this to unblock the reading from time to time
        setTimeOut(timeOutValue);
        
        this.setActive(true);
        while (isActive) {
            buf = new byte[DisConnectionService.MAX_PACKET_SIZE];
            packet = new DatagramPacket(buf, buf.length);
            try {
                // this will block until something is received or time out is reached
                socket.receive(packet);
                synchronized (datagramVec) {
                    datagramVec.addElement(packet);
                }
            } catch (IOException ex) {
            }
        }
    }

    public Vector getDatagrams() {
        Vector theDatagrams;
        synchronized (datagramVec) {
            theDatagrams = datagramVec;
            datagramVec = new Vector();
        }
        return theDatagrams;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        this.isActive = active;
    }

    public void stop() {
        this.setActive(false);
        super.stopConnection();
    }

    public int getTimeOutValue() {
        return timeOutValue;
    }

    public void setTimeOutValue(int timeoutValue) {
        this.timeOutValue = timeoutValue;
    }

    private void setTimeOut(int t) {
        try {
            this.getDatagramSocket().setSoTimeout(t);
        } catch (SocketException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
