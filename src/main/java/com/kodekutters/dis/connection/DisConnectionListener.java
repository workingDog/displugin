package com.kodekutters.dis.connection;

import java.util.EventListener;

/**
 * DisConnectionListener
 *
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
public interface DisConnectionListener extends EventListener {

    public void stopConnection();

    public void makeSocketConnection();
}
