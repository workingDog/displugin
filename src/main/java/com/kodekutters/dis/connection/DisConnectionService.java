/**
 * DisConnectionService
 *
 * ref see:  http://open-dis.sourceforge.net/Open-DIS.html xplane example
 * 
 * @author Ringo Wathelet
 * @version 1.0, 2012
 */
package com.kodekutters.dis.connection;

import com.kodekutters.dis.util.SubnetUtils;
import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.EventListenerList;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service = DisConnectionService.class)
public class DisConnectionService {

    public static int DEFAULT_PORT = 3000;
    public static String DEFAULT_MULTICAST_ADDRESS = "239.255.255.250";
    public static int MAX_PACKET_SIZE = 8192;
    protected DatagramSocket socket = null;
    private int port;
    private String address;
    private int timeToLive = 64;  // 32 same site, 64 same region, 128 same continent, 255 the world

    public static enum CONNECTION_TYPE {
        LOCALHOST, MULTICAST, UNICAST, BROADCAST
    };
    private CONNECTION_TYPE connectionType = CONNECTION_TYPE.MULTICAST;
    private final EventListenerList listeners = new EventListenerList();

    public static DisConnectionService getDefault() {
        DisConnectionService service = Lookup.getDefault().lookup(DisConnectionService.class);
        if (service == null) {
            service = new DisConnectionService();
        }
        return service;
    }

    public void addEventListener(DisConnectionListener listener) {
        listeners.add(DisConnectionListener.class, listener);
    }

    public void removeEventListener(DisConnectionListener listener) {
        listeners.remove(DisConnectionListener.class, listener);
    }

    protected final void fireMakeSocketConnection() {
        for (DisConnectionListener listener : listeners.getListeners(DisConnectionListener.class)) {
            listener.makeSocketConnection();
        }
    }

    protected final void fireStopConnection() {
        for (DisConnectionListener listener : listeners.getListeners(DisConnectionListener.class)) {
            listener.stopConnection();
        }
    }

    public void makeSocketConnection() throws IOException {
        fireMakeSocketConnection();
        switch (this.getConnectionType()) {

            case BROADCAST:
                this.socket = new DatagramSocket(this.getPort());
                this.socket.setBroadcast(true);
                this.socket.connect(this.getInetAddress(), this.getPort());
                break;

            case MULTICAST:
                this.socket = new MulticastSocket(this.getPort());
                ((MulticastSocket) this.socket).setTimeToLive(timeToLive);
                ((MulticastSocket) this.socket).joinGroup(this.getInetAddress());
                break;

            case UNICAST:
                this.socket = new DatagramSocket(this.getPort());
                this.socket.connect(this.getInetAddress(), this.getPort());
                break;

            case LOCALHOST:
                this.socket = new DatagramSocket();
                break;

            default:
                System.out.println(" ------- connection NOT started ...> no connection type -------");
        }

        System.out.println("Starting connection at " + this.getAddress() + " port " + this.getPort() + " Connection type " + connectionType.toString() + " = " + getInetAddress());

    }

    public void stopConnection() {
        if (this.socket != null) {
            this.socket.disconnect();
            this.socket.close();
        }
        fireStopConnection();
    }

    public int getMaxPacketSize() {
        return MAX_PACKET_SIZE;
    }

    public void setMaxPacketSize(int max_size) {
        this.MAX_PACKET_SIZE = max_size;
    }

    public void setConnectionType(CONNECTION_TYPE cType) {
        this.connectionType = cType;
    }

    public void setAddress(String newAddress) {
        this.address = newAddress;
    }

    public void setPort(int newPort) {
        this.port = newPort;
    }

    public String getAddress() {
        return this.address;
    }

    public InetAddress getInetAddress() {
        try {
            return InetAddress.getByName(this.getAddress());
        } catch (UnknownHostException ex) {
            Logger.getLogger(DisConnectionService.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public int getPort() {
        return this.port;
    }

    public CONNECTION_TYPE getConnectionType() {
        return this.connectionType;
    }

    public DatagramSocket getDatagramSocket() {
        return this.socket;
    }

    public int getTimeToLive() {
        return timeToLive;
    }

    public void setTimeToLive(int val) {
        this.timeToLive = val;
    }

    public String calculateBroadcastAddress() throws IOException {
        NetworkInterface networkInterface;
        short mask = 0;
        try {
            networkInterface = NetworkInterface.getByInetAddress(InetAddress.getLocalHost());
            // pick the first mask
            mask = networkInterface.getInterfaceAddresses().get(0).getNetworkPrefixLength();
            // if the first mask is no good, try to pick the first valid mask
            if (mask > 31) {
                for (InterfaceAddress ipnet : networkInterface.getInterfaceAddresses()) {
                    mask = ipnet.getNetworkPrefixLength();
                    if (mask <= 31) {
                        // pick the first valid mask
                        break;
                    } else {
                        mask = 0;
                    }
                }
            }
            // calculate the broadcast address from the local address and the mask
            SubnetUtils netUtil = new SubnetUtils(InetAddress.getLocalHost().getHostAddress() + "/" + String.valueOf(mask));
            return netUtil.getInfo().getBroadcastAddress();
        } catch (Exception ex) {
            Logger.getLogger(DisConnectionService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
