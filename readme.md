# A Distributed Interactive Simulation (DIS) plugin for Terramenta.

A DIS plugin made using the [open-dis library](http://open-dis.sourceforge.net/Open-DIS.html)
 for use with the NASA World Wind [Terramenta framework](https://bitbucket.org/teamninjaneer/terramenta/wiki/Home). It is for small
scale DIS capture, replay and after action replay (AAR) and mostly deals with entity state PDU. See the Wiki for more information.

Ringo Wathelet

key words: DIS, World Wind, worldwind, Terramenta, Distributed Interactive Simulation, NASA World Wind

## Background

From [wikipedia](http://en.wikipedia.org/wiki/Distributed_Interactive_Simulation),
"Distributed Interactive Simulation (DIS) is an IEEE standard for conducting real-time 
platform-level wargaming across multiple host computers and is used worldwide, 
especially by military organizations but also by other agencies such as those 
involved in space exploration and medicine."

DIS is a simple, old but widespread standard to make simulations work together. 
It has now been replaced by the High Level Architecture [(HLA)](http://standards.ieee.org/findstds/standard/1516-2010.html) 
and [TENA](https://www.tena-sda.org/display/intro/Home), particularly in Defence simulations. However DIS is in a large number of legacy simulations and newer simulations 
often provide some DIS compatibility or "gateway" services, translating from/to DIS and HLA. 

Although well past its prime, the DIS standard is still worked on by the Simulation Interoperability Standards Organization [SISO](http://www.sisostds.org/Home.aspx). 
An overview of DIS can be found in this [SISO document](http://www.sisostds.org/DigitalLibrary.aspx?Command=Core_Download&EntryId=29289).   
 
This DIS plugin is for looking at the "big picture" when running war games. To 
display the world view of "the game" on a 3D globe. It is not a tool for detailed analysis between the 
simulation entities, e.g. measurements of one-on-one engagements with fast missiles. It is more for understanding the 
broad disposition of forces in relation to the high level aims of the mission to be achieved.

## Components

There are 4 main visual components:

  - **Dis Viewer**, reads DIS packets from the network, displays them on the map and can store them in a file. 
  - **Dis Replay**, replays DIS packets from a file (or directory), shows them on the map and sends them to the network. 
  - **Dis AAR**, the after action replay component, replays DIS packets from a file but does not send them to the network.
  - **Dis Force**, a panel that allows the user to select what forces to display.

Both **Dis Viewer** and **Dis Replay** use the same base panel to setup the DIS network connection with the usual port and ip address. 
The files to store and replay the DIS packets are zip files containing xml file
entries. 

## Dis viewer

The **Dis Viewer** is used to view and record DIS packets that are being sent by an external simulation. 
In **Dis Viewer** the user sets up the DIS network connection and has the option of recording the DIS 
traffic to a file. The entities are represented on the globe by a small 
tee shaped icon. The color represents the force type, e.g. blue for friendly, red for opposing, green for neutral.

The *Start* button starts the reading of the DIS packets from the network. 
The user can *Pause* the display/recording, then restart again by pressing the *Start* button. 
Pressing the *Stop* button will stop the display and recording, and write the recorded DIS packets to a zip file in the chosen directory. 

The annotation of each entity on the globe can be dragged (out of the way for example). You
can click on an entity or its annotation and see the entity properties in the
properties panel. 

## Dis Replay

The **Dis Replay** is used to replay a pre-recorded (using **Dis Viewer**) DIS file or directory of files.
The entities are displayed on the globe and at the same time are sent to the network for others to receive.
As with the **Dis Viewer** the user needs to setup the network connection.
The playback can also be paused, then restarted or stopped.

## Dis AAR

The experimental **Dis AAR** replays a pre-recorded file (using **Dis Viewer**), 
displays the entities on the globe, but does not sends the DIS packets to the network. 
In **Dis AAR**, the user must first load a pre-recorded file, 
then can use the *Start* button or the slider bar to play the file.
The speed of replay can be adjusted with a speed slider. The user can also use a pdu slider
bar to play the file at the desired pdu number. This component shows the pdu in the 
order they were recorded.

## Dis Force

The **Dis Force** is a panel to select which force types are to be displayed 
while recording or playback. For example Friendly, Opposing, Neutral.

## Layers

The **Dis Viewer** and **Dis Replay** visual components create 3 worldwind layers, one layer for the entities, one
for their annotations and one layer for showing the position history, that is a line that
shows the entities past movements. These layers should appear in the layer manager.
Note the **Dis AAR** does not have a history layer. 

The layers currently only show entity state pdu, but the files record and play everything as set by
the settings. A DIS entity is represented by a small tee icon of the force type
colour and pointing in the heading direction. Munitions are small dots. 

## Status 

Limited but successful testing was done with receiving and replaying DIS pdu. The *Auto start/stop*
has never been tested. I found that some systems use multicast but really is broadcast. 
Never tried unicast to another PC. Sending and receiving using unicast to 
yourself will not work, use multicast instead. 

## Dependencies

This plugin uses the opendis external libraries, these can found in the lib directory.
